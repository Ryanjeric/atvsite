<div class="container-fluid">
  <div class="container">
    <div class="row">
      <div class="terms-of-use">
        <h1>Terms of Use</h1>
        <p>Welcome to SedonaVortexHealingATV.com (the "Website"). By visiting our Website and accessing the information, services, products and tools (collectively the "Services") we provide, you signify your agreement to these terms of use (the "Terms"). Please read them carefully.</p>


        <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>1. Definitions of parties and related persons.</b>
        <p>The term "we," "us," "our," and "ours," and other first person personal pronouns shall mean and refer to Vortex Healing ATV Adventure, LLC operating the website SedonaVortexHealingATV.com, and its members, successors, assigns, affiliates, subsidiaries, and business licensors (collectively and individually referred to herein as "VortexHealingATV"). The terms "you", "your", "yours", and other second person personal pronouns mean and refer to all persons who use or visit the Website, or any products, software, data feeds, and services provided by VortexHealingATV. </p>


        <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>2. Changes to Terms of Use.</b>
        <p>We may change these Terms at any time by notifying you of the change in writing or electronically (including without limitation, by E-mail or by posting a notice on the Website that these Terms have been "updated"). The changes also will appear in this document, which you can access at any time by going to the link to these Terms at the bottom of every page. You signify that you agree to be bound by such changes by using a Service after changes are made to these Terms. </p>


        <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>3. Limitations on use.</b>

        <p>a. Only one individual may access a Service at the same time using the same user name or password, unless we agree otherwise.</p>
        <p>b. The text, graphics, images, video, metadata, design, organization, compilation, look and feel, advertising and all other protectable intellectual property (the "Content") available through the Services are our property or the property of our advertisers and licensors and are protected by copyright and other intellectual property laws. You may only access and view the Content personally and for a non-commercial purpose in compliance with these Terms. You may not either directly or through the use of any device, software, internet site, web-based service, or other means remove, alter, bypass, avoid, interfere with, or circumvent any copyright, trademark, or other proprietary notices marked on the Content or any digital rights management mechanism, device, or other content protection or access control measure associated with the Content. You may not either directly or through the use of any device, software, internet site, web-based service, or other means copy, download, stream capture, reproduce, duplicate, archive, distribute, upload, publish, modify, translate, broadcast, perform, display, sell, transmit or retransmit the Content unless expressly permitted by VortexHealingATV in writing. You may not incorporate the Content into, or stream or retransmit the Content via, any hardware or software application or make it available via frames or in-line links unless expressly permitted by VortexHealingATV in writing. Furthermore, you may not create, recreate, distribute or advertise an index of any significant portion of the Content unless authorized by VortexHealingATV. You agree not to post any Content (other than headlines from our RSS feed with active links back to the full article on the Service) to Weblogs, newsgroups, mail lists or electronic bulletin boards, without our written consent. You may not build a business utilizing the Content, whether or not for profit. In addition, you are strictly prohibited from creating derivative works or materials that otherwise are derived from or based on in any way the Content, including montages, mash-ups and similar videos, wallpaper, desktop themes, greeting cards, and merchandise, unless it is expressly permitted by VortexHealingATV in writing. This prohibition applies even if you intend to give away the derivative materials free of charge.</p>



        <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>4. Cancellation & Refund Policy</b>
        <p>You may cancel an online reservation 48 hours or more before the scheduled time of your rental and the entire amount paid will be refunded to you. However, if you cancel a booking in less than 48 hours before the scheduled time of your rental a $100 Cancellation Fee will be deducted from any refund.</p>


        <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>5. Gift Certificates </b>
        <p>
          Gift certificates purchased on the Website (the "Gift Certificates") may only be redeemed toward the purchase of eligible Services on the Website. Redemption of Gift Certificates is subject to change in our sole discretion. If a purchase exceeds the redeemer’s Gift Certificate balance, the remaining amount must be paid with another payment method.
        </p>
        <p>
          Gift Certificates do not expire. However, Gift Certificates cannot be refunded, reloaded, resold, transferred for value, redeemed for cash, or applied to any other account, either prior or subsequent to redemption, unless otherwise required by applicable law.
        </p>


        <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>6. Your Account</b>
        <p>
          In order to access some features of the Services, you may have to create a VortexHealingATV account. You must not use another's account without permission. When creating your account, you must provide accurate and complete information. You are solely responsible for the activity that occurs on your account, and you must keep your account password secure. You must notify VortexHealingATV immediately of any breach of security or unauthorized use of your account. Although VortexHealingATV will not be liable for your losses caused by any unauthorized use of your account, you may be liable for the losses of VortexHealingATV or others due to such unauthorized use.
        </p>


        <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>7. Collection and Use of Personal Information</b>
        <p>
          For information about VortexHealingATV's policies and practices regarding the collection and use of your personally identifiable information, please read <a href="/privacy-policy">VortexHealingATV's Privacy Policy</a>. The Privacy Policy is incorporated by reference and made part of these Terms. Thus, by agreeing to these Terms, you agree that your use of the Services are governed by the Privacy Policy in effect at the time of your use.
        </p>


        <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>8. Community; user-generated content.</b>

        <p>a. <i>User Name.</i> We may require you to register to have access to our community area. We reserve the right to disclose any information about you, including registration data, in order to comply with any applicable laws and/or requests under legal process, to operate our systems properly, to protect our property or rights, and to safeguard the interests of others.</p>
        <p>b. User-generated content.</p>

        <p>
          i. <i>User Content.</i> We offer you the opportunity to comment on and engage in discussions regarding articles, companies and various topics. Any content, information, graphics, audio, images, and links you submit as part of creating your profile or in connection with any of the foregoing activities is referred to as "User Content" in these Terms and is subject to various terms and conditions as set forth below.
        </p>
        <p>
          ii. <i>Cautions Regarding Other Users and User Content.</i> You understand and agree that User Content includes information, views, opinions, and recommendations of many individuals and organizations and is designed to help you gather the information you need to help you make your own decisions. Importantly, you are responsible for your own decisions and for properly analyzing and verifying any information you intend to rely upon. We do not endorse any recommendation or opinion made by any user. You should also be aware that other users may use our Services for personal gain. As a result, please approach messages with appropriate skepticism. User Content may be misleading, deceptive, or in error.
        </p>
        <p>
          iii. <i>Grant of Rights and Representations by You.</i> If you upload, post or submit any User Content on a Service, you represent to us that you have all the necessary legal rights to upload, post or submit such User Content and that it will not violate any law or the rights of any person. You agree that upon uploading, posting or submitting information on the Services, you grant VortexHealingATV a non-exclusive, transferable, worldwide, fully paid-up, royalty-free, perpetual, irrevocable right and license to use, distribute, publicly perform, display, reproduce, and create derivative works from your User Content in any and all media, in any manner, in whole or part, without any duty to compensate you. You also grant us the right to authorize the use of User Content, or any portion thereof, by users and other users in accordance with these Terms, including the rights to feature your User Content specifically on the Services and to allow other users to request access to your User Content, such as for example through an RSS feed.
        </p>
        <p>
          iv. <i>Monitoring or Removal of Certain User Content. </i> We may also monitor or remove any User Content for any reason and without notice to you. This includes all materials related to your use of the services or membership, including E-mail accounts, postings, profiles or other personalized information you have created while on the Services.
        </p>
        <p>
          v. <i>Rules of Conduct.</i> Users must not post material that is offensive, contains personal attacks, is misleading or demonstrably false or is solely (or overwhelmingly) self-promotional.
        </p>

        <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>9. Linked Destinations and Advertising</b>
        <p>
          a. <i>Third Party Destinations.</i> If we provide links or pointers to other websites or destinations, you should not infer or assume that VortexHealingATV operates, controls, or is otherwise connected with these other websites or destinations. When you click on a link within the Services, we will not warn you that you have left the Services and are subject to the terms and conditions (including privacy policies) of another website or destination. In some cases, it may be less obvious than others that you have left the Services and reached another website or destination. Please be careful to read the terms of use and privacy policy of any other website or destination before you provide any confidential information or engage in any transactions. You should not rely on these Terms to govern your use of another website or destination.
        </p>
        <p>
          VortexHealingATV is not responsible for the content or practices of any website or destination other than the SedonaVortexHealingATV.com site, even if it links to the SedonaVortexHealingATV.com site and even if the website or destination is operated by a company affiliated or otherwise connected with VortexHealingATV. By using the Services, you acknowledge and agree that VortexHealingATV is not responsible or liable to you for any content or other materials hosted and served from any website or destination other than the SedonaVortexHealingATV.com site.
        </p>
        <p>
          b. <i>Advertisements.</i> VortexHealingATV takes no responsibility for advertisements or any third party material, nor does it take any responsibility for the products or services provided by advertisers. Any dealings you have with advertisers found while using the Services are between you and the advertiser, and you agree that VortexHealingATV is not liable for any loss or claim that you may have against an advertiser.
        </p>

        <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>10. Disclaimers</b>
        <p>
          a. <i>Disclaimers of Warranties and Limitations on Liability.</i> You agree that your access to, and use of, the services and the content available through the Services is on an "as-is", "as available" basis and we specifically disclaim any representations or warranties, express or implied, including, without limitation, any representations or warranties of merchantability or fitness for a particular purpose. VortexHealingATV will not be liable (jointly or severally) to you or any other person as a result of your access or use of the Services for indirect, consequential, special, incidental, punitive, or exemplary damages, including, without limitation, lost profits and lost revenues (collectively, the "excluded damages"), whether or not characterized in negligence, tort, contract, or other theory of liability, even if the VortexHealingATV has been advised of the possibility of or could have foreseen any of the excluded damages, and irrespective of any failure of an essential purpose of a limited remedy.
        </p>
        <p>
          b. <i>Medical Disclaimer.</i> If the SedonaVortexHealingATV.comsite provides health-related or medical information, no such information is intended to treat or cure any disease or to offer any specific diagnosis to any individual as we do not give medical advice, nor do we provide medical or diagnostic services. We assume no responsibility for injuries suffered while practicing these techniques. We strongly recommend that you get a professional medical advice before you apply any techniques presented on our site.
        </p>

        <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>11. Disputes and Jurisdiction</b>
        <p>
          <i>Resolving Disputes.</i> Since we always prefer to find ways to satisfy you as quickly and efficiently as possible, we want to address your concerns without needing a formal legal case. Before filing a claim against VortexHealingATV, you agree to try to resolve any dispute or claim relating to your relationship with VortexHealingATV or your use of products or services sold or distributed through SedonaVortexHealingATV.com ("Dispute") informally by contacting us at <a href="#">VortexHealingATV@gmail.com</a>..   We will also try to resolve the dispute informally by contacting you by email. If a dispute is not resolved within 30 days of submission, you or VortexHealingATV may bring a formal proceeding.
        </p>
        <p>
          <i>We Both Agree To Arbitrate.</i> You and VortexHealingATV agree to resolve any claims relating to these Terms or the Services through final and binding arbitration, except as set forth under Exceptions to Agreement to Arbitrate below. The Federal Arbitration Act and federal arbitration law apply to these Terms. There is no judge or jury in arbitration, and court review of an arbitration award is limited. However, an arbitrator can award on an individual basis the same damages and relief as a court (including injunctive and declaratory relief or statutory damages), and must follow these Terms as a court would.
        </p>
        <p>
          The arbitration will be conducted by the American Arbitration Association (AAA) under its rules, including the AAA's Supplementary Procedures for Consumer-Related Disputes. The AAA's rules are available at www.adr.org or by calling 1-800-778-7879. Payment of all filing, administration and arbitrator fees will be governed by the AAA's rules. We will reimburse those fees for claims totaling less than $10,000 unless the arbitrator determines the claims are frivolous. Likewise, VortexHealingATV will not seek attorneys' fees and costs in arbitration unless the arbitrator determines the claims are frivolous. You may choose to have the arbitration conducted by telephone, based on written submissions, or in person in the county where you live or at another mutually agreed location.
        </p>
        <p>
          <i>No Class Actions.</i> You may only resolve disputes with us on an individual basis, and may not bring a claim as a plaintiff or a class member in a class, consolidated, or representative action. Class arbitrations, class actions, private attorney general actions, and consolidation with other arbitrations aren't allowed.
        </p>
        <p>
          If for any reason a Dispute proceeds in court rather than in arbitration we each waive any right to a jury trial. We also both agree that you or we may bring suit in court to enjoin infringement or other misuse of intellectual property rights.
        </p>

        <p>
          <i>Exceptions to Agreement to Arbitrate.</i> Either you or VortexHealingATV may assert claims, if they qualify, in small claims court in Sedona, Arizona or any United States county where you live or work. Either party may bring a lawsuit solely for injunctive relief to stop unauthorized use or abuse of the Services, or intellectual property infringement (for example, trademark, trade secret, copyright, or patent rights) without first engaging in arbitration or the informal dispute-resolution process described above.
        </p>
        <p>
          You agree that these Terms, as well as any and all claims arising from these Terms will be governed by and construed in accordance with the laws of the State of Arizona, United States of America, without regard to any conflict or choice of law principles. The sole jurisdiction and venue for any litigation arising out of these Terms will be an appropriate federal or state court located in Arizona. These Terms will not be governed by the United Nations convention on contracts for the international sale of goods.
        </p>


        <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>13. General Provisions.</b>

        <p>
          a. <i>Site Policies.</i> Please review our other policies posted on this site. These policies also govern your visit to VortexHealingATV.com. We reserve the right to make changes to our site, policies, and these Terms at any time.
        </p>
        <p>
          b. <i>Severability.</i> If any provision of these Terms or its application is invalid, void or unenforceable, the remainder of these Terms will not be impaired or affected and will remain in full force and effect.
        </p>
        <p>
          c. <i>Entire Agreement.</i> These Terms contains the final and entire agreement between us regarding your use of the services and supersedes all previous and contemporaneous oral or written agreements regarding your use of the Services.
        </p>
        <p>
          d. <i>Waiver.</i> Unless otherwise specified in these Terms, the failure by a party to require performance of any provision shall not affect that party's right to require performance at any time thereafter, nor shall a waiver of any breach or default of these Terms constitute a waiver of any subsequent breach or default or a waiver of the provision itself.
        </p>
        <p>
          e. <i>Change to Services.</i> We may discontinue or change the Services, or their availability to you, at any time for any reason and without notice to you.
        </p>
        <p>
          f. <i>No Assignment.</i> These Terms are personal to you, which means that you may not assign your rights or obligations under these Terms to anyone. No third party is a beneficiary of these Terms.
        </p>
        <p>
          g. <i>Survival of Termination.</i> The following provisions will survive expiration or termination of these Terms: Sections 7 through 11 of these Terms and other provisions that by their nature are intended to survive termination of these Terms.
        </p>

      </div>
    </div>
  </div>
</div>
