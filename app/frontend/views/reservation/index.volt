<div ng-controller="ReserveCtrl">
	<script type="text/ng-template" id="reserve.html">
		<div ng-include="'/fe/tpl/reserve.html'"></div>
	</script>
	<script type="text/ng-template" id="atv.html">
		<div ng-include="'/fe/tpl/atv.phtml'"></div>
	</script>
<script type="text/javascript">
  function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>
<form class="form-horizontal" role="form" id="wewForm"  ng-submit="savereservationz(res)">

<!-- <div ng-show="steponez">
	<div class="container-fluid no-margin padbot">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 pad_md">
					<center><span class="rocky_title"><strong>Reservation</strong></span></center>
				</div>
				<div class="col-sm-6 pad_md_z mv_centered">
					<img src="{[{ picture }]}" class="img-responsive">
					<br>
				</div>
				<div class="col-sm-6 pad_md_z mv_centered">
					<div class="form-group ">
						<label class="title" for="sel1">Select ATV:</label>
						<select class="form-control" id="sel1" ng-model="atv" ng-change="atvselected(atv)">
							<option value='' style="display:none">CHOOSE ATV</option>
							<option ng-repeat="data in listatv" value="{[{ data.id }]}" label="{[{ data.title }]}">{[{ data.title }]}</option>
						</select>
						<input type="hidden" ng-model="atvtxt">
					</div>
					<div ng-show="wew">
					<label class="clr_orange">{[{ subtitle }]}</label> <br>
						<p>
						{[{ feature }]}
						</p>
						<div ng-bind-html="desc">

						</div>
					<p></p>
					</div>
					<label class="clr_orange">Rental Hour/Price</label> <br>
					<div class="form-group ">
						<select class="form-control" id="sel2" ng-model="rentalhr" ng-change="rentalselected(rentalhr)">
							<option value='' style="display:none">Choose RentalHr/Price</option>
							<option ng-repeat="data in listrental" value="{[{ data.rentalid }]}" label="{[{ data.session }]} : {[{ data.totalhr }]}Hr/s Price : ${[{ data.price }]}"></option>
						</select>
						<input type="hidden" ng-model="rentaltxt">
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="container-fluid no-margin bg_yellowish padbot">
		<div class="container">
			<div class="row"><br><br>
				<div class="col-sm-12 pad_md whitebg">
					 <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
				</div>
			</div>

		</div>
	</div>
	<div class="container-fluid no-margin bg_yellowish padbot" ng-show="reservelist.length > 0">
		<div class="container">
			<div class="row"><br><br>
				<div class="panel panel-default">
					<div class="panel-heading">
						Reservation Lists
					</div>
					<div class="table-responsive">
						<table class="table table-striped b-t b-light">
							<thead>
								<tr>
									<th>Description</th>
									<th>Amount</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr class="ng-scope" ng-repeat="x in reservelist">
								<td class="ng-binding">
									ATV : {[{ x.atv }]}
									<p class="text-muted ng-binding">Rental Hour : {[{ x.rentalhr }]}<br>
									<strong class="ng-binding">RESERVATION DATE : {[{ x.datereserve | date : 'MMMM d, y' }]}</strong></p>
								</td>
								<td class="ng-binding">
									${[{ x.price }]}
								</td>
								<td style="width: 10px;">
									<a href="" ng-click="deleteService($index, x.datestart)" class="active"><i class="fa fa-times text-danger text-active" alt="delete">Delete</i></a>
								</td>
								</tr>
							<tr>
								<td class="text-right"> Total </td>
								<td colspan="2" class="text-lefth ng-binding">${[{ getTotal() }]}</td>
							</tr>
						</tbody>
					</table>
					<div class="wizard-footer">
						<div class="pull-right">
							<input type="button" ng-click="stepone=false;steptwo=true;" class="btn btn-next btn-fill btn-info btn-wd btn-sm" name="next" value="Proceed to 2nd Step" style="display: inline-block;margin:20px">
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div> -->

<!-- <div class="container-fluid no-margin bg_yellowish padbot">
	<div class="container">
		<div class="row row-centered">
			<div class="col-sm-12 pad_md">
				<center><span class="rocky_title"><strong>Reserve Your Ride!</strong></span></center>
			</div>
			<div class="col-sm-8 whitebg pad_md col-centered" >
				<div class="form-group">
					<label class="control-label col-sm-2" for="fname">First name:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" required id="fname" ng-model="res.fname" placeholder="Firstname">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="lname">Last name:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" required id="lname" ng-model="res.lname" placeholder="Lastname">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="number">Phone Number:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" required id="number" ng-model="res.phonenum" placeholder="Phonenumber">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="Email">Email:</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" required id="Email" ng-model="res.email" placeholder="Email">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-7" for="many">How many people will be in the vehicle?</label>
					<div class="col-sm-5">
						<input type="number" class="form-control" required id="many" ng-model="res.numpeople" placeholder="number only" min="1" onkeypress='return isNumberKey(event)'>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-7" for="drive">How many people will drive?</label>
					<div class="col-sm-5">
						<input type="number" class="form-control" required id="drive" ng-model="res.numpeopledrive" placeholder="number only" min="1" onkeypress='return isNumberKey(event)'>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-12" for="drive">Are you bringing kids? If yes, how many and what age are they?</label>
					<div class="col-sm-12">
						<textarea class="form-control" required rows="5" ng-model="res.kids" placeholder="kids?"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-12" for="drive">Any special requests?</label>
					<div class="col-sm-12">
						<textarea class="form-control" required rows="5" ng-model="res.request" placeholder="requests?"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-12" for="drive">Do you want us to deliver RZRs to Sedona?
						(We are located in Cottonwood, but we offer delivery to Sedona for your convenience.)</label>
						<div class="col-sm-12">
							<textarea class="form-control" required rows="5" ng-model="res.deliver" placeholder="requests?"></textarea>
						</div>
				</div>
				<div class="pull-left">
				<button type="button" class="btn btn-primary" ng-click="stepone=true;steptwo=false;">BACK</button>
				</div>
				<div class="pull-right">
				<button type="submit" class="btn btn-primary">SUBMIT RESERVATION</button>
				</div>
			</div>
		</div>
	</div>
</div> -->

<div class="container-fluid no-margin padbot" ng-show="stepone">
	<div class="container">
		<div class="row row-centered">
			<div class="col-sm-12 pad_md callusat">
				<strong></strong><center>Please call us at 928-634-5990 with any questions about your reservation</center></strong>
			</div>
			<div class="col-sm-9 whitepanel col-centered lightgray">
				<center><span class="stronger"><strong>Reserve Your Ride!</strong></span></center>
			</div>
			<div class="col-sm-9 whitepanel pad_md col-centered" >
				<div class="form-group">
					<label class="control-label col-sm-2" for="fname">First name<span class="reddot">*</span></label>
					<div class="col-sm-4">
						<input type="text" class="form-control" required id="fname" ng-model="res.fname" placeholder="Firstname">
					</div>
					<label class="control-label col-sm-2" for="lname">Last name<span class="reddot">*</span></label>
					<div class="col-sm-4">
						<input type="text" class="form-control" required id="lname" ng-model="res.lname" placeholder="Lastname">
					</div>
				</div>
				<br>
				<div class="linegreen">
					<span class="textintheline">
					 Contact information
					</span>
				</div><br>


				<div class="form-group">
					<label class="control-label col-sm-4 col-centered" for="number">
					Phone Number<span class="reddot">*</span><br>
					<span class="graytext">best to reach you while you are travelling</span>
					</label>
					<div class="col-sm-8 col-centered">
						<input type="number" class="form-control" required id="number" ng-model="res.phonenum" placeholder="Phonenumber">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4" for="Email">Email<span class="reddot">*</span>
					<br>
					<span class="graytext">valid email address for confirmation</span>
					</label>
					<div class="col-sm-8">
						<input type="email" class="form-control" required id="Email" ng-model="res.email" placeholder="Email">
					</div>
				</div>

				<br>
				<div class="linegreen">
					<span class="textintheline1">
					 Rental Date & Duration
					</span>
				</div><br>

				<div class="form-group">
					<label class="control-label col-sm-2" for="Rentaldate">Rental Date<span class="reddot">*</span></label>
					<div class="col-sm-4 date">
						<div class="input-group">
						<input type="text" class="form-control" required id="Rentaldate" ng-model="res.rentaldate" placeholder="Rentaldate">
						<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div>
					</div>
					<label class="control-label col-sm-2" for="RentalHr">Duration<span class="reddot">*</span></label>
					<div class="col-sm-4">
					<select class="form-control" id="RentalHr" required ng-model="res.rental" ng-change="selduration(res.rental)">
						<option value='' style="display:none">Choose Duration</option>
						<option ng-repeat="data in listrenthr" value="{[{ data.id }]}">{[{ data.session }]} : {[{ data.starttime }]}-{[{ data.endtime }]} ({[{ data.duration }]}hr)</option>
					</select>
					</div>
				</div>

				<br>
				<div class="linegreen">
					<span class="textintheline2">
					 Choose Your ATVs
					</span>
				</div><br>

				<div class="form-group ">
					<span class="col-sm-12 graytext1">Please select 1 or more  ATVs you would like to rent</span>
					<div class="col-sm-12">
					<div class="checkbox" ng-repeat="data in listatv">
						<label bold>
							<input type="checkbox" name="list" ng-model="res.atv[$index]" ng-true-value="'{[{data.id}]}'">{[{ data.title }]} {[{ data.subtitle }]}
							<a href="#" ng-click="view(data.id)"><span class="glyphicon glyphicon-question-sign"></span></a>
						</label>
					</div>
					</div>
				</div>

				<br>
				<div class="linegreen">
					<span class="textintheline3">
						 Help us serve your needs better
					</span>
				</div><br>

				<div class="form-group">
					<label class="control-label col-sm-7" for="many">How many people will be in the vehicle?</label>
					<div class="col-sm-5">
						<input type="number" class="form-control" required id="many" ng-model="res.numpeople" placeholder="number only" min="1" onkeypress='return isNumberKey(event)'>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-7" for="drive">How many people will drive?</label>
					<div class="col-sm-5">
						<input type="number" class="form-control" required id="drive" ng-model="res.numpeopledrive" placeholder="number only" min="1" onkeypress='return isNumberKey(event)'>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-12" for="drive">Are you bringing kids? If yes, how many and what age are they?</label>
					<div class="col-sm-12">
						<textarea class="form-control" required rows="5" ng-model="res.kids" placeholder="kids?"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-12" for="drive">Any special requests?</label>
					<div class="col-sm-12">
						<textarea class="form-control" required rows="5" ng-model="res.request" placeholder="requests?"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-12" for="drive">Do you want us to deliver RZRs to Sedona?
						(We are located in Cottonwood, but we offer delivery to Sedona for your convenience.)</label>
						<div class="col-sm-12">
							<textarea class="form-control" required rows="5" ng-model="res.deliver" placeholder="requests?"></textarea>
						</div>
				</div>

				<br>
				<div class="linegreen">
					<span class="textintheline4">
						 Secure your Reservation
					</span>
				</div><br>

				<div class="form-group">
						<div class="col-sm-12 mejored pad_md">Valid credit information is required to make your reservation.<br></div>
						<label class="control-label col-sm-4" for="cc">Credit Card Type</label>
						<div class="col-sm-8 col-centered">
							<select class="form-control" id="cc" required ng-model="res.cardtype">
								<option value="">Choose</option>
								<option value="visa">Visa</option>
								<option value="mastercard">Master Card</option>
								<option value="Americanexpress">American Express</option>
								<option value="discovercard">Discover Card</option>
							</select>
						</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4" for="ccn">Credit Card Number:</label>
					<div class="col-sm-8 col-centered">
						<input type="text" class="form-control" required id="ccn" ng-model="res.cardnum" onkeypress='return isNumberKey(event)'>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4" for="ccn">Expiration Date:</label>
					<div class="col-sm-8 date">
						<div class="input-group">
						<input type="text" class="form-control" required id="ccn" ng-model="res.cardexp">
						<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4" for="ccn">Security Code:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" required id="ccn" ng-model="res.securitycode">
					</div>
				</div>
				<hr>
				<div class="pull-right">
				<button type="submit" ng-disabled="dismember" class="btn btn-primary">SUBMIT RESERVATION</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid no-margin bg_yellowish padbot" ng-if="steptri">
	<div class="container">
		<div class="row row-centered">
			<div class="col-sm-12 pad_md">
				<center><span class="rocky_title"><strong>Reserve Your Ride!</strong></span></center>
			</div>
			<div class="col-sm-8 whitebg pad_md col-centered" >
				<div class="jumbotron">
    				<h1>Thank you!</h1>
    				 Your reservation successfully added to our Waiting list wait for a phone call to confirm your reservation Thank you!

    			</div>
			</div>
		</div>
	</div>
</div>


<div class="container-fluid no-margin bg_yellow">
	<div class="container">
		<div class="row pad_md_vert">
			<div class="col-sm-10 col-sm-offset-1">
				<span class="title" id="requirements_title">NOTIFICATION!</span>
				<ul class="requirements">
					<?php foreach($listnotice as $list) { ?>
					<li><?php echo $list->notice; ?></li>
					<?php } ?>
				</ul>
		</div>
	</div>
</div>
</div>
<toaster-container toaster-options="{'time-out': 6000, 'close-button':true, 'animation-class': 'toast-bottom-right', 'position-class': 'toast-bottom-right'}"></toaster-container>
</form>
</div>
