<!-- {#<div class="container-fluid bg_yellowish">
	<div class="container">
		<div class="row">
			<div class="col-sm-5 col-sm-offset-1 pad_md_vert">
       <div class="col-sm-6">
        <img src="/img/frontend/innerpage/contact.jpeg" class="img-responsive margin_top">
      </div>
      <div class="col-sm-6 borderblck">
        <p>
          To make a reservation or for any comments, questions, or suggestions, please fill out and submit the form below. We look forward to hearing from you.
        </p>
      </div>
    </div>
      <div class="col-sm-5 pad_md_vert mv_centered pad1">
          <span class="contactfont">EMAIL ADDRESS</span><br>
          <a href="mailto:rioverdeatv@gmail.com" style="color: #ff8400;"><span class="contactfont">rioverdeatv@gmail.com</span></a><br><br>
          <span class="contactfont">TELEPHONE NUMBER</span><br>
          <span class="contactgray">928-634-5990</span><br><br>
          <span class="contactfont">PHYSICAL ADDRESS</span><br>
          <span class="contactgray">3420 E. SR 89A <br>Cottonwood, AZ 86326</span>
      </div>
		</div>
	</div>
</div>#}

  {#<div class="container-fluid pad_md_vert bg_yellowish" ng-controller="ContactCtrl"
			style="border">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 whitebg">
          <div class="col-sm-12"><span class="title">Contact Us</span></div>
          <form name="message" ng-submit="send(jb)">
            <div class="col-sm-6"><b>Name</b><span class="reditu">*</span><input type="text" class="fill_col" ng-model="jb.fname" required><br><span class="contact_label">First</span></div>
            <div class="col-sm-6"><input type="text" style="margin-top:20px" class="fill_col" ng-model="jb.lname" required><span class="contact_label">Last</span></div>
            <div class="col-sm-12"><br>
              <b>Email</b><span class="reditu">*</span><br><input type="email" class="fill_col" required ng-model="jb.email"><br><br>
              <b>Comment</b><span class="reditu">*</span><br><textarea class="fill_col textarea" required ng-model="jb.content"></textarea><br><br>

              <div class="col-sm-2 pad"><b>Captcha</b><span class="reditu">*</span><br><br><span id="txtCaptchaDiv" class="captchatxt"></span><input type="hidden" id="txtCaptcha" ng-model="str1" name="captcha" /><br><br></div>

              <div class="col-sm-5" style="margin-left: 10px;"><input type="text" name="txtInput" id="txtInput" size="15" class="fill_col" placeholder="Type in the characters in the image" ng-model="str2" required name="captcha2"/></div>
              <br>
              <br>
              <div class="col-sm-12 pad"><input type="submit" class="submitbutton" ng-disabled="dismember" value="Submit"></div>
            </div>
        </form>
        <script type="text/javascript">
          function checkform(theform){
            var why = "";

            if(theform.txtInput.value == ""){
              why += "Captcha code should not be empty";
            }
            if(theform.txtInput.value != "") {
              if(ValidCaptcha(theform.txtInput.value) == false) {
                why += "Captcha code did not match";
              }
            }
            if(why != "") {
              alert(why);
              return false;
            }
          }

          //Generates the captcha function

          function randomString() {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
            var string_length = 6;
            var randomstring = '';
            for (var i=0; i<string_length; i++) {
              var rnum = Math.floor(Math.random() * chars.length);
              randomstring += chars.substring(rnum,rnum+1);
            }
            return randomstring;
          }

          var code = randomString();

          document.getElementById("txtCaptcha").value = code;
          document.getElementById("txtCaptchaDiv").innerHTML = code;

          // Validate the Entered input aganist the generated security code function
          function ValidCaptcha(){
            var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
            var str2 = removeSpaces(document.getElementById('txtInput').value);
            if (str1 == str2){
              return true;
            }else{
              return false;
            }
          }
          // Remove the spaces from the entered and generated code
          function removeSpaces(string){
            return string.split(' ').join('');
          }
          </script>
        <br>
          <div class="col-sm-6 msgsent" ng-show="notify" ng-bind="msg"></div>
          <div class="col-sm-6 msgerr" ng-show="err" ng-bind="msg1"></div>
        </div>
      </div>
    </div>
  </div>#} -->

<div class="container-fluid bg_yellowish">
	<div class="container wrapper-md">
		<div class="row">
			<div class="col-sm-3">
				<img src="<?php echo $amazonlink . "/uploads/reservation/".$contact->reservationimg; ?>" class="img-responsive" id="contact-img">
			</div>
			<div class="col-sm-9">
				<p id="contact-message">
          "<?php echo $contact->reservationtext; ?>"
        </p>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid pad_md_vert" ng-controller="ContactCtrl"
		style="border">
		<div class="container">
			<div class="row" id="contactus">
				<div class="col-sm-12" id="contactus-header">
					CONTACT US
				</div>
				<div class="col-sm-4" id="contactus-lbody">
					<center>
						<span class="contactfont"><i class="glyphicon glyphicon-envelope"></i> Email</span><br>
	          <a href="mailto:<?php echo $contact->email; ?>" style="color: #ff8400;"><span class="contactfont"><?php echo $contact->email; ?></span></a><br><br>
	          <span class="contactfont"><i class="glyphicon glyphicon-earphone"></i> Phone</span><br>
	          <span class="contactgray"><?php echo $contact->contactno; ?></span><br><br>
	          <span class="contactfont"><i class="glyphicon glyphicon-home"></i> Address</span><br>
	          <span class="contactgray"><?php echo $contact->address; ?></span>
	          	<!-- 3420 E. SR 89A <br>Cottonwood, AZ 86326 -->
						<img src="/img/frontend/innerpage/polarisrzr570_shadow.png" style="width:100%"/>
					</center>
				</div>
				<div class="col-sm-8">
					<form name="contactUsForm" ng-submit="send(jb, contactUsForm)" novalidate>

						<div class="col-sm-2 contactfont">First Name</div>
						<div class="col-sm-10">
							<span class="pull-right" ng-show="contactUsForm.firstname.$touched" ng-messages="contactUsForm.firstname.$error">
								<span class="badge bg-orange" ng-message="required">Required</span>
								<span class="badge bg-orange" ng-message="pattern">Invalid</span>
							</span>
							<input ng-model="jb.fname" id="firstname" name="firstname" type="text" class="contact-input" pattern="[\w\s\u00C0-\u017F\-.,'`]{1,50}" title="Please enter your valid first name" required>
						</div>

						<div class="col-sm-2 contactfont">Last Name</div>
						<div class="col-sm-10">
							<span class="pull-right" ng-show="contactUsForm.lastname.$touched" ng-messages="contactUsForm.lastname.$error">
								<span class="badge bg-orange" ng-message="required">Required</span>
								<span class="badge bg-orange" ng-message="pattern">Invalid</span>
							</span>
							<input ng-model="jb.lname"  id="lastname" name="lastname" type="text" class="contact-input" pattern="[\w\s\u00C0-\u017F\-.,'`]{1,50}" title="Please enter your valid last name" required>
						</div>

						<div class="col-sm-2 contactfont">Email</div>
						<div class="col-sm-10">
							<span class="pull-right" ng-show="contactUsForm.email.$touched" ng-messages="contactUsForm.email.$error">
								<span class="badge bg-orange" ng-message="required">Required</span>
								<span class="badge bg-orange" ng-message="pattern">Invalid</span>
							</span>
							<input ng-model="jb.email" id="email" name="email" type="email" class="contact-input" pattern="^[^\s@]+@[^\s@]+\.[^\s@]{2,}$" title="Please enter your valid email address" required>
						</div>

						<div class="col-sm-2 contactfont">Message</div>
						<div class="col-sm-10">
							<span class="pull-right" ng-show="contactUsForm.content.$touched" ng-messages="contactUsForm.content.$error">
								<span class="badge bg-orange" ng-message="required">Required</span>
							</span>
							<textarea ng-model="jb.content" id="content" name="content" class="contact-input" placeholder="Type your message here..." required></textarea>
						</div>

						<div class="col-sm-offset-2 col-sm-5">
							<center>
								<span id="captcha-container">
									<span id="txtCaptchaDiv" class="captchatxt"></span>
									<input ng-model="str1" id="txtCaptcha" name="captcha" type="hidden"  />
									<input ng-model="str2" id="txtInput" name="captcha2" type="text"  name="txtInput" class="contact-input"   placeholder="Captcha" required>
									<span ng-show="contactUsForm.captcha2.$touched" ng-messages="contactUsForm.captcha2.$error">
										<span class="badge bg-orange" ng-message="required">Required</span>
									</span>
								</span>
							</center>
						</div>

						<div class="col-sm-5">
							<center>
								<button type="submit" class="submitbutton">
									<i class="glyphicon glyphicon-send"></i> SEND
								</button>
								<br><br>
								<i ng-show="err" class="glyphicon glyphicon-warning-sign" style="color:red; font-size:30px"></i>
								<div class="msgsent" ng-show="notify" ng-bind="msg"></div>
			          			<div class="msgerr" ng-show="err" ng-bind="msg1"></div>
							</center>
						</div>
					</form>
					<script type="text/javascript">
						function checkform(theform){
							var why = "";

							if(theform.txtInput.value == "") {
								why += "Captcha code should not be empty";
							}
							if(theform.txtInput.value != "") {
								if(ValidCaptcha(theform.txtInput.value) == false){
									why += "Captcha code did not match";
								}
							}
							if(why != ""){
								alert(why);
								return false;
							}
						}

						//Generates the captcha function

						function randomString() {
							var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
							var string_length = 6;
							var randomstring = '';
							for (var i=0; i<string_length; i++) {
								var rnum = Math.floor(Math.random() * chars.length);
								randomstring += chars.substring(rnum,rnum+1);
							}
							return randomstring;
						}

						var code = randomString();

						document.getElementById("txtCaptcha").value = code;
						console.log(document.getElementById("txtCaptcha").value, "HERE!!!!!!!!!!!!")
						document.getElementById("txtCaptchaDiv").innerHTML = code;

						// Validate the Entered input aganist the generated security code function
						function ValidCaptcha(){
							var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
							var str2 = removeSpaces(document.getElementById('txtInput').value);
							if (str1 == str2){
								return true;
							}else{
								return false;
							}
						}
						// Remove the spaces from the entered and generated code
						function removeSpaces(string){
							return string.split(' ').join('');
						}
						</script>
				</div>
			</div>
		</div>
</div>


<div class="container-fluid bg_yellow">
  <div class="row pad_sm">
    <div class="col-sm-12 center"><span id="call_us">CALL US! <?php echo $contact->contactno; ?></span></div>
  </div>
</div>

<div class="container-fluid no_margin" id="atvmap">
</div>
