<div class="container-fluid">
<div class="container">
    <div class="row">
      <div class="col-sm-11 col-sm-offset-1">
        <br>
        <span class="breadcrumbs" style="font-size:18px;">
          <a href="<?php echo $trails;?>" class="clr_yellow">Trails</a>
          &#x276f; <!-- arrow -->
          <?php if($trail->category == 'cottonwood') { $trail->category = 'sedona-Cottonwood'; }?>
          <a href="<?php echo $trailcategory;?>-<?php echo strtolower($trail->category);?>-arizona" class="clr_yellow"><?php echo ucwords($trail->category); ?></a>
          &#x276f; <!-- arrow -->
          <?php echo $trail->title; ?>
        </span>
      </div>
    </div>
		<div class="row">
			<div class="col-sm-5  col-sm-offset-1 pad_md_vert">
				<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $trail->featuredimage; ?>" class="img-responsive center-block" style="width:100%;">
				<div class="col-sm-6 col-xs-6  pad trailinfo_pad">
				<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $trail->firstimage; ?>" class="img-responsive margin_top">
				</div>
				<div class="col-sm-6 col-xs-6 pad1 trailinfo_pad1">
					<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $trail->secondimage; ?>" class="img-responsive margin_top">
				</div>
			</div>
			<div class="col-sm-5 pad_md_vert mv_centered">
				<span class="title"><?php echo $trail->title; ?></span><br>
				<p>
					<?php echo $trail->desc; ?>
				</p>
				<label class="clr_orange">Details</label> <br>
				<span class="details">
          <?php foreach ($map as $key => $zxc) {
					if($zxc->trailid == $trail->id){ ?>
					<a href="#" data-toggle="modal" data-target="#<?php echo $zxc->mapid; ?>">
            <span class="btn_orange mg_tb_xs"><?php echo $zxc->totalhr; ?> HR MAP</span>
          </a>
					<?php } } ?><br><br>
					<label class="clr_orange">Distance : </label> <?php echo $trail->distance; ?> miles

					<br>
					<label class="clr_orange">Length of time : </label>
					<?php
					$out = array();
					foreach ($map as $key => $p) {
						if($p->trailid == $trail->id){
							array_push($out,$p->totalhr.'Hrs');
						}
					} echo implode(', ', $out);
					?>
					<br>
					<label class="clr_orange">Difficulty level : </label> <?php if($trail->dlf == $trail->dlt){echo ucfirst($trail->dlf);}else{ echo ucfirst($trail->dlf) . ' to ' .ucfirst($trail->dlt); } ?>
					<br>
					<label class="clr_orange">Elevation : </label> <?php echo $trail->elevation; ?>

					<br>
					<label class="clr_orange">What to bring : </label> <?php echo $trail->wtb; ?>
				</span>

			</div>
		</div>
    <div class="row">
      <div class="col-sm-11 col-sm-offset-1">
          <div id="disqus_thread"></div>
      </div>
    </div>
		<?php foreach ($map as $key => $wew) {
					if($wew->trailid == $trail->id){ ?>
					<!-- Modal -->
					<div id="<?php echo $wew->mapid; ?>" class="modal fade" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title"><?php echo $trail->title; ?></h4>
								</div>
								<div class="modal-body">
									<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $wew->map; ?>" class="img-responsive center-block">
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>

						</div>
					</div>
					<!-- Modal -->
			<?php }
		} ?>

	</div>
	</div>
