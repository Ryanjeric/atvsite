<div class="container-fluid">
		<nav class="navbar navbar-default"  style="background:orange">
			<div class="container-fluid" id="navContainer">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#trailNavbar">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<span class="trail-header" style="display:inline-block;
																		color:white;
																		font-size:18px;
																		font-weight:700;
																		margin:12px 3px 0 7px;">TRAIL LOCATIONS: </span>
						</div>
						<center>
						<div class="collapse navbar-collapse" id="trailNavbar">
								<ul class="nav trail-nav nav-justified">
									<li><a href="<?php echo $trail; ?>" class="nav_a">ALL</a></li>
                  <li><a href="<?php echo $sedona; ?>" class="nav_a">SEDONA</a></li>
                  <li><a href="#" class="nav_a active"><span id="cottonwood">SEDONA & COTTONWOOD</span></a></li>
                  <li><a href="<?php echo $jerome; ?>" class="nav_a">JEROME</a></li>
                  <li><a href="<?php echo $cornville; ?>" class="nav_a">CORNVILLE</a></li>
								</ul>
						</div>

			</div>
		</nav>
</div>
<?php
if($trails!='NODATA'){
  foreach ($trails as $key => $value) {

  	if (($key & 1)== 0) {
?>
<div class="container-fluid">
<div class="container">
		<div class="row">
			<div class="col-sm-5 col-sm-offset-1 pad_md_vert">
				<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $value->featuredimage; ?>" class="img-responsive center-block">
				<div class="col-sm-6 pad">
				<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $value->firstimage; ?>" class="img-responsive margin_top">
				</div>
				<div class="col-sm-6 pad1">
					<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $value->secondimage; ?>" class="img-responsive margin_top">
				</div>

			</div>
			<div class="col-sm-5 pad_md_vert mv_centered">
				<a class="blacklink" href="<?php echo $trail; ?>/<?php echo $value->slugs; ?>"><span class="title"><?php echo $value->title; ?></span></a> <br>
				<p>
					<?php echo $value->desc; ?>
				</p>
				<label class="clr_orange">Details</label> <br>
				<span class="details">
					<?php foreach ($map as $key => $zxc) {
					if($zxc->trailid == $value->id){ ?>
					<a href="#" data-toggle="modal" data-target="#<?php echo $zxc->mapid; ?>">
						<span class="btn_orange mg_tb_xs"><?php echo $zxc->totalhr; ?> HR MAP</span>
					</a>
					<?php } } ?><br><br>
					<label class="clr_orange">Distance : </label> <?php echo $value->distance; ?> miles

					<br>
					<label class="clr_orange">Length of time : </label>
					<?php
					$out = array();
					foreach ($map as $key => $p) {
						if($p->trailid == $value->id){
							array_push($out,$p->totalhr.'Hrs');
						}
					} echo implode(', ', $out);
					?>
					<br>
					<label class="clr_orange">Difficulty level : </label> <?php if($value->dlf == $value->dlt){echo ucfirst($value->dlf);}else{ echo ucfirst($value->dlf) . ' to ' .ucfirst($value->dlt); } ?>
					<br>
					<label class="clr_orange">Elevation : </label> <?php echo $value->elevation; ?>

					<br>
					<label class="clr_orange">What to bring : </label> <?php echo $value->wtb; ?>
				</span>

			</div>

		</div>
		<?php foreach ($map as $key => $wew) {
					if($wew->trailid == $value->id){ ?>
					<!-- Modal -->
					<div id="<?php echo $wew->mapid; ?>" class="modal fade" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title"><?php echo $value->title; ?></h4>
								</div>
								<div class="modal-body">
									<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $wew->map; ?>" class="img-responsive center-block">
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>

						</div>
					</div>
					<!-- Modal -->
			<?php }
		} ?>


	</div>
	</div>
	<?php } else{ ?>
	<div class="container-fluid no-margin bg_yellowish">
	<div class="container">
		<div class="row">
			<div class="col-sm-5 col-sm-offset-1 pad_md_vert">
				<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $value->featuredimage; ?>" class="img-responsive center-block">
				<div class="col-sm-6 pad">
				<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $value->firstimage; ?>" class="img-responsive margin_top">
				</div>
				<div class="col-sm-6 pad1">
					<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $value->secondimage; ?>" class="img-responsive margin_top">
				</div>

			</div>
			<div class="col-sm-5 pad_md_vert mv_centered">
				<a class="blacklink" href="<?php echo $trail; ?>/<?php echo $value->slugs; ?>"><span class="title"><?php echo $value->title; ?></span></a> <br>
				<p>
					<?php echo $value->desc; ?>
				</p>
				<label class="clr_orange">Details</label> <br>
				<span class="details">
					<?php foreach ($map as $key => $zxc) {
					if($zxc->trailid == $value->id){ ?>
					<a href="#" data-toggle="modal" data-target="#<?php echo $zxc->mapid; ?>">
						<span class="btn_orange mg_tb_xs"><?php echo $zxc->totalhr; ?> HR MAP</span>
					</a>
					<?php } } ?><br><br>
					<label class="clr_orange">Distance : </label> <?php echo $value->distance; ?> miles

					<br>
					<label class="clr_orange">Length of time : </label>
					<?php
					$out = array();
					foreach ($map as $key => $p) {
						if($p->trailid == $value->id){
							array_push($out,$p->totalhr.'Hrs');
						}
					} echo implode(', ', $out);
					?>
					<br>
					<label class="clr_orange">Difficulty level : </label> <?php if($value->dlf == $value->dlt){echo ucfirst($value->dlf);}else{ echo ucfirst($value->dlf) . ' to ' .ucfirst($value->dlt); } ?>
					<br>
					<label class="clr_orange">Elevation : </label> <?php echo $value->elevation; ?>

					<br>
					<label class="clr_orange">What to bring : </label> <?php echo $value->wtb; ?>
				</span>

			</div>

		</div>
		<?php foreach ($map as $key => $wew) {
					if($wew->trailid == $value->id){ ?>
					<!-- Modal -->
					<div id="<?php echo $wew->mapid; ?>" class="modal fade" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title"><?php echo $value->title; ?></h4>
								</div>
								<div class="modal-body">
									<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $wew->map; ?>" class="img-responsive center-block">
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>

						</div>
					</div>
					<!-- Modal -->
			<?php }
		} ?>


	</div>
	</div>

<?php }}}else{ ?>
<div class="container-fluid">
<div class="container">
		<div class="row">
			<div class="row row-centered">
			<div class="col-sm-12 pad_md">
				<center><span class="rocky_title"><strong>No Trails yet...</strong></span></center>
			</div>
			</div>
		</div>
</div>
</div>

<?php } ?>
