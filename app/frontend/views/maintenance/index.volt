<?php
if($logoimage->value1 == 0){
  header('Location: /../');
}
?>
<!DOCTYPE html>
<html lang="en" data-ng-app="atvapp">
<head>
    <meta charset="utf-8">
    <title>RioverdeATV</title>
    <meta name="title" content="Rioverde ATV">
    <meta name="keywords" content="maintenance,comeback,later">
    <meta name="description" content="RIOVERDE ATVE RENTAL">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="user-scalable = yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <!-- Styles -->
    <!-- Bootstrap CSS -->
    <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font awesome CSS
    <link href="/vendors/fontawesome/css/font-awesome.min.css" rel="stylesheet">-->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/vendors/angularjs-toaster/toaster.css">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Palanquin+Dark:400,700,600,500' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Piedra' rel='stylesheet' type='text/css'>
    <link href="/vendors/fullcalendar/dist/fullcalendar.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/fe/css/style.atv.css" rel="stylesheet">
    <link href="/fe/css/navigation-media.css" rel="stylesheet">
    <link href="/fe/css/header-media.css" rel="stylesheet">
    <link href="/fe/css/author-media.css" rel="stylesheet">

    <!-- plugins CSS -->
    <link href="/vendors/lightgallery/src/css/lightgallery.css" rel="stylesheet">
    <link href="/vendors/fancyBox/source/jquery.fancybox.css" rel="stylesheet">
    <!-- Stylesheet for Color -->
    <link href="/fe/css/blue.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Favicon -->
    <link rel="shortcut icon" href="/img/frontend/toppage/logo.png">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="background-image"></div>
	<div class="container content">
		<div class="row">
			<div class="col-sm-12">
				<center><br><br><br><br><img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $logoimage->logo; ?>" class="img-responsive"></center>
			</div>
			<div class="col-sm-12">
				<center><br><br><h2>Rioverde ATV website is under maintenance</h2></center>
				<center><br><br><h3>"<?php echo $logoimage->value2; ?>"</h3></center>

				<center><br><h3>Stay Connected</h3><li class="socials_li">
                  <a href="https://www.facebook.com/rioverdeatv" target="_blank"><img src="/img/frontend/social-icons/fb.png" class="social-icons"></a>
                  <a href="https://www.youtube.com/rioverdeatv" target="_blank"><img src="/img/frontend/social-icons/yt.png" class="social-icons"></a>
                  <a href="https://plus.google.com/+Rioverdeatv" target="_blank"><img src="/img/frontend/social-icons/g+.png" class="social-icons"></a>
                </li></center>
			</div>
		</div>
	</div>
</body>
</html>