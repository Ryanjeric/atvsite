<div class="container-fluid no-margin padbot">
	<div class="container">
		<div class="row row-centered">
			<div class="col-sm-12 pad_md">
				<center><span class="rocky_title"><strong>Special Offers</strong></span></center>
			</div>
			<div class="col-sm-9 pad_md_z col-centered">
				<div class="row row-centered whitepanel lightgray">
					<div class="col-sm-9 col-centered" style="font-size:20px;font-weight:bold">
						<?php echo $special->content; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>