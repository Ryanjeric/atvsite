<div class="container" ng-controller="blogviewCtrl" >
		<div class="row">
		<div class="col-sm-12" id="view">
			<div class="col-sm-9 pad_md_vert" ng-show="archieveshow">
        <span class="newstitle theride">{{news.title}}</span>
				<p>{{news.summary}}</p>
				<p><span class="clr_yellow">Date: </span>{{news.datepublished}}</span> | <span class="clr_yellow" >Author:</span> {{news.name}}</p>
				<hr>

				{% if news.featuredtype == "img" %}

						<img src="{{featimage}}" class="pinterest-img"> {#for pinterest use, this is not visible#}

						<a class="fancybox-effects-a" href="{{featimage}}">
							<div style="background:#000 url('{{featimage}}');" id="featimage"></div>
						</a>
				{% elseif news.featuredtype == "vid" %}
					<div id="featvideo">
					{{news.video}}
					</div>
				{% endif  %}

				<br><br>
				<div class="body">
					{{news.body}}
				</div>
				<br><br>
				Category:
				{% for cat in news.categories %}
					<span class="categories"><a ng-click="gotocategory('{{cat.id}}', '{{cat.categoryname}}')" class="clr_orange">{{cat.categoryname}}</a></span>{% if loop.last == false %},{% endif  %}
				{% endfor %}
				<br>
				Tags:
				{% for tags in news.tags %}
					<span class="tags"><a ng-click="gototag('{{tags.id}}', '{{tags.tagname}}')" class="clr_orange">{{ tags.tagname }}</a></span>{% if loop.last == false %},{% endif  %}
				{% endfor %}
				<hr class="styled-hr">

				<div id="aboutauthor">
					<h4 class="abouttheauthor">About The Author</h4>
					<div class="col-sm-2">
						<a href="/blog/author/{{author.id}}"><img src="{{ authors3 }}{{ author.photo }}" id="authorphoto" err-SRC="/img/a0.jpg"/></a>
						<br><br>
					</div>
					<div class="col-sm-10">
						<span class="authorname"><a href="/blog/author/{{author.id}}" class="clr_orange">{{author.name}}</a></span>
						<span>{{author.about}}</span>
					</div>
				</div>

				<hr class="styled-hr">

				{#socialbuttons#}
				<div class="socialbuttons">
					<div class="button-div" facebook>
						<div class="fb-like" data-href="/blog/{{news.slugs}}" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
					</div>

					<div class="button-div" twitter>
						<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
					</div>

					<div class="button-div" pinterest>
						<a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark">
						<img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>
						<!-- PINTEREST -->
						<script
							type="text/javascript"
							async defer
							data-pin-debug="true"
							src="//assets.pinterest.com/js/pinit.js"
						></script>
						<!-- PINTEREST ends -->
					</div>
				</div>
				{#socialbuttons ends#}

				<div id="disqus_thread"></div>
			</div>


			<div class="col-sm-9 pad_md_vert" ng-hide="archieveshow">
				<h4 class="blog-title-list theride">{[{ newstitle }]}</h4>
				<div class="list-news-wrapper" ng-show="meron">
					<div ng-repeat="data in list | limitTo: limit" class="row list-title-blog ng-scope">
						<div ng-if="data.type=='img'" class="col-sm-3 news-thumb-container" style="background-image: url('{[{ s3 }]}{[{ data.featuredbanner }]}')" ng-click="redirectNews(data.slugs);" title="{[{ data.title }]}">
							<img src="{[{ s3 }]}{[{ data.featuredbanner }]}" class="pinterest-img" alt="{[{ data.title }]}">
							<img class="pinterest-img" alt="{[{ data.title }]}">
							<a href="/blog/view/{[{ data.slugs }]}">
							</a>
						</div>

						<div ng-if="data.type=='vid'" class="col-sm-3 news-thumb-container" style="background-image: url('{[{ data.featuredbanner }]}')" ng-click="redirectNews(data.slugs);" title="{[{ data.title }]}">
							<img src="{[{ data.featuredbanner }]}" class="pinterest-img" alt="{[{ data.title }]}">
							<img class="pinterest-img" alt="asdasd'sadasd">
							<div class="youtube-play"><img src="/img/youtubeplay.png" ng-click="redirectNews(news.slugs);"></div>
							<a href="/blog/view/{[{ data.slugs }]}">
							</a>
						</div>

						<div class="col-xs-8 col-md-7 news-list-desc">
							<div class="rowz row">
								<div class="col-sm-12">
									<span class="size25 font1 news-title ng-binding" ng-click="redirectNews(data.slugs);">{[{ data.title }]}</span>
								</div>
								<div class="col-sm-12 ng-binding">
									<strong><span style=" white-space: pre-line;" class="thin-font1 orange">{[{ data.category }]}</span></strong><span ng-show="data.author !=''" class="thin-font1"> / by <a href="/blog/author/{[{data.authorid}]}"><strong><span class="orange ng-binding">{[{ data.author }]}</span></strong></a></span> / {[{ data.datepublished  | dateToISO | date: format:'EEEE, MMMM d, y'}]}
									<br>
									<p>{[{ data.summary }]}</p>
								</div>
								<div class="col-sm-12">
									<div class="font1 size14 summary ng-binding">
										<br><br>
									</div>
								</div>
							</div>
							<div style="clear:both"></div><br>
						</div>
					</div>
				</div>
				<div class="list-news-wrapper" ng-show="!meron">
					NO NEWS
				</div>
				<button ng-hide="hideloadmore" class="news-list-show-more" ng-click="showmorenews()" ng-disabled="loading"><span ng-show="loading">Loading...</span><span ng-hide="loading">Show More</span></button>
			</div>

			<div class="col-sm-3 pad_md_vert">
				<h4 class="blog-title-list theride">Categories</h4>
				<div class="categorylist-wrapper">
					<a href="{[{ BaseURL }]}/blog" ng-click="changecat(undefined)"><span class="orange2">All</span><br></a>
					<a ng-repeat="data in categ" ng-click="gotocategory(data.id, data.categoryname)"><span class="orange2">{[{ data.categoryname }]}</span><br></a>
					<br>
				</div>
				<hr class="styled-hr">
				<span class="fa fa-calendar fa-1x"></span>  <span class="theride">Archives</span>
				<div class="ul-archives">
					<a ng-repeat="data in archieve" ng-click="gotoarchive(data.datepublished, data.month, data.year)">
						<span class="fa fa-chevron-right orange2"></span>
						<span class="orange2">{[{ data.month }]} {[{ data.year }]}</span>
						<br>
					</a>
					<br>
				</div>
        <hr class="styled-hr">
				<span class="fa fa-tag fa-1x"></span> <span class="theride"> Tags</span> <br>
					<a ng-repeat="data in listtags" ng-click="gototag(data.id, data.tagname)">
						<span ng-class-odd="'odd'" ng-class-even="'even'">{[{ data.tagname }]} </span>
					</a>
					<br><br>
					<hr class="styled-hr">
				<a href="{[{ BaseURL }]}/rss" target="_blank"><h4 class="blog-title-list theride"><img src="/img/rss.png" class="rssfeed">RSS FEED</h4></a>
			</div>
		</div>
		</div>
</div>

<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'rioverdeatv';

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
