<div class="container" ng-controller="archiveCtrl">
		<div class="row">
		<div class="col-sm-12">
			<div class="col-sm-9 pad_md_vert">
				<h4 class="blog-title-list theride" ng-bind="newstitle"></h4>
				<div class="list-news-wrapper" ng-show="meron">
					<div ng-repeat="data in list | limitTo: limit | orderBy: '-datepublished'" class="row list-title-blog ng-scope">
						<div ng-if="data.type=='img'" class="col-sm-3 news-thumb-container" style="background-image: url('{[{ s3 }]}{[{ data.featuredbanner }]}')" ng-click="redirectNews(data.slugs);" title="{[{ data.title }]}">
							<img src="{[{ s3 }]}{[{ data.featuredbanner }]}" class="pinterest-img" alt="{[{ data.title }]}">
							<img class="pinterest-img" alt="{[{ data.title }]}">
							<a href="/blog/{[{ data.slugs }]}">
							</a>
						</div>

						<div ng-if="data.type=='vid'" class="col-sm-3 news-thumb-container" style="background-image: url('{[{ data.featuredbanner }]}')" ng-click="redirectNews(data.slugs);" title="{[{ data.title }]}">
							<img src="{[{ data.featuredbanner }]}" class="pinterest-img" alt="{[{ data.title }]}">
							<img class="pinterest-img" alt="asdasd'sadasd">
							<div class="youtube-play"><img src="/img/youtubeplay.png" ng-click="redirectNews(news.slugs);"></div>
							<a href="/blog/{[{ data.slugs }]}">
							</a>
						</div>

						<div class="col-xs-8 col-md-7 news-list-desc">
							<div class="rowz row">
								<div class="col-sm-12">
									<span class="size25 font1 news-title ng-binding" ng-click="redirectNews(data.slugs);" ng-bind="data.title"></span>
								</div>
								<div class="col-sm-12 ng-binding">
									<strong><span style=" white-space: pre-line;" class="thin-font1 orange" ng-bind="data.category"></span></strong><span ng-show="data.author !=''" class="thin-font1"> / by <a href="/blog/author/{[{data.authorid}]}"><strong><span class="orange ng-binding" ng-bind="data.author"></span></strong></a></span> / <span ng-bind="data.datepublished  | dateToISO | date: format:'EEEE, MMMM d, y'"></span>
								</div>
								<div class="col-sm-12">
									<br>
									<div class="font1 size14 summary ng-binding">
										<p ng-bind="data.summary"></p>
									</div>
								</div>
							</div>
							<div style="clear:both"></div><br>
						</div>
					</div>
				</div>
				<div class="list-news-wrapper" ng-show="!meron">
					NO NEWS
				</div>
				<button ng-hide="hideloadmore" class="news-list-show-more" ng-click="showmorenews()" ng-disabled="loading"><span ng-show="loading">Loading...</span><span ng-hide="loading">Show More</span></button>
			</div>
			<div class="col-sm-3 pad_md_vert">
				<h4 class="blog-title-list theride">Categories</h4>
				<hr class="styled-hr">
				<div class="categorylist-wrapper">
					<a href="{[{ BaseURL }]}/blog" ng-click="changecat(undefined)"><span class="orange2">All</span><br></a>
					<a ng-repeat="data in categ" ng-click="gotocategory(data.id, data.categoryname)"><span class="orange2" ng-bind="data.categoryname"></span><br></a>
					<br>
				</div>
				<hr class="styled-hr">
				<span class="fa fa-calendar fa-1x"></span>  <span class="theride">Archives</span>
				<div class="ul-archives">
					<a ng-repeat="data in archieve" ng-click="gotoarchive(data.datepublished, data.month, data.year)">
            <span class="fa fa-chevron-right orange2"></span>
            <span class="orange2"><span ng-bind="data.month"></span>&nbsp<span ng-bind="data.year"></span></span>
            <br>
          </a>
					<br>
				</div>
				<hr class="styled-hr">
				<span class="fa fa-tag fa-1x"></span> <span class="theride"> Tags</span> <br>
					<a ng-repeat="data in listtags" ng-click="gototag(data.id, data.tagname)"><span ng-class-odd="'odd'" ng-class-even="'even'" ng-if="data.tagname!=''">{[{ data.tagname }]} </span></a>
					<br><br>
					<hr class="styled-hr">
				<a href="{[{ BaseURL }]}/rss" target="_blank"><h4 class="blog-title-list theride"><img src="/img/rss.png" class="rssfeed">RSS FEED</h4></a>
			</div>
		</div>
		</div>
</div>
