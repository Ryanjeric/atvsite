<div class="container" id="authorpage" ng-controller="authorCtrl">
  <div class="row">
    <div class="col-md-12">
      <div class="authorphoto" style="background:#e84e1a url('{{ authors3 }}{{ author.photo }}');">
      </div>
      <center>
        <p id="authorname" ng-bind="author.name"></p>

        <p><span class="clr_yellow">Location:</span> <span id="location" ng-bind="author.location"></span></p>
        <p><span class="clr_yellow">Occupation:</span> <span id="occupation" ng-bind="author.occupation"></span></p>
      </center>
    </div>

    <div class="col-md-12">
      <div class="alittleabout">
        A little about me
      </div>
      <div ng-bind-html="author.about"></div>
    </div>

    <div ng-show="ngdata" class="col-sm-12">
      <div class="col-md-12"
          style="padding-left:10px;
                  font-size:17px;
                  color:#f9b234;
                  font-weight:600;
                  border-style:solid;
                  border-width:1.5px 0 1.5px 0;
                  border-color:#BBB;
                  padding:2px 0 2px 15px;
                  margin:2% 0">
        THE NEWS
      </div>
      <div ng-repeat="blog in myblogs" class="container wrapper-md authorsblogs pointer">
        <div class="col-sm-1" style="font-size:20px; font-weight:bold; color:orange;" ng-bind="$index + 1"></div>

        <div ng-show="blog.featuredtype == 'img' " class="col-sm-3 featured" ng-click="goto(blog.slugs)"
              style="background:#000 url('{[{newss3}]}{[{featured[$index]}]}');
                                      background-size:cover;
                                      background-repeat:no-repeat;
                                      background-position:center center;
                                      min-height:180px;">
            {#<img src="{{ newss3 }}{{ featured[index] }}" class="img-responsive">#}
            <a href="/blog/{[{blog.slugs}]}" class="clr_orange"></a>
        </div>

        <div ng-show="blog.featuredtype == 'vid' " class="col-sm-3 featured"   ng-click="goto(blog.slugs)"
                style="background:#000 url('{[{featured[$index]}]}');
                                      background-size:cover;
                                      background-repeat:no-repeat;
                                      background-position:center center;
                                      min-height:180px;
                                      text-align:center;">
            <img src="/img/frontend/social-icons/yt.png" style="width:38px; height:38px; border-radius:5px;  margin:30% auto">
        </div>

        <div class="col-sm-8">
            <span class="blogtitle"><a href="/blog/{[{blog.slugs}]}" class="clr_orange" ng-bind="blog.title"></a></span> <br>

            {#Must be one line only#}
            <span ng-repeat="cat in categories[$index]" class="categories"><span ng-if="$index != 0">, </span><span ng-bind="cat.name"></span></span>

            / by <span class="authorname" ng-bind="blog.name"></span>
            / <span ng-bind="datepublished[$index]"></span>
            <br><br>
            <p ng-bind="blog.summary"></p>
        </div>
        <div class="col-sm-12"><br><br></div>
      </div> <!-- end of myblogs ng-repeat -->
      <div class="col-sm-10 col-sm-offset-1">

        <button ng-show="items < totalblogs" class="news-list-show-more" ng-click="showmorenews(items, curldata = true)" ng-disabled="loading">
          <span ng-show="loading">Loading...</span><span ng-hide="loading">Show More</span>
        </button>

      </div>

    </div>
    <div id="bottom"></div>

  </div>
</div>
<br><br>
