<div class="container-fluid no-margin padbot">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 pad_md">
				<center><span class="rocky_title"><strong>Videos</strong></span></center>
			</div>
			<div class="col-sm-12 pad_md_z mv_centered">
				<div id="video">
					<?php foreach($vid as $key => $list){
						//MAGICK
						$x = preg_match('/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/i',$list->embed,$match);
						$img = 'http://img.youtube.com/vi/'. $match[1] .'/hqdefault.jpg';
						$url = $match[1];
						?>
						<a class="col-sm-4 col-md-3 col-xs-6 gal2" href="https://www.youtube.com/watch?v=<?php echo $url; ?>">
							<img src="<?php echo $img; ?>" class="imgz img-responsive">
							<img src="/img/youtubeplay.png" class="youtube-play-gallery">
						</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div ng-controller="galCtrl" class="container-fluid no-margin bg_yellowish padbot">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 pad_md">
				<center><span class="rocky_title"><strong>Images</strong></span></center>
			</div>
			<div class="col-sm-12 pad_md_z mv_centered">
				<div id="gal">
					<a lightgallery ng-repeat="img in imgs | limitTo : limit" class="col-sm-4 col-md-3 col-xs-6 gal" href="<?php echo $this->config->application->amazonlink; ?>/uploads/images/{[{ img.filename }]}">
						{#<img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $gal->filename; ?>" alt="rio verde atv #<?php echo $gal->id; ?>" class="img-responsive"><div class="bg-img img-gallery" style="background:url(<?php echo $this->config->application->amazonlink; ?>/uploads/images/{[{ img.filename }]})"></div> #}
						<img class="bg-img img-gallery img-responsive" src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/{[{ img.filename }]}">
					</a>
				</div>
				<div class="col-sm-12 pad_md_z mv_centered">
					<button ng-hide="hideloadmore" class="gal-list-show-more" ng-click="showmoregal();" ng-disabled="loading">
					<span ng-show="loading">Loading...</span><span ng-hide="loading">Show More</span>
				</button>
				</div>
			</div>
		</div>
	</div>
</div>
