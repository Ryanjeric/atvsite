<div class="container-fluid">
  <div class="container">
    <div class="row">
      <div class="terms-of-use">
        <div class="col-sm-12">
          <h2>Posted June 23, 2016.</h2>
          <h1 class="SedonaVortexHealingATV">SedonaVortexHealingATV.com Privacy Policy</h1>
          <p>
            Thank you for visiting our web site. This Privacy Policy pertains to Vortex Healing ATV Adventure, LLC operating the websites SedonaVortexHealingATV.com and SedonaVortexHealingATV.com and its members, successors, assigns, affiliates, subsidiaries, and business licensors (collectively and individually referred to herein as "VortexHealingATV").
          </p>
          <p>
            VortexHealingATV is committed to protecting and safeguarding your privacy on this website and this Privacy Policy describes VortexHealingATV’s policies and practices in collecting, using, and safeguarding the private information that may be obtained through use of this website.
          </p>
          <p>
            From time to time, VortexHealingATV operates these websites and related promotions in connection with STI Network, Inc., Vortex AMC LLC, Body & Brain Yoga & Health Centers, Inc., Body and Brain Center, LLC and its franchisees, and each of their members, successors, assigns, affiliates, subsidiaries, and business licensors and this Privacy Policy applies to each of these entities with respect to their involvement with this website, but does not apply to other third party websites accessible via hyperlinks from this web site, which may have separate privacy policies.
          </p>

          <div class="termsofuse-content">


            <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>1. Consent to this Privacy Policy</b>
            <p>
              By using our web site in any way, such as browsing or interacting, you signify that you agree with the terms of our current Privacy Policy as posted here. If you do not agree with this Privacy Policy, you should not use this web site. VortexHealingATV reserves the right to change or remove this Privacy Policy at our discretion. Our Privacy Policy may change from time to time. However, we will not materially change this Privacy Policy to make them less protective of customer information collected in the past without the consent of affected customers. We encourage you to visit this area frequently to stay informed as your continued use of our web site following the posting of changes to these terms means that you consent to such changes. To make this policy easy to find, we make it available on our home page and at every point where personally identifiable information may be requested.
            </p>

            <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>2. What Information Is Collected</b>
            <p>We collect two types of information about visitors:</p>

            <p>
              a. <u>Personally Identifiable Information:</u> Personally identifiable information is any information which can be used to identify an individual, such as a name, address, telephone number, social security number, billing and shipping information, credit card information, e-mail address or other information that you voluntarily provide to us about yourself. When you visit our web site, we will not collect any personally identifiable information about you unless you provide it to us voluntarily. You can visit our web sites without telling us who you are or revealing personally identifiable information about yourself, in which case you may however be unable to participate in certain product promotions or receive product information. From time to time, VortexHealingATV may offer sweepstakes, contests and surveys. Participation in these promotions is completely voluntary and the user therefore has a choice whether or not to disclose this information. In the event you wish to enter a sweepstakes, contest or survey but do not wish us to maintain your personal information for any purpose unrelated to the sweepstakes, contest or survey, you may notify us, and provide a description of the specific sweepstakes, contest or survey to: <a href="#">VortexHealingATV@gmail.com</a>.
            </p>
            <p>
              Please note that we will only collect personally identifiable information revealing social security numbers, driver’s license information, racial or ethnic origin, financial information, political opinions, religious or philosophical beliefs, union membership, health information, sex life or criminal convictions with your explicit opt in consent, and only to the extent necessary for the provision of the services offered by our web site. If you apply for a US employment position via our careers center you may be asked to provide information on your gender or race where permitted by law. You should understand that, if provided, this information will only be used in accordance with applicable law. Providing this information is strictly voluntary and you will not be subject to any adverse action or treatment if you choose not to provide this information.
            </p>
            <br>
            <p>
              b. <u>Aggregate and Statistical Data:</u> VortexHealingATV may collect certain non-personally identifiable aggregate data called web log information (such as your web browser, operating system, pages visited, etc.) and use cookies or web beacons (see definitions below) when you visit some of our web pages. For instance, when you visit one of our web sites, our webserver will automatically recognize some non-personal information, including but not limited to the date and time you visited our site, the pages you visited, the referring website you came from, the type of browser you are using, the type of operating system you are using and the domain name and address of your internet server. We also may collect your Internet Protocol (IP) address. An IP address is a number that is assigned to your computer when you use the Internet. The IP address data that we collect does not contain any personally identifiable information about you and is used to administer our site, to determine the numbers of different visitors to the site and to gather broad demographic data. We do not use cookies to retrieve information that is unrelated to your visit to or your interaction with this web site.
            </p>
            <p>
              In some cases we may also collect information that you voluntarily submit, such as general statistical information (e.g., age, gender, household size, zip/postal code, preferences or interests).
            </p>
            <br>



            <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>3. How Information Is Collected</b>
            <p>
              <u>Personally Identifiable Information:</u> VortexHealingATV collects personally identifiable information from our web site visitors only on a voluntary basis. Such information may be collected from you if you choose to participate in the following via our web site: web site registrations, contests, sweepstakes, questionnaires, surveys, consumer service contacts, employment inquiries, product purchases, or other similar activities requiring the submission of personally identifiable information. We may enhance or merge the information that you provide with information about you obtained from third parties for the same purpose.
            </p>
            <p>
              <u>Aggregate and Statistical Data:</u> Cookies and web beacons are used to collect non-personal information automatically from visitors to our site. A "cookie" is a small piece of data that can be sent by a webserver to your computer, which then may be stored by your browser on your computer´s hard drive. Cookies help us in many ways to make your visit to our web site more convenient and meaningful to you. For example, cookies allow us to tailor a web site or advertisement to better match your interests and preferences, or to save you the trouble of re-entering certain information in some registration areas. Web beacons are electronic files on our web site that allow us to count users who have visited that page or to access certain cookies.
            </p>
            <p>
              Most Internet browsers enable you to erase cookies from your computer hard drive, block all cookies, or receive a warning before a cookie is stored. Please refer to your browser instructions or help screen to utilize or learn more about these functions. If you disable cookies, however, you may not be able to use certain personalized functions of this web site.
            </p>

            <p>
              Statistical non-personally identifying information (e.g., gender, zip code, etc.) is only collected if you voluntarily submit it to us.
            </p>


            <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>4. Use of Information</b>
            <p>
              a. <u>Personally Identifiable Information:</u> We collect, maintain and use personally identifiable information you have
            </p>
            <p>
              voluntarily submitted to our web site for the following purposes:
            </p>

            <p>
              - helping to establish and verify the identity of users;
            </p>
            <p>
              - opening, maintaining, administering and servicing users profiles, accounts or memberships;
            </p>
            <p>
              - processing, servicing or enforcing transactions and sending related communications; - providing services and support to users;
            </p>
            <p>
              - providing you with information about employment opportunities, administering the application process and considering you for employment if you apply for a job or an internship;
            </p>
            <p>
              -to conduct sweepstakes, surveys and contests, and to provide the results thereof;
            </p>
            <p>
              - improving the web site, including tailoring it to users´ preferences;
            </p>
            <p>
              - providing users with product or service updates, promotional notices and offers, and other information about VortexHealingATV;
            </p>
            <p>
              - responding to your questions, inquiries, comments and instructions; and
            </p>
            <p>
              - maintaining the security and integrity of our systems.
            </p>

            <p>
              b. <u>Aggregate and Statistical Data:</u> VortexHealingATV uses web log information (such as your web browser, operating system, pages visited, etc.) to help us design our web site, to identify popular features, and for other managerial purposes. However, such information may also be used if necessary to help identify any person attempting to break into or damage our web site. We may share such information, possibly along with personally identifiable information, with law enforcement agencies if we believe that we have evidence of a violation of computer security or other laws.
            </p>



            <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>5. Data Sharing and Storage</b>
            <p>
              Information about our customers is an important part of our business, and we are not in the business of selling it to others. We do not share personally identifiable information with companies, organizations and individuals outside of VortexHealingATV unless one of the following circumstances apply.
            </p>

            <p>
              a. <u>To Service Providers:</u> VortexHealingATV may use other companies and individuals such as agents, contractors and affiliates to help it maintain and operate its web site, to provide you with services that you have requested, to perform outsourced recruitment functions for us, or for other reasons related to the operation of its business, and those companies and individuals may receive your personally identifiable information for that purpose. These companies and individuals are subject to our Privacy Policy and any other appropriate confidentiality and security measures, and are restricted from using this data in any way other than to provide services for VortexHealingATV.
            </p>
            <p>
              b. <u>For Legal Reasons:</u> We reserve the right to share personally identifiable information with companies, organizations or individuals outside of VortexHealingATV if we have a good-faith belief that access, use, preservation or disclosure of the information is reasonably necessary to: - meet any applicable law, regulation, legal process or enforceable governmental request.
            </p>

            <p>
              - enforce applicable Terms of Use, including investigation of potential violations.
            </p>
            <p>
              - detect, prevent, or otherwise address fraud, security or technical issues.
            </p>
            <p>
              - protect against harm to the rights, property or safety of VortexHealingATV, our users or the public as required or permitted by law.
            </p>

            <p>
              c. <u>For Business Transfers:</u> As we continue to develop our business, we might sell or buy business units. In such transactions, customer information generally is one of the transferred business assets but remains subject to the promises made in any pre-existing Privacy Notice (unless the customer consents otherwise).
            </p>
            <p>
              d. <u>With Your Consent:</u> We will share personal information with companies, organizations or individuals outside of VortexHealingATV when we have your consent to do so.
            </p>
            <p>
              We may share aggregated, non-personally identifiable information publicly and with our partners like publishers, advertisers or connected sites.
            </p>
            <p>
              Your personally identifiable information will be stored for a reasonable amount of time in VortexHealingATV databases located in the United States, and will be automatically transferred to these databases for storage and maintenance.
            </p>



            <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>6. Security</b>
            <p>
              VortexHealingATV is committed to keeping secure the data you provide us and will take reasonable precautions to protect your personal information from loss, misuse or alteration. Agents or contractors of VortexHealingATV who have access to your personal information in connection with providing services for VortexHealingATV are required to keep the information confidential and are not permitted to use this information for any other purpose than to carry out the services they are performing for VortexHealingATV. If transactions are offered on this site, transaction information is transmitted to and from us in encrypted form using industry-standard secure connections to help protect such information from interception. Please be aware, however, that any email or other transmission you send through the Internet cannot be completely protected against unauthorized interception.
            </p>


            <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>7. Children</b>
            <p>
              VortexHealingATV has no intention of collecting any personal information from individuals under 13 years of age, and users under age 13 should not submit any personal information to us. Where appropriate, VortexHealingATV will specifically instruct children not to submit such information on our web sites or advertisements. If a child has provided us with personal information, a parent or guardian of that child may contact us at the e-mail address or address listed in Section 10 of this Privacy Policy if they would like this information deleted from our records. We will use reasonable effort to delete the child´s information from our databases.
            </p>


            <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>8. Your Choices and Regarding Your Personal Information</b>
            <p>
              You are in control of the personally identifiable information you provide to VortexHealingATV via our web sites.
            </p>
            <p>
              If you have submitted personally identifiable information through a VortexHealingATV web site and you would like to have that information deleted from our databases, please contact our Privacy Coordinator via e-mail at <a href="#">VortexHealingATV@gmail.com</a>. We will then use reasonable efforts to remove your personal information from our files and confirm to you that your personally identifiable information has been deleted (save for an archival copy which is not accessible by you or third parties on the internet). The archival copy is retained only for as long as VortexHealingATV reasonably considers necessary for audit and record purposes. If third parties previously accessed your personally identifiable information we are not able to delete the information or copies thereof from their systems.
            </p>
            <p>
              You may also contact us through either of the above methods to request the personal information that VortexHealingATV has collected about you through its web sites. Before providing you with your personally identifiable information or correcting, updating or deleting such information, we reserve the right to verify and authenticate your identity and the personally identifiable information to which you have requested access. Access to or correction, updating or deletion of your personally identifiable information may be denied or limited by VortexHealingATV as otherwise permitted by applicable law. We will respond to information access requests within 30 days of receipt. If we require additional time to provide access to your information, we will acknowledge receipt of your request within 30 days and promptly supplement our response within the time period required by applicable law.
            </p>



            <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>9. Links to Other sites</b>
            <p>
              Our sites may contain links to other web sites. VortexHealingATV is not responsible for the privacy practices or the content of the other web sites. Please note that the privacy policies applicable to such third party web sites may differ significantly from the VortexHealingATV Privacy Policy, so we advise you to please read them carefully before using those sites. VortexHealingATV cannot be held liable for any actions by or content of such third party web sites.
            </p>
            <p>
              Our web pages might be equipped with plug-ins of the social network facebook.com, which is operated by Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA ("Facebook"). Such plug-ins might in particular be Facebook's "Like" button respectively. If you access one of our web pages that is equipped with such a plug-in, your internet browser will make a direct connection to the Facebook servers and the plug-in will be displayed on the screen through communication to your browser. The plug-in will inform the Facebook server which of our web pages you have visited. If you are a member of Facebook and are logged on to Facebook user account while visiting our web page, Facebook will relate this information to your Facebook user account. On making use of any of the plug-in functions (e.g. clicking the "Like" button, leaving a comment), this information will also be related to your Facebook user account. Further information on the collection and use of data by Facebook and on the rights and possibilities available to you for protecting your privacy in this context can be found in Facebook’s data protection information. If you want to avoid that Facebook is able to relate the visit of our web page to your Facebook user account, you have to log-off your Facebook user account prior to visiting our web pages.
            </p>


            <i class="fa fa-circle fa-xs bullets" aria-hidden="true"></i> &nbsp<b>10. Queries and Complaints</b>
            <p>
              VortexHealingATV is committed to working with consumers to obtain a fair and rapid resolution of any complaints or disputes about privacy. Please send us your questions or comments regarding our privacy practices by <a href="#">VortexHealingATV@gmail.com</a>.
            </p>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>
