<div class="container">
	<div class="row">
		<div class="col-sm-4 pad_md_vert mv_centered">
			<img src="/img/frontend/innerpage/rentalhours.png" id="rentalhours_icon">
			<span class="rocky_title" id="rentalhours_title">RENTAL HOURS</span>
			<br><br>
			
			{% for sessions in rentalhours %}

				<label class="clr_orange">{{ sessions[0].session  ~ ' (' ~ sessions[0].duration ~ ' HR)' }}</label>

				{% for s in sessions %}
					
					<span style="display:block; margin-left:11px;">
						{{ s.starttime ~ ' - ' ~ s.endtime }}
						<br>
						{{ s.description }}
					</span>
				
				{% endfor %}

				<br>
					
			{% endfor %}

			<?php
				echo $rentalmsg->content;
			?>
		</div>
		<div class="col-sm-4 pad_md_vert mv_centered">
			<img src="/img/frontend/innerpage/freebies.png" id="freebies_icon">
			<span class="rocky_title" id="freebies_title">FREEBIES</span>
			<br><br>
			<?php
				echo $freebies->content;
			?>
		</div>
		<div class="col-sm-4 pad_md_vert mv_centered">
			<img src="/img/frontend/innerpage/special.png" id="freebies_icon">
			<span class="rocky_title" id="freebies_title">WE DELIVER</span>
			<br><br>
			<?php
				echo $deliver->content;
			?>
		</div>
	</div>
</div>


<?php
  foreach ($atv as $key => $value) {

  	if (($key & 1)== 0) {
?>

<div class="container-fluid no-margin bg_yellowish">
	<div class="container">
		<div class="row">
			<div class="col-sm-5 col-sm-offset-1 pad_md_vert">
				<img alt="<?php echo $value->title; ?>" src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $value->picture; ?>" class="img-responsive center-block">
			</div>
			<div class="col-sm-5 pad_md_vert mv_centered">
				<span class="title"><?php echo $value->title; ?></span> <br>
				<label class="clr_orange"><?php echo $value->subtitle; ?></label> <br>
				<p>
					<?php echo $value->features; ?>
					<?php echo $value->desc; ?>
				</p>
				<label class="clr_orange">Rental Prices</label> <br>
				<span class="rental_prices">
				<?php foreach ($price as $key => $p) {
					if($p->atvid == $value->id){ ?>
					$<?php echo $p->price; ?> / <span class="clr_orange"><?php echo $p->totalhr; ?></span>
					<br>
				<?php }
				} ?>
				</span>
				<br>
				<a href="<?php echo $reservation; ?>"><span class="btn_orange">RESERVE ONLINE</span></a>
			</div>
		</div>
	</div>
</div>
<?php } else{
 ?>
 <div class="container-fluid no-margin">
	<div class="container">
		<div class="row">
			<div class="col-sm-5 col-sm-offset-1 pad_md_vert">
				<img alt="<?php echo $value->title; ?>" src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $value->picture; ?>" class="img-responsive center-block">
			</div>
			<div class="col-sm-5 pad_md_vert mv_centered">
				<span class="title"><?php echo $value->title; ?></span> <br>
				<label class="clr_orange"><?php echo $value->subtitle; ?></label> <br>
				<p>
					<?php echo $value->features; ?>
					<?php echo $value->desc; ?>
				</p>
				<label class="clr_orange">Rental Prices</label> <br>
				<span class="rental_prices">
				<?php foreach ($price as $key => $p) {
					if($p->atvid == $value->id){ ?>
					$<?php echo $p->price; ?> / <span class="clr_orange"><?php echo $p->totalhr; ?></span>
					<br>
				<?php }
				} ?>
				</span>
				<br>
				<a href="<?php echo $reservation; ?>"><span class="btn_orange">RESERVE ONLINE</span></a>
			</div>
		</div>
	</div>
</div>


<?php }} ?>



<div class="container-fluid no-margin bg_yellow">
	<div class="container">
		<div class="row pad_md_vert">
			<div class="col-sm-10 col-sm-offset-1">
				<span class="title" id="requirements_title">REQUIREMENTS</span>
				<ul class="requirements">
				<?php foreach($listreq as $list) { ?>
					<li><?php echo $list->requirements; ?></li>
				<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</div>
