<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class PrivacyPolicyController extends ControllerBase
{
    public function indexAction()
    {
      $this->view->script_google = $this->curl('/settings/script');
      $this->view->logoimage = $this->curl('/settings/managesettings');
      $this->view->metadata = (object) array('metatitle' => 'Privacy Policy | Vortex Healing ATV');
      $this->view->url = "privacy-policy";
    }
}
