<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class ContactusController extends ControllerBase
{
    public function indexAction()
    {
    	$this->view->special = $this->curl('/special/show');
        // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->metadata = $this->curl('/metadata/edit/4');
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $data = $this->curl('/fe/getbanner/6');
        $this->view->pagebanner = $data;
        $this->view->banner = $this->config->application->amazonlink . "/uploads/images/" .$data->banner;
        $this->view->activecontact= "active";
        $this->view->url = "contactus";
    }
}
