<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class AtvspricesController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->logoimage = $this->curl('/settings/managesettings');
    	  // $this->view->rentalhours = $this->curl('/atv/listrentals');
        $this->view->rentalhours = $this->curl('/atv/listrentalhours');
    	  $this->view->rentalmsg = $this->curl('/rentalmsg/show');
    	  $this->view->freebies = $this->curl('/freebies/show');
        $this->view->deliver = $this->curl('/deliver/show');
        $this->view->special = $this->curl('/special/show');
    	  $this->view->listreq = $this->curl('/requirements/felist');
    	 $this->view->atv = $this->curl('/atv/felist')->data;
    	 $this->view->price = $this->curl('/atv/felist')->data2;
         $data = $this->curl('/fe/getbanner/2');
         $this->view->pagebanner = $data;
         $this->view->banner = $this->config->application->amazonlink . "/uploads/images/" .$data->banner;
        // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->metadata = $this->curl('/metadata/edit/3');
        $this->view->activeprices = "active";
        $this->view->url = "atvsprices";
    }
}
