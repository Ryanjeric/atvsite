<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class BlogController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->metadata = 'nodata';
        $this->view->title = 'Blog |';
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->url = "blog";
    }
    public function viewAction($newsslugs)
    {
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $service_url = $this->config->application->ApiURL.'/fe/blog/view/' . $newsslugs;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->news = $decoded;
        $this->view->metadata = $decoded;
        /*$this->view->metadata->metadescription = $decoded->metadesc;*/
        $this->view->url = 'blog/view/'.$newsslugs;
        $this->view->author = $decoded->author;
        $this->view->amazonlink = "https://rioverdeatv.s3.amazonaws.com/uploads/news/";
        $this->view->authors3 = "https://rioverdeatv.s3.amazonaws.com/uploads/authors/";
        if($decoded->image !=null){
            $this->view->featimage = "https://rioverdeatv.s3.amazonaws.com/uploads/news/" . $decoded->image;
        }
    }
    public function categoryAction() {
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->metadata = 'nodata';
        $this->view->title = 'category';
    }
    public function archiveAction() {
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->metadata = 'nodata';
        $this->view->url = "blog";
        $this->view->title = 'archive';
    }
    public function tagsAction() {
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->metadata = 'nodata';
        $this->view->url = "blog";
         $this->view->title = 'tags';
    }
    public function authorAction($authorid) {
      $this->view->script_google = $this->curl('/settings/script');
      $this->view->logoimage = $this->curl('/settings/managesettings');
      $service_url = $this->config->application->ApiURL.'/fe/blog/author/' . $authorid . '/0';
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->view->author = $decoded->author;
        $this->view->blogs = $decoded->myblogs;
        $this->view->totalblogs = $decoded->mytotalblogs;
        $this->view->featured = $decoded->featured;
        $this->view->categories = $decoded->categories;
        $this->view->datepublished = $decoded->datepublished;
        $this->view->authors3 = "https://rioverdeatv.s3.amazonaws.com/uploads/authors/";
        $this->view->newss3 = "https://rioverdeatv.s3.amazonaws.com/uploads/news/";
        $this->view->metadata = 'nodata';
        $this->view->url = "blog";
    }
    public function previewAction() {
      $this->view->script_google = $this->curl('/settings/script');
      $this->view->logoimage = $this->curl('/settings/managesettings');
    }
}
