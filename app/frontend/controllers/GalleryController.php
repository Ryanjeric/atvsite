<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class GalleryController extends ControllerBase
{
    public function indexAction()
    {
    	$this->view->gallery = $this->curl('/gallery/list');
    	$this->view->vid = $this->curl('/gallery/vidlist');
    	$this->view->special = $this->curl('/special/show');
    	$this->view->metadata = $this->curl('/metadata/edit/5');
        // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $data = $this->curl('/fe/getbanner/4');
        $this->view->pagebanner = $data;
        $this->view->banner = $this->config->application->amazonlink . "/uploads/images/" .$data->banner;
        $this->view->activegallery = "active";
        $this->view->url = "gallery";
    }
}
