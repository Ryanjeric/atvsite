<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class RssController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->datarss = $this->curl('latest/rss') ;
    }
}
