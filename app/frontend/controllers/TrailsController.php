<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class TrailsController extends ControllerBase
{
  public function indexAction()
  {
    $this->view->script_google = $this->curl('/settings/script');
    $this->view->logoimage = $this->curl('/settings/managesettings');
    $this->view->trails = $this->curl('/trails/felist')->data;
    $this->view->map = $this->curl('/trails/felist')->data2;
    // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    $this->view->metadata = $this->curl('/metadata/edit/6');
    $data = $this->curl('/fe/getbanner/3');
    $this->view->pagebanner = $data;
    $this->view->banner = $this->config->application->amazonlink . "/uploads/images/" .$data->banner;
    $this->view->activetrails = "active";
    $this->view->url = "trails";
  }
  public function sedonaAction(){
    $this->view->script_google = $this->curl('/settings/script');
    $this->view->logoimage = $this->curl('/settings/managesettings');
    $this->view->trails = $this->curl('/trails/fecatinfo/sedona')->data;
    $this->view->map = $this->curl('/trails/fecatinfo/sedona')->data2;
    // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    $this->view->metadata = $this->curl('/metatrails/edit/1');
    $this->view->activetrails = "active";
    $this->view->url = "trails/sedona";
  }
  public function cottonwoodAction(){
    $this->view->script_google = $this->curl('/settings/script');
    $this->view->logoimage = $this->curl('/settings/managesettings');
    $this->view->trails = $this->curl('/trails/fecatinfo/cottonwood')->data;
    $this->view->map = $this->curl('/trails/fecatinfo/cottonwood')->data2;
    // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    $this->view->metadata = $this->curl('/metatrails/edit/2');
    $this->view->activetrails = "active";
    $this->view->url = "trails/cottonwood";
  }
  public function jeromeAction(){
    $this->view->script_google = $this->curl('/settings/script');
    $this->view->logoimage = $this->curl('/settings/managesettings');
    $this->view->trails = $this->curl('/trails/fecatinfo/jerome')->data;
    $this->view->map = $this->curl('/trails/fecatinfo/jerome')->data2;
    // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    $this->view->metadata = $this->curl('/metatrails/edit/3');
    $this->view->activetrails = "active";
    $this->view->url = "trails/jerome";
  }
  public function cornvilleAction(){
    $this->view->script_google = $this->curl('/settings/script');
    $this->view->logoimage = $this->curl('/settings/managesettings');
    $this->view->trails = $this->curl('/trails/fecatinfo/cornville')->data;
    $this->view->map = $this->curl('/trails/fecatinfo/cornville')->data2;
    // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    $this->view->metadata = $this->curl('/metatrails/edit/4');
    $this->view->activetrails = "active";
    $this->view->url = "trails/cornville";
  }
  public function infoAction($slugs) {
    $this->view->script_google = $this->curl('/settings/script');
    $this->view->logoimage = $this->curl('/settings/managesettings');
    $decoded = $this->curl('/fe/trail/infobyslugs/'.$slugs);
    $this->view->trail = $decoded->data;
    $this->view->map = $decoded->data2;
    $this->view->banner = $this->config->application->amazonlink . "/uploads/images/" . $decoded->data->featuredimage;
    $this->view->special_banner = true;
    $this->view->activetrails = "active";
    $this->view->url = "trails/info/".$id;
    $this->view->metadata = $this->curl('/fe/trail/infobyid/'.$id)->data;
  }
}
