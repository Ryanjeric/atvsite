<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;



class IndexController extends ControllerBase
{
    public function indexAction()
    {
    	$this->view->gallery = $this->curl('/gallery/list');
    	$this->view->vid = $this->curl('/gallery/vidlist');
    	$this->view->adventure = $this->curl('/adventure/show');
        $this->view->details = $this->curl('/fe/gethome');
        $this->angularLoader(array(
            'userfactory' => 'fe/scripts/factory/user.js'
        ));
        $this->view->metadata = $this->curl('/metadata/edit/1');
        $this->view->script_google = $this->curl('/settings/script');
        $this->view->logoimage = $this->curl('/settings/managesettings');
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

      // $this->view->pick("index/index");
    }
}
