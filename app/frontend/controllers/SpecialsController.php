<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class SpecialsController extends ControllerBase
{
    public function indexAction()
    {
    	$this->view->script_google = $this->curl('/settings/script');
    	$this->view->logoimage = $this->curl('/settings/managesettings');
    	$this->view->special = $this->curl('/special/show');
        // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->metadata = $this->curl('/metadata/edit/7');
        $data = $this->curl('/fe/getbanner/7');
        $this->view->pagebanner = $data;
        $this->view->banner = $this->config->application->amazonlink . "/uploads/images/" .$data->banner;
        $this->view->url = "specials";
    }
}

