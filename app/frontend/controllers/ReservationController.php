<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class ReservationController extends ControllerBase
{
    public function indexAction()
    {
    	$this->view->script_google = $this->curl('/settings/script');
    	$this->view->logoimage = $this->curl('/settings/managesettings');
    	$this->view->special = $this->curl('/special/show');
    	$this->view->metadata = $this->curl('/metadata/edit/2');
        $data = $this->curl('/fe/getbanner/5');
        $this->view->pagebanner = $data;
        $this->view->banner = $this->config->application->amazonlink . "/uploads/images/" .$data->banner;
    	$this->view->activereservation = "active";
    	$this->view->url = "reservation";
    	$this->view->listnotice = $this->curl('/reservationnotice/felist');
    }
}
