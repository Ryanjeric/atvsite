<?php

namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    public function initialize() {
      $this->view->banner = "/img/frontend/innerpage/bannersite.png";
      $this->view->special_banner = "";
      $this->view->activehome = "";
      $this->view->activeprices = "";
      $this->view->activetrails = "";
      $this->view->activegallery = "";
      $this->view->activereservation = "";
      $this->view->activecontact = "";
      $this->view->amazonlink = $this->config->application->amazonlink;
      //ROUTES
      $this->view->home = "/sedona-adventures";
      $this->view->atvsnprices = "/sedona-adventures-atv-and-prices";
      $this->view->trails = "/atv-trails-all-sedona-verde-valley-arizona";
      $this->view->trail = "/atv-trails-all-sedona-verde-valley-arizona"; //array to string conversion solution
      $this->view->trailx = "/atv-trails-all-sedona-verde-valley-arizona"; //array to string conversion solution
      $this->view->trailcategory = "/atv-trails";
      $this->view->sedona = "/atv-trails-sedona-arizona";
      $this->view->cottonwood = "/atv-trails-sedona-cottonwood-arizona";
      $this->view->jerome = "/atv-trails-jerome-arizona";
      $this->view->cornville = "/atv-trails-cornville-arizona";
      $this->view->gallery = "/sedona-adventures-gallery";
      $this->view->galleryx = "/sedona-adventures-gallery"; //array to string conversion solution
      $this->view->reservation = "/sedona-adventures-reservation";
      $this->view->contactus = "/sedona-adventures-contact-us";
      $this->view->contactusx = "/sedona-adventures-contact-us";
      $this->view->specials = "/sedona-adventures-specials";
      $this->view->termsofuse = "/terms-of-use";
      $this->view->privacypolicy = "/privacy-policy";

      $curl = curl_init($this->config->application->ApiURL.'/pagedetails/getcontact');
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $curl_response = curl_exec($curl);

      if($curl_response === false){
          $info = curl_getinfo($curl);
          curl_close($curl);
          die('error occured during curl exec. Additional info: ' . var_export($info));
      }

      curl_close($curl);
      $decoded = json_decode($curl_response);

      $this->view->contact = $decoded;

    }

    public function angularLoader($ang){
        $modules = array();
        $scripts = '';
        foreach($ang as $key => $val){
            $scripts .= $this->tag->javascriptInclude($val);
            $modules[] = $key;
        }
        $this->view->modules = (!empty($modules) ? $modules : array());
        $this->view->otherjvascript = $scripts;
    }
    public function httpPost($url,$params)
    {
        $postData = '';
        //create name value pairs seperated by &
        foreach($params as $k => $v)
        {
            $postData .= $k . '='.$v.'&';
        }
        rtrim($postData, '&');

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output=curl_exec($ch);

        curl_close($ch);
        return $output;
    }
    public function curl($url){
        $service_url = $this->config->application->ApiURL.'/'.$url;
        $curl = curl_init($service_url);
/*        curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);*/
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        return $decoded = json_decode($curl_response);
    }
}
