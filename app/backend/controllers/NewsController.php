<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class NewsController extends ControllerBase
{
    public function createnewsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'News') === false){
            $this->response->redirect('atvadmin/admin');
        }
    }
    public function categorytagsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'News') === false){
            $this->response->redirect('atvadmin/admin');
        }
    }
    public function newauthorAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'News') === false){
            $this->response->redirect('atvadmin/admin');
        }
    }
    public function manageauthorsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'News') === false){
            $this->response->redirect('atvadmin/admin');
        }
    }
    public function updateauthorAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'News') === false){
            $this->response->redirect('atvadmin/admin');
        }
    }
    public function managenewsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false  && strpos($auth['roles'],'News') === false){
            $this->response->redirect('atvadmin/admin');
        }

    }
    public function updatenewsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'News') === false){
            $this->response->redirect('atvadmin/admin');
        }
    }

}
