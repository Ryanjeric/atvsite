<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class PageController extends ControllerBase
{
    public function indexAction() {

    }

    public function homeAction() {
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function contactAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        // $auth = $this->session->get('auth');
        // if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'News') === false){
        //     $this->response->redirect('atvadmin/admin');
        // }
    }

    public function bannerAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}
