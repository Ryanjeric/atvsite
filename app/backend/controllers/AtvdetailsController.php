<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class AtvdetailsController extends ControllerBase {
	public function freebiesAction(){
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		$auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'ATV') === false){
            $this->response->redirect('atvadmin/admin');
        }
	}
	public function requirementsAction(){
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		$auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'ATV') === false){
            $this->response->redirect('atvadmin/admin');
        }
	}
	public function rentalhoursAction(){
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		$auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'ATV') === false){
            $this->response->redirect('atvadmin/admin');
        }
	}
	public function createpricesAction(){
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		$auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'ATV') === false){
            $this->response->redirect('atvadmin/admin');
        }
	}
	public function managepricesAction(){
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		$auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'ATV') === false){
            $this->response->redirect('atvadmin/admin');
        }
	}
	public function updatepricesAction(){
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		$auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'ATV') === false){
            $this->response->redirect('atvadmin/admin');
        }
	}
}