<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class MetadataController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'Meta') === false){
            $this->response->redirect('atvadmin/admin');
        }
    }

}

