<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class SettingsController extends ControllerBase
{
    public function settingsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'Settings') === false){
            $this->response->redirect('atvadmin/admin');
        }
    }
}

