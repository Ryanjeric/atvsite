<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class TrailsController extends ControllerBase {
	public function createtrailsAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		$auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'Trails') === false){
            $this->response->redirect('atvadmin/admin');
        }
	}
	public function managetrailsAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		$auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'Trails') === false){
            $this->response->redirect('atvadmin/admin');
        }
	}
	public function updatetrailsAction(){
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		$auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'Trails') === false){
            $this->response->redirect('atvadmin/admin');
        }
	}
	public function metadataAction(){
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		$auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'Trails') === false){
            $this->response->redirect('atvadmin/admin');
        }
	}
}