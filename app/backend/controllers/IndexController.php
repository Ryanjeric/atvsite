<?php

namespace Modules\Backend\Controllers;  

use Phalcon\Mvc\View;
use Phalcon\Tag;
use Modules\Backend\Models\Users as Users;
use Modules\Backend\Models\Userrole as Userrole;
class IndexController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }   
    public function indexAction()
    {   
        $auth = $this->session->get('auth');
        if ($auth){
            $this->response->redirect('atvadmin/admin');
        } 

        $this->view->error = null;
        if ($this->request->isPost()) {
            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
            $shapass='';
            //$hashpass = $this->security->hash($password);
            $user = Users::findFirst("username='$username' and status=1");
            $deactivated = Users::findFirst("username='$username' and status=0");
            if($user){
                $shapass = sha1($password);
                if($shapass == $user->password){
                    $roles = Userrole::find("userid='".$user->id."'");

                    foreach ($roles as $value) {
                        $role.= $value->role.',';
                    } 

                    $this->_registerSession($user,$role);
                    $this->response->redirect('atvadmin/admin');
                }
                else{
                    $this->flash->warning('Wrong Username/Password');  
             }
            }
            else if($deactivated){
                $this->flash->warning('Account Deactivated Please Contact us if you want to Activate your account.');
            }
            else{
               $this->flash->warning('Wrong Username/Password');  
            }
            
        }
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);   
    }

    private function _registerSession($user,$role)
    {
        $this->session->set('auth', array(
            'atv_id' => $user->id,
            'atv_fullname' => $user->firstname .' '.$user->lastname,
            'atv_username' => $user->username,
            'atv_profilepic' =>$user->profile_pic_name,
            'task' =>$user->task,
            'roles' => $role
        ));

        //Set SuperAdmin
        if($user->userLevel){
            $this->session->set('SuperAgent', true );
        }
    }
}

