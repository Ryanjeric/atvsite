<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class UsersController extends ControllerBase
{

    public function indexAction()
    {

    }
    //USER LIST
    public function userlistAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'Users') === false){
            $this->response->redirect('atvadmin/admin');
        }
    }
    //USER CREATE
    public function usercreateAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'Users') === false){
            $this->response->redirect('atvadmin/admin');
        }
    }
    //USER Edit
    public function updateuserAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if (strpos($auth['roles'],'superadmin') === false && strpos($auth['roles'],'Users') === false){
            $this->response->redirect('atvadmin/admin');
        }
    }
    public function updateprofileAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    
}

