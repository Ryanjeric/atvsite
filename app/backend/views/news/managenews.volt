<style>
.i-switch i:before {
    background-color: gray!important;
    transition: all 0.3s;
}
.status {
  position:relative;
  top:-6.7px;
  display:inline-block;
  min-width:60px;
}
table tbody tr td { vertical-align:middle!important; }
</style>

<script type="text/ng-template" id="updatedelete">
  <div ng-include="'/be/tpl/updatedelete.html'"></div>
</script>

<div id="top" class="bg-light b-b wrapper-md">
  <h1 class="m-n font-thin h3 text-light">Manage News</h1>
</div>

<div class="container-fluid wrapper-md">
<div class="row">
  <div class="col-sm-12">
    <alert ng-repeat="alert in mainalerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <div class="panel panel-default">
      <div class="panel-heading font-bold">
        News Information
      </div>
      <div class="panel-body">
        <div class="col-md-4 input-group pull-right">
          <input type="text" class="input-sm form-control" placeholder="Search Title/Author" ng-model="keyword" ng-change="search(keyword)">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-danger" ng-click="clearsearch()"><i class="fa fa-refresh"></i></button>
          </span>
        </div>
        <br><br>
        <div class="table-responsive">
          <table class="table table-striped b-t b-light">
            <thead>
              <tr>
                <th style="width:2%;"></th>
                <th  style="width:33%;">Title</th>
                <th  style="width:20%;">Author</th>
                <th  style="width:15%;">Publish Date</th>
                <th style="width:15%;">Status</th>
                <th style="width:15%;">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="n in news">
                <td>{[{CurrentPage * 10 - 10 + $index + 1}]}</td>
                <td>{[{n.title}]}</td>
                <td>{[{n.name}]}</td>
                <td>{[{n.datepublished}]}</td>
                <td>
                  <div ng-if="n.status == 1">
                      <span class="label bg-danger status">Active</span>
                      <label class="i-switch bg-danger m-t-xs m-r">
                        <input checked="false" type="checkbox" ng-click="newstatus(n.id, 0, keyword, CurrentPage)">
                        <i></i>
                      </label>
                  </div>

                  <div ng-if="n.status == 0">
                    <span class="label bg-dark status">Inactive</span>
                    <label class="i-switch bg-danger m-t-xs m-r">
                      <input type="checkbox" ng-click="newstatus(n.id, 1, keyword, CurrentPage)">
                      <i></i>
                    </label>
                  </div>
                </td>
                <td>
                  <a class="btn btn-sm btn-default" ng-click="update(n.id)"><i class="icon icon-note "></i> Edit<a>
                  <a class="btn btn-sm btn-dark" ng-click="delete(n.id, keyword, CurrentPage)"><i class="icon  icon-close"></i> Delete<a>
                </td>
              </tr>
              <tr ng-if="TotalItems == 0 ">
                <td colspan="6"><center><h4>NO NEWS FOUND</h4></center></td>
              </tr>
            </tbody>
            <tfoot>
              <tr ng-show="TotalItems > 0">
                <td colspan="6">
                  <center>
                  <pagination total-items="TotalItems" ng-model="CurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(CurrentPage, keyword)"></pagination>
                  </center>
                </td>
              </tr>
            </tfoot>
          </table>
        </div> <!-- table-responsive -->
      </div> <!-- panel-body -->
    </div> <!-- panel-default-->
  </div>
</div>
</div>
