<script type="text/ng-template" id="medialibrary">
  <div ng-include="'/be/tpl/medialibrary.html'"></div>
</script>
<script type="text/ng-template" id="featuredmedialibrary">
  <div ng-include="'/be/tpl/featuredmedialibrary.html'"></div>
</script>
<script type="text/ng-template" id="yesno">
  <div ng-include="'/be/tpl/yesno.html'"></div>
</script>
<script type="text/ng-template" id="copypaste">
  <div ng-include="'/be/tpl/copypaste.html'"></div>
</script>
<script type="text/ng-template" id="saveone">
  <div ng-include="'/be/tpl/saveone.html'"></div>
</script>
<script type="text/ng-template" id="neworlist">
  <div ng-include="'/be/tpl/neworlist.html'"></div>
</script>
<script type="text/ng-template" id="stayorlist">
  <div ng-include="'/be/tpl/stayorlist.html'"></div>
</script>

<div id="top" class="bg-light lter b-b wrapper-md">
  <span class="m-n font-thin h3 text-light">Update News</span>
  <a ui-sref="managenews" class="btn btn-md btn-danger pull-right" title="Go Back"><i class="fa fa-mail-reply"></i></a>
</div>

<form editable-form class="form-validation form-horizontal  ng-invalid ng-invalid-required ng-scope ng-valid-date ng-dirty ng-valid-parse ng-valid-maxlength ng-valid-date-disabled" name="formnews" id="formnews">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <!-- ngRepeat: alert in alerts -->
      <div class="row">
        <div class="col-sm-12">
          <alert ng-repeat="alert in mainalerts" type="{[{alert.type }]}" close="closeAlert($index, 'main')">{[{ alert.msg }]}</alert>
        </div>
        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              News Information
            </div>

            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-2 control-label">
                  <label for="title">Title</label>
                </div>
                <div class="col-sm-10">
                  <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched"
                    ng-model="news.title"
                    ng-keyup="onnewstitle(news.title)"
                    ng-change="onnewstitle(news.title, disableslugs = true)"
                    required>
                </div>
              </div>

              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  News Slugs:
                </label>
                <div class="col-sm-10">
                  <div class="input-group">
                    <input type="text" class="form-control" name="slugs"
                        ng-model="news.slugs"
                        ng-disabled="disableslugs"
                        ng-pattern="/^([a-z0-9])([a-z0-9-])+[a-z0-9]$/"
                        ng-change="ensureuniqueslugs(news.slugs)"
                        required>
                    <span class="input-group-btn" ng-show="disableslugs">
                      <a class="btn btn-default" type="button" ng-click="disableslugs = false"><i class="icon icon-note"></i></a>
                    </span>
                    <span class="input-group-btn" ng-hide="disableslugs">
                      <a class="btn btn-success" type="button" ng-click="disableslugs = true"><i class="icon icon-pin"></i></a>
                    </span>
                    <span class="input-group-btn">
                      <a class="btn btn-default" type="button" ng-click="onnewstitle(news.title, disableslugs = true)"><i class="fa fa-refresh"></i></a>
                    </span>
                  </div>
                  <em class="text-danger" ng-show="formnews.slugs.$error.unique">That News Slugs exist, please change it.</em>
                  <em class="text-danger" ng-show="formnews.slugs.$error.pattern">Starts & ends with alphanumeric. White spaces is not allowed only dashed. No capital letters. Minimum of 3 characters</em>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  Author
                </label>
                <div class="col-sm-10">
                  <div ui-module="select2" ng-hide="authors == 0">
                    <select ui-select2 class="form-control"
                        ng-model="news.author"
                        required>
                        <option ng-repeat="author in authors" ng-value="author.id">{[{author.name}]}</option>
                    </select>
                  </div>
                  <div ng-show="authors == 0">
                    NO CURRENT LIST OF AUTHORS <br>
                    <a ui-sref="newauthor" class="text-info">Click here to add new</a>
                  </div>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <label for="title">Body Content</label>
                </label>
                <div class="col-sm-10">
                  <a class="btn btn-default btn-xs pull-right" ng-click="media('content')"><i class="fa  fa-folder-open"></i> Media Library </a> <br> <br>
                  <textarea ng-model="news.body" class="ck-editor ng-untouched ng-dirty ng-valid-parse ng-invalid ng-invalid-required" required></textarea>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <label for="title">Summary</label>
                </label>
                <div class="col-sm-10">
                  <textarea ng-model="news.summary" name="summary" required="required" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-maxlength" style="resize:vertical;" ng-maxlength="1000"></textarea>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <label for="title">Meta Title</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="metatitle" name="metatitle" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="news.metatitle" required>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <label for="title">Meta Description</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="metadesc" name="metadesc" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="news.metadesc" required>
                </div>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <label for="title">Meta Keyword</label>
                </label>
                <div class="col-sm-10">
                  <input type="text" id="metakeyword" name="metakeyword" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="news.metakeyword" required>
                </div>
              </div>
            </div>

          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Publish
            </div>
            <div class="panel-body">

              Publish Immediatley:
              <div class="input-group col-sm-10">
                <span class="input-group-btn">
                  <input id="date" name="date" ng-model="news.datepublished" class="form-control" datepicker-popup="yyyy-MM-dd"
                  style="max-width:80%"
                  type="text"
                  is-open="opened"
                  datepicker-options="dateOptions"
                  ng-required="true"
                  close-text="Close"
                  ng-init="news.datepublished = datenow "
                  disabled>
                  <button type="button" class="btn btn-default" ng-click="open($event)" style="width:20%"><i class="glyphicon glyphicon-calendar"></i></button>
                </span>
              </div>
              <div class="line line-dashed b-b line-lg"></div>
              <button type="submit" ng-click="saveNews(news,0)" class="btn btn-default btn-sm" ng-disabled="formnews.$invalid" disabled="disabled">Save Draft</button>
              <a ng-click="preview(news)" class="btn btn-default btn-sm" ng-disabled="formnews.$invalid" disabled="disabled">Preview</a>
              <button type="submit" ng-click="saveNews(news,1)" class="btn btn-success btn-sm pull-right" ng-disabled="formnews.$invalid" disabled="disabled">Publish</button>
            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Category
              <a class="btn btn-default btn-xs pull-right" ng-click="addcategory()"><i class="fa fa-plus"></i> Add</a>
            </div>
            <div class="panel-body">
              <alert ng-repeat="alert in categoryalerts" type="{[{alert.type }]}" close="closeAlert($index,'category')">{[{ alert.msg }]}</alert>
              <select chosen multiple class="chosen-choices form-control"
                style="width:100%"
                ng-model="news.categories"
                options="categories"
                ng-change="test(news.categories)"
                ng-options="category.id as category.categoryname for category in categories"
                required>
              </select>

              <label><em class="text-muted">Click add to create more categories to choose from.</em></label>
            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Tags
              {#<a class="btn btn-default btn-xs pull-right" ng-click="addtag()"><i class="fa fa-plus"></i> Add</a>#}
            </div>
            <div class="panel-body">
              <alert ng-repeat="alert in tagalerts" type="{[{alert.type }]}" close="closeAlert($index,'tag')">{[{ alert.msg }]}</alert>

              <ui-select multiple tagging tagging-label="(Enter to add)"
                            ng-model="news.tags"
                            sortable="false"
                            style="width:100%;" title="Choose/Enter a tag" required>
                <ui-select-match placeholder="Choose/Enter a tag">{[{$item}]}</ui-select-match>
                <ui-select-choices repeat="tagname in tagnames | filter:$select.search">
                  {[{tagname}]}
                </ui-select-choices>
              </ui-select>

              <label><em class="text-muted">For tags that doesnt exist just type and press enter. It will be added to your database of tags once the page is published.</em></label>
            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Select featured
              <a class="btn btn-default btn-xs pull-right" ng-click="media('featured')"><i class="fa  fa-folder-open"></i> Media Library</a>
            </div>
            <div class="panel-body" ng-init="hasChooseFeatured = true">
              <input type="hidden" name="featimage" ng-model="news.featimage" class="ng-pristine ng-untouched ng-invalid ng-invalid-required">
              <input type="hidden" name="featvideo" ng-model="news.featvideo" class="ng-pristine ng-untouched ng-invalid ng-invalid-required">
              <input type="hidden" name="featured" ng-model="news.featuredtype" class="ng-pristine ng-untouched ng-invalid ng-invalid-required" required>
              <label><em class="text-muted">Click select to add videos and image or select an existing image or video.</em></label>

                <div ng-show="hasChooseFeatured" id="featuredbox">

                  <img ng-show="featuredtype == 'img' " id="featuredimage" src="" style="width: 100%;" />
                  <div ng-show="featuredtype == 'vid' " ng-bind-html="featuredvid" alt="Loading.. please wait" style="width: 100%;"></div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </fieldset>
</form>
<script type="text/javascript">
  var config = {
    '.chosen-choices'           : {},
    '.chosen-select'           : {},
    '.chosen-select-deselect'  : {allow_single_deselect:true},
    '.chosen-select-no-single' : {disable_search_threshold:10},
    '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
    '.chosen-select-width'     : {width:"95%"}
  }
  for (var selector in config) {
    $(selector).chosen(config[selector]);
  }
</script>
