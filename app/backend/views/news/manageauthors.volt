<script type="text/ng-template" id="updatedelete">
  <div ng-include="'/be/tpl/updatedelete.html'"></div>
</script>
<script type="text/ng-template" id="deleteauthor">
  <div ng-include="'/be/tpl/deleteauthor.html'"></div>
</script>


<div id="top" class="bg-light lter b-b wrapper-md ng-scope">
  <h1 class="m-n font-thin h3">Manage Authors</h1>
</div>

<div class="container-fluid wrapper-md">
	<div class="row">
		<div class="col-md-12">

      <alert ng-repeat="alert in mainalerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

			<div class="panel panel-default">
				<div class="panel-heading font-bold">List</div>
				<div class="panel-body">
          <div class="col-md-4 input-group pull-right">
            <input type="text" class="input-sm form-control" placeholder="Search Name" ng-model="keyword" ng-change="search(keyword)">
            <span class="input-group-btn">
              <button class="btn btn-sm btn-danger" ng-click="clearsearch()"><i class="fa fa-refresh"></i></button>
            </span>
          </div>
          <br><br>
					<div class="table-responsive">
			      <table class="table table-striped b-t b-light">
			        <thead>
			          <tr>
			          	<th></th>
			            <th>Name</th>
			            <th>Location</th>
			            <th>Occupation</th>
			            <th>Author Since</th>
			            <th>Action</th>
			          </tr>
			        </thead>
			        <tbody>
			          <tr ng-repeat="author in authors">
			          	<td>{[{CurrentPage * 10 - 10 + $index + 1}]}</td>
			            <td ng-bind="author.name"></td>
			            <td ng-bind="author.location"></td>
			            <td ng-bind="author.occupation"></td>
			            <td ng-bind="author.since"></td>
			            <td>
			            	<a class="btn btn-sm btn-default" ng-click="update(author.id)"><i class="icon icon-note "></i> Edit<a>
			            	<a class="btn btn-sm btn-dark" ng-click="delete(author.id, keyword, CurrentPage)"><i class="icon  icon-close"></i> Delete<a>
			            </td>
			          </tr>
			          <tr ng-if="TotalItems == 0 ">
			          	<td colspan="6"><center><h4>NO AUTHOR FOUND</h4></center></td>
			          </tr>
			        </tbody>
			        <tfoot>
			        	<tr ng-show="TotalItems > 0">
			        		<td colspan="6">
			        			<center>
			        			<pagination total-items="TotalItems" ng-model="CurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(CurrentPage, keyword)"></pagination>
			        			</center>
			        		</td>
			        	</tr>
			        </tfoot>
			      </table>
			    </div>
				</div> <!-- panel-body -->
			</div> <!-- panel-primary -->
		</div> <!-- col-md-12 -->
	</div> <!-- row -->
</div> <!-- container-fluid -->
