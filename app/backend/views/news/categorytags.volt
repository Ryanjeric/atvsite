<script type="text/ng-template" id="saveone">
  <div ng-include="'/be/tpl/saveone.html'"></div>
</script>
<script type="text/ng-template" id="notification2">
  <div ng-include="'/be/tpl/notification2.html'"></div>
</script>
<script type="text/ng-template" id="dynamic">
  <div ng-include="'/be/tpl/dynamic.html'"></div>
</script>
<script type="text/ng-template" id="deletecategory">
  <div ng-include="'/be/tpl/deletecategory.html'"></div>
</script>
<script type="text/ng-template" id="deletetag">
  <div ng-include="'/be/tpl/deletetag.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">Category/Tags</h1>
</div>

<div class="row wrapper-md">
	<div class="col-sm-6">
		<div class="panel panel-default">
			<div class="panel-heading b-b wrapper-md"><strong>Search</strong>
 					<form class="form-inline">
 						<input type="text" class="form-control" placeholder="Tag Name" ng-model="searchbox_tag" ng-change="search_tag(searchbox_tag)" >
 						<button class="btn btn-sm pull-right btn-danger" ng-click="newtag()">Add New Tag</button>
 					</form>
 			</div>
 			<div class="panel-body">
 				<center>
 					<pagination ng-hide="TotalItems_tag == 0" total-items="TotalItems_tag" ng-model="CurrentPage_tag" max-size="maxSize_tag" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage_tag(CurrentPage_tag)"></pagination>
 				</center>
        <alert ng-repeat="alert in tagalerts" type="{[{alert.type }]}" close="closetagAlert($index)">{[{ alert.msg }]}</alert>
 				<table class="table table-responsive">
 					<thead>
 						<tr>
              <th style="width:5%"></th>
 							<th>Tag Name</th>
 							<th>Action</th>
 						</tr>
 					</thead>
 					<tbody>
 						<tr ng-repeat="list in tagslist">
              <td>{[{CurrentPage_tag * 10 - 10 + $index + 1}]}</td>
 							<td>
 								<span editable-text="list.tagname"
 											onbeforesave="updatetag(list.id, list.tagname, $data, $index)"
 											e-form="xeditabletagname"
 											e-required>
 											{[{list.tagname}]}
								</span>
 							</td>
 							<td style="vertical-align:middle; width:30%">
 								<a ng-click="xeditabletagname.$show()" ng-hide="xeditabletagname.$visible">
 									<span class="label bg-info" >Edit</span>
 								</a>
 								<a ng-click="removetag(list.tagname, list.id, CurrentPage_tag, searchbox_tag)"><span class="label bg-danger">Remove</span></a>
 							</td>
 						</tr>
            <tr ng-show="TotalItems_tag == 0 ">
              <td colspan="3"><center><h4>NO TAG FOUND!</h4></center></td>
            </tr>
 					</tbody>
 				</table>
        <div class="overlay" ng-show="tagspinner">
          <i class="fa fa-spin fa-spinner text-danger spin" style="font-size:80px;"></i>
        </div>
 			</div>
 		</div>
 	</div>

 	<div class="col-sm-6">
 		<div class="panel panel-default">
 			<div class="panel-heading b-b wrapper-md"><strong>Search</strong> <br>
 					<form class="form-inline">
 						<input type="text" class="form-control" placeholder="Category Name" ng-model="searchbox_category" ng-change="search_category(searchbox_category)" >
 						<button class="btn btn-sm pull-right btn-danger" ng-click="newcategory()">Add New Category</button>
 					</form>
 			</div>
 			<div class="panel-body">
 				<center>
 					<pagination ng-hide="TotalItems_category == 0" total-items="TotalItems_category" ng-model="CurrentPage_category" max-size="maxSize_category" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage_category(CurrentPage_category)"></pagination>
 				</center>
        <alert ng-repeat="alert in categoryalerts" type="{[{alert.type }]}" close="closecategoryAlert($index)">{[{ alert.msg }]}</alert>
 				<table class="table table-responsive">
 					<thead>
 						<tr>
              <th style="width:5%"></th>
 							<th>Category Name</th>
 							<th>Action</th>
 						</tr>
 					</thead>
 					<tbody>
 						<tr ng-repeat="list in categorylist">
              <td>{[{CurrentPage_category * 10 - 10 + $index + 1}]}</td>
 							<td>
 								<span editable-text="list.categoryname"
 											onbeforesave="updatecategory(list.id, list.categoryname, $data, $index)"
 											e-form="xeditablecategoryname"
 											e-required>
 											{[{list.categoryname}]}
								</span>
 							</td>
 							<td style="vertical-align:middle; width:30%">
 								<a ng-click="xeditablecategoryname.$show()" ng-hide="xeditablecategoryname.$visible">
 									<span class="label bg-info" >Edit</span>
 								</a>
 								<a ng-click="removecategory(list.categoryname, list.id, searchbox_category)"><span class="label bg-danger">Remove</span></a>
 							</td>
 						</tr>
            <tr  ng-show="TotalItems_category == 0">
              <td colspan="3">
                <center><h4>NO CATEGORY FOUND!</h4></center>
              </td>
            </tr>
 					</tbody>
 				</table>
        <div class="overlay" ng-show="categoryspinner">
          <i class="fa fa-spin fa-spinner text-danger spin" style="font-size:80px;"></i>
        </div>
 			</div>
 		</div>
 	</div>
</div>
