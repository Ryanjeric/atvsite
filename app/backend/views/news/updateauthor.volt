<script type="text/ng-template" id="authorsgallery">
  <div ng-include="'/be/tpl/authorsgallery.html'"></div>
</script>
<script type="text/ng-template" id="stayorlist">
  <div ng-include="'/be/tpl/stayorlist.html'"></div>
</script>
<script type="text/ng-template" id="yesno">
  <div ng-include="'/be/tpl/yesno.html'"></div>
</script>

<div id="top" class="bg-light lter b-b wrapper-md ng-scope">
  <h1 class="m-n font-thin h3">Update Author</h1>
</div>
<form class="form-validation ng-invalid ng-invalid-required ng-scope ng-valid-date ng-dirty ng-valid-parse ng-valid-date-disabled" ng-submit="updateAuthor(author)" name="authorFORM">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md">
    <!-- ngRepeat: alert in alerts -->

      <div class="row">
        <div class="col-sm-12">
          <alert ng-repeat="alert in mainalerts" type="{[{alert.type }]}" close="closeAlert(type='main',$index)">{[{ alert.msg }]}</alert>
        </div>
        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Authors Information
            </div>

              <div class="panel-body">

                <div class="form-group hiddenoverflow">
                  <label class="col-sm-2 control-label">
                    <label for="title">Name</label>
                  </label>
                  <div class="col-sm-10">
                    <label class="text-danger pull-right" ng-show="authorFORM.name.$error.unique">
                    <i class="glyphicon glyphicon-info-sign"></i> Already taken.</label>
                    <label class="text-danger pull-right" ng-show="authorFORM.name.$error.pattern">
                    <i class="glyphicon glyphicon-info-sign"></i> Please remove any unwanted characters.</label>
                    <input type="text" id="name" name="name" alt="{[{author.name}]}" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="author.name" ng-pattern="/^[a-zA-Z \s-.]+$/" required="required" ng-change="ensureUnique(author.name)">
                  </div>
                </div>

                <div class="line line-dashed b-b line-lg"></div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">
                    <label for="title">Location</label>
                  </label>
                  <div class="col-sm-10">
                    <input type="text" id="location" name="location" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="author.location" required="required">
                  </div>
                </div>

                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group hiddenoverflow">
                  <label class="col-sm-2 control-label">
                    <label for="title">Occupation</label>
                  </label>
                  <div class="col-sm-10">
                    <input type="text" id="occupation" name="occupation" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="author.occupation" ng-pattern="/^[a-zA-Z \s-]+$/" required="required">
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg"></div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">
                    <label for="title">Since</label>
                  </label>
                  <div class="col-sm-4">
                  <div class="input-group">
                    <span class="input-group-btn">
                      <input id="date" name="date" ng-model="author.since" class="form-control" datepicker-popup="yyyy-MM-dd"
                      style="max-width:80%"
                      type="text"
                      is-open="opened"
                      datepicker-options="dateOptions"
                      ng-required="true"
                      close-text="Close"
                      ng-init="author.since = datenow "
                      disabled>
                      <button type="button" class="btn btn-default" ng-click="open($event)" style="width:20%"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>
                  </div>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg"></div>

                <div class="form-group hiddenoverflow">
                  <label class="col-sm-2 control-label">
                    <label for="title">About the Author</label>
                  </label>
                  <div class="col-sm-10">
                    <textarea class="ck-editor ng-untouched ng-dirty ng-valid-parse ng-invalid ng-invalid-required" ng-model="author.about" style="visibility: hidden; display: none;"></textarea>
                  </div>
                </div>
              </div>

          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Photo of the Author
              <a class="btn btn-default btn-xs pull-right" ng-click="media(size = 'lg')"><i class="fa fa-folder-open"></i> Media Library </a>
            </div>
            <div class="panel-body">
              <input type="hidden" ng-model="author.imgid" />
              <div class="form-group" ng-show="hasChooseImg">
                <div class="input-group">
                  <input class="form-control" type="text" ng-model="author.photo" ng-disabled="true">
                  <span class="input-group-btn">
                    <button class="btn btn-danger" type="button" title="Remove" ng-click="removephoto()"><i class="fa fa-minus-circle" ></i></button>
                  </span>
                </div>
                <img id="authorphoto" src="{[{image}]}" style="width: 100%;" />
              </div>

            </div>
          </div>
        </div>

      </div>

      <div class="row">
        <div class="panel-body">
            <footer class="panel-footer text-right bg-light lter">
              <a ui-sref="manageauthors" class="btn btn-default" href="/dashboard"> Cancel </a>
              <button type="submit" class="btn btn-success" ng-disabled="authorFORM.$invalid" disabled="disabled">Save</button>
            </footer>
        </div>
      </div>

  </div>
</fieldset>
</form>
