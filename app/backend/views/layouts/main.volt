<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head>
  <link rel="shortcut icon" href="/img/favicon.ico">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta charset="utf-8">
  <meta name="viewport" content="user-scalable = yes" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <!-- Title and other stuffs -->
  {{ get_title() }}
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  {{ tag_html("link", ["rel":"stylesheet/less", "type":"text/css", "href":"/be/css/less/custom.less"]) }}
  
  {{ stylesheet_link('be/css/bootstrap.css') }}
  {{ stylesheet_link('be/css/animate.css') }}
  {{ stylesheet_link('be/css/font-awesome.min.css') }}
  {{ stylesheet_link('be/css/simple-line-icons.css') }}
  {{ stylesheet_link('be/css/font.css') }}
  {{ stylesheet_link('be/css/app.css') }}
  {{ stylesheet_link('be/css/custom.css') }}
  {{ stylesheet_link('be/js/jquery/fullcalendar/fullcalendar.css') }}
  {{ stylesheet_link('be/js/jquery/chosen/chosen.css') }} <!-- seems better -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.min.css"> -->

  <!-- {{ stylesheet_link('be/js/jquery/select2/select2.css') }} -->
  <!-- {{ stylesheet_link('be/js/jquery/select2/select2-bootstrap.css') }} -->

  {{ stylesheet_link('vendors/angularjs-toaster/toaster.css') }}
  {{ stylesheet_link('vendors/angular-xeditable/dist/css/xeditable.css') }}
  {{ stylesheet_link('vendors/angular-ui-select/dist/select.css') }}
  {{ stylesheet_link('vendors/angular-bootstrap-colorpicker/css/colorpicker.min.css') }}
  {{ stylesheet_link('be/js/jquery/select2/select2.css') }}
  {{ stylesheet_link('vendors/angular-ui-select/dist/select.css') }}
  {{ stylesheet_link('vendors/sweetalert/dist/sweetalert.css') }}

  <script>
    var userid = "<?=$username['atv_id']?>";
    var username = "<?=$username['atv_username']?>";
  </script>
</head>
<body ng-controller="AppCtrl">
{{ content() }}
<div class="app" id="app" ng-class="{'app-header-fixed':app.settings.headerFixed, 'app-aside-fixed':app.settings.asideFixed, 'app-aside-folded':app.settings.asideFolded}">
    <div class="app-header navbar bg-cream">
      <!-- navbar header -->
      <div class="navbar-header bg-orange ">
        <button class="pull-right visible-xs bg-success dk" ui-toggle-class="show" data-target=".navbar-collapse">
          <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle-class="off-screen" data-target=".app-aside" ui-scroll="app">
          <i class="glyphicon glyphicon-align-justify"></i>
        </button>

        <!-- brand -->
        <center>
        <a href="#/" class="navbar-brand text-lt">
          <img src="/img/atvlogo.png" alt=".">
          <span class="hidden-folded m-l-xs">ATV</span>
        </a>
        </center>
        <!-- / brand -->
      </div>
      <!-- / navbar header -->

      <!-- navbar collapse {[{app.settings.navbarCollapseColor}]} the angular beside the box-shadow below -->
      <div class="collapse navbar-collapse box-shadow">
        <!-- buttons -->
        <div class="nav navbar-nav m-l-sm hidden-xs">
          <a href class="btn no-shadow navbar-btn" ng-click="app.settings.asideFolded = !app.settings.asideFolded">
            <i class="fa {[{app.settings.asideFolded ? 'fa-indent' : 'fa-dedent'}]} fa-fw"></i>
          </a>
          <a href class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
            <i class="icon-user fa-fw"></i>
          </a>
        </div>
        <!-- / buttons -->

        <!-- link and dropdown -->
       <!--  <ul class="nav navbar-nav hidden-sm">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
              <span translate="header.navbar.new.NEW">New</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li>
                <a ui-sref="createnews">
                  <span>NEWS/BLOG</span>
                </a>
              </li>
              <li>
                <a ui-sref="usercreate">
                  <span translate="header.navbar.new.USER">User</span>
                </a>
              </li>
            </ul>
          </li>
        </ul> -->
        <!-- / link and dropdown -->

        <!-- search form -->
        <!-- <form class="navbar-form navbar-form-sm navbar-left shift" ui-shift="prependTo" target=".navbar-collapse" role="search" ng-controller="TypeaheadDemoCtrl">
          <div class="form-group">
            <div class="input-group">
              <input type="text" ng-model="selected" typeahead="state for state in states | filter:$viewValue | limitTo:8" class="form-control input-sm bg-light no-border rounded padder" placeholder="Search projects...">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-sm bg-light rounded"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </div>
        </form> -->
        <!-- / search form -->

        <!-- nabar right -->
        <ul class="nav navbar-nav navbar-right">
          <li class="hidden-xs">
            <a ui-fullscreen></a>
          </li>
          <li class="dropdown">
            <a href class="dropdown-toggle">
              <i class="icon-bell fa-fw"></i>
              <span class="visible-xs-inline">Notifications</span>
              <span class="badge badge-sm up bg-danger pull-right-xs">2</span>
            </a>
            <!-- dropdown -->
            <div class="dropdown-menu w-xl animated fadeInUp">
              <div class="panel bg-white">
                <div class="panel-heading b-light bg-light">
                  <strong>You have <span>2</span> notifications</strong>
                </div>
                <div class="list-group">
                  <a href class="media list-group-item">
                    <span class="pull-left thumb-sm">
                      <img src="/img/a0.jpg" alt="..." class="img-circle">
                    </span>
                    <span class="media-body block m-b-none">
                      Use awesome animate.css<br>
                      <small class="text-muted">10 minutes ago</small>
                    </span>
                  </a>
                  <a href class="media list-group-item">
                    <span class="media-body block m-b-none">
                      1.0 initial released<br>
                      <small class="text-muted">1 hour ago</small>
                    </span>
                  </a>
                </div>
                <div class="panel-footer text-sm">
                  <a href class="pull-right"><i class="fa fa-cog"></i></a>
                  <a href="#notes" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                </div>
              </div>
            </div>
            <!-- / dropdown -->
          </li>
          <li class="dropdown">
            <a href class="dropdown-toggle clear" data-toggle="dropdown">
              <span class="thumb-sm avatar pull-left m-t-n-sm m-b-n-sm m-l-sm">
                <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $username['atv_profilepic']; ?>" err-SRC="/img/noimg.jpg">
              </span>
              <span class="hidden-sm hidden-md"><?php echo $username['atv_username']; ?></span> <b class="caret"></b>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu animated fadeInRight w">
              <?php if(strpos($username['roles'],'superadmin') !== false || strpos($username['roles'],'Settings') !== false){ ?>
              <li>
                <a ui-sref="settings">
                  <span>Settings</span>
                </a>
              </li>
              <?php } ?>
              <li>
                <a ui-sref="updateprofile">Profile</a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="/atvadmin/admin/logout">Logout</a>
              </li>
            </ul>
            <!-- / dropdown -->
          </li>
        </ul>
        <!-- / navbar right -->
      </div>
      <!-- / navbar collapse -->
      </div>
  <!-- menu -->
<div class="app-aside hidden-xs {[{app.settings.asideColor}]}">
<div class="aside-wrap">
  <div class="navi-wrap">
    <!-- user -->
    <div class="clearfix hidden-xs text-center hide" id="aside-user">
      <div class="dropdown wrapper">
        <a ui-sref="app.page.profile">
          <span class="thumb-lg w-auto-folded avatar m-t-sm">
            <img src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/<?php echo $username['atv_profilepic']; ?>" err-SRC="/img/noimg.jpg" class="img-full" alt="...">
          </span>
        </a>
        <a href class="dropdown-toggle hidden-folded">
          <span class="clear">
            <span class="block m-t-sm">
              <strong class="font-bold text-lt"></strong>
              <b class="caret"></b>
            </span>
            <span class="text-muted text-xs block">ADMIN</span>
          </span>
        </a>
        <!-- dropdown -->
        <!-- <ul class="dropdown-menu animated fadeInRight w hidden-folded">
          <li class="wrapper b-b m-b-sm bg-info m-t-n-xs">
            <span class="arrow top hidden-folded arrow-info"></span>
            <div>
              <p>300mb of 500mb used</p>
            </div>
            <progressbar value="60" type="white" class="progress-xs m-b-none dker"></progressbar>
          </li>
          <li>
            <a href>Settings</a>
          </li>
          <li>
            <a ui-sref="updateprofile">Profile</a>
          </li>
          <li>
            <a href>
              <span class="badge bg-danger pull-right">3</span>
              Notifications
            </a>
          </li>
          <li class="divider"></li>
          <li>
            <a ui-sref="access.signin">Logout</a>
          </li>
        </ul> -->
        <!-- / dropdown -->
      </div>
      <div class="line dk hidden-folded"></div>
    </div>
    <!-- / user -->

    <!-- nav -->
    <nav ui-nav class="navi">
<!-- first -->
<ul class="nav">
  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span>Navigation</span>
  </li>
  <li>
    <a ui-sref="dashboard">
      <i class="glyphicon glyphicon-stats icon text-primary-dker"></i>

      <span class="font-bold" translate="aside.nav.DASHBOARD">Dashboard</span>
    </a>
  </li>
  <?php  if(strpos($username['roles'],'superadmin') !== false || strpos($username['roles'],'Users') !== false){  ?>
  <li ng-class="{active:$state.includes('app.users')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-user icon"></i>
      <span class="font-bold" translate="aside.nav.users.USERS">Users</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="usercreate">
          <span translate="aside.nav.users.CREATE_USER">Create User</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="userlist">
          <span translate="aside.nav.users.USER_LIST">User List</span>
        </a>
      </li>
    </ul>
  </li>
  <?php } if(strpos($username['roles'],'superadmin') !== false || strpos($username['roles'],'Gallery') !== false){ ?>
  <li ng-class="{active:$state.includes('app.ui')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="fa fa-image icon text-info-dker"></i>
      <span class="font-bold">Gallery</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="gallery">
          <span>Images/Videos</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="adventure">
          <span>Exiting Adventures Await</span>
        </a>
      </li>
    </ul>
  </li>
<?php } if(strpos($username['roles'],'superadmin') !== false || strpos($username['roles'],'News') !== false){ ?>
  <li>
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="glyphicon glyphicon-edit icon text-info-lter"></i>
      <span class="font-bold">News</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="createnews">
          <span translate="aside.nav.news.CREATE_NEWS">Create News</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="managenews">
          <span translate="aside.nav.news.MANAGE_NEWS">Manage News</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="categorytags">
          <span translate="aside.nav.news.CATEGORY_TAGS">Category/Tags</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="newauthor">
          <span translate="aside.nav.news.NEWAUTHOR">New Author</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="manageauthors">
          <span translate="aside.nav.news.MANAGEAUTHORS">Manage Authors</span>
        </a>
      </li>
    </ul>
  </li>
  <?php } if(strpos($username['roles'],'superadmin') !== false || strpos($username['roles'],'Contacts') !== false){ ?>
  <li ui-sref-active="active">
    <a ui-sref="contacts">
      <!-- <b class="badge bg-success dk pull-right">16</b> -->
      <i class="fa fa-envelope-o icon"></i>
      <span class="font-bold">Contacts</span>
    </a>
  </li>
  
  <li ng-class="{active:$state.includes('app.table')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="fa icon-book-open icon"></i>
      <span class="font-bold">Page</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="homedetails">
          <span>Home</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="contactdetails">
          <span>Contacts</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="pagebanners">
          <span>Banners</span>
        </a>
      </li>
    </ul>
  </li>

  <li class="line dk"></li>

  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span translate="aside.nav.components.COMPONENTS">Components</span>
  </li>
    <?php } if(strpos($username['roles'],'superadmin') !== false || strpos($username['roles'],'ATV') !== false){ ?>
  <li ng-class="{active:$state.includes('app.ui')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="fa fa-cab icon text-success"></i>
      <span class="font-bold">ATV Details</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="rentalhours">
          <span>Rental Hours</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="freebies">
          <span>Freebies,Delivery Service Message & Special offers</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="requirements">
          <span>Requirements</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="createprices">
          <span>Create Price/s</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="manageprices">
          <span>Manage Prices</span>
        </a>
      </li>
    </ul>
  </li>
  <?php } if(strpos($username['roles'],'superadmin') !== false || strpos($username['roles'],'Trails') !== false){ ?>
  <li ng-class="{active:$state.includes('app.table')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <!-- <b class="label bg-primary pull-right">2</b> -->
      <i class="glyphicon icon-map"></i>
      <span class="font-bold">Trails</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="createtrails">
          <span>Create Trails</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="managetrails">
          <span>Manage Trails</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="trailsmeta">
          <span>Category Metadata</span>
        </a>
      </li>
    </ul>
  </li>
  <?php } if(strpos($username['roles'],'superadmin') !== false || strpos($username['roles'],'Reservations') !== false){ ?>
  <li ng-class="{active:$state.includes('app.table')}">
    <a href class="auto">
      <span class="pull-right text-muted">
        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
      </span>
      <i class="fa icon-book-open icon"></i>
      <span class="font-bold">Reservations</span>
    </a>
    <ul class="nav nav-sub dk">
      <li ui-sref-active="active">
        <a ui-sref="reservations">
          <span>Manage Reservations</span>
        </a>
      </li>
      <li ui-sref-active="active">
        <a ui-sref="reservationnotice">
          <span>Reservation Notice</span>
        </a>
      </li>
    </ul>
  </li>
   <?php } if(strpos($username['roles'],'superadmin') !== false || strpos($username['roles'],'Meta') !== false){ ?>


  <li ui-sref-active="active" ng-class="{active:$state.includes('app.table')}">
    <a ui-sref="metadata">
      <i class="fa fa-file-code-o icon"></i>
      <span class="font-bold">Meta Data</span>
    </a>
  </li>

  <?php } ?>
  <li class="line dk hidden-folded"></li>

  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
    <span translate="aside.nav.your_stuff.OTHERS">Your Stuff</span>
  </li>
  <li>
    <a ui-sref="updateprofile">
      <i class="icon-user icon text-success-lter"></i>
      <!-- <b class="badge bg-success dk pull-right">30%</b> -->
      <span translate="aside.nav.your_stuff.PROFILE">Profile</span>
    </a>
  </li>
  <?php if(strpos($username['roles'],'superadmin') !== false || strpos($username['roles'],'Settings') !== false){ ?>
   <li>
    <a ui-sref="settings">
      <i class="glyphicon glyphicon-cog icon"></i>
      <span translate="aside.nav.your_stuff.SETTINGS">Settings</span>
    </a>
  </li>
    <?php } ?>
</ul>
<!-- / third -->

    </nav>
    <!-- nav -->

    <!-- aside footer -->
  <!--   <div class="wrapper m-t">
      <div class="text-center-folded">
        <span class="pull-right pull-none-folded">60%</span>
        <span class="hidden-folded" translate="aside.MEMORY">Milestone</span>
      </div>
      <progressbar value="60" class="progress-xxs m-t-sm dk" type="info"></progressbar>
      <div class="text-center-folded">
        <span class="pull-right pull-none-folded">35%</span>
        <span class="hidden-folded" translate="aside.DISK">Release</span>
      </div>
      <progressbar value="35" class="progress-xxs m-t-sm dk" type="primary"></progressbar>
    </div> -->
    <!-- / aside footer -->
  </div>
</div>
  </div>
  <!-- / menu -->
  <!-- content -->
  <div class="app-content">
    <div ui-butterbar></div>
    <a href class="off-screen-toggle hide" ui-toggle-class="off-screen" data-target=".app-aside" ></a>
    <div class="app-content-body fade-in-up" ui-view></div>
  </div>
  <!-- /content -->
  <!-- aside right -->
  <div class="app-aside-right pos-fix no-padder w-md w-auto-xs bg-white b-l animated fadeInRight hide">
    <div class="vbox">
      <div class="wrapper b-b b-light m-b">
        <a href class="pull-right text-muted text-md" ui-toggle-class="show" target=".app-aside-right"><i class="icon-close"></i></a>
        Chat
      </div>
      <div class="row-row">
        <div class="cell">
          <div class="cell-inner padder">
            <!-- chat list -->
            <div class="m-b">
              <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                  <span class="arrow left pull-up"></span>
                  <p class="m-b-none">Hi John, What's up...</p>
                </div>
                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i> 2 minutes ago</small>
              </div>
            </div>
            <div class="m-b">
              <a href class="pull-right thumb-xs avatar"><img src="/img/a3.jpg" class="img-circle" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm bg-light r m-r-sm">
                  <span class="arrow right pull-up arrow-light"></span>
                  <p class="m-b-none">Lorem ipsum dolor :)</p>
                </div>
                <small class="text-muted">1 minutes ago</small>
              </div>
            </div>
            <div class="m-b">
              <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="..."></a>
              <div class="clear">
                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                  <span class="arrow left pull-up"></span>
                  <p class="m-b-none">Great!</p>
                </div>
                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i>Just Now</small>
              </div>
            </div>
            <!-- / chat list -->
          </div>
        </div>
      </div>
      <div class="wrapper m-t b-t b-light">
        <form class="m-b-none">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Say something">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">SEND</button>
            </span>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- / aside right -->

  <!-- footer -->
  <div class="app-footer wrapper b-t bg-light">
    &copy; 2015 Copyright.
    <span class="pull-right">{[{app.version}]} <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
  </div>
  <!-- / footer -->

</div>

{{ javascript_include('vendors/jquery/dist/jquery.js') }}
{{ javascript_include('vendors/angular/angular.min.js') }}
{{ javascript_include('be/js/angular/angular-cookies.min.js') }}
{{ javascript_include('vendors/angular-animate/angular-animate.min.js') }}
{{ javascript_include('be/js/angular/angular-ui-router.min.js') }}
{{ javascript_include('be/js/angular/angular-translate.js') }}
{{ javascript_include('be/js/angular/ngStorage.min.js') }}
{{ javascript_include('be/js/angular/ui-load.js') }}
{{ javascript_include('be/js/angular/ui-jq.js') }}
{{ javascript_include('be/js/angular/ui-validate.js') }}
{{ javascript_include('be/js/angular/ui-bootstrap-tpls.min.js') }}

{{ javascript_include('vendors/angular-ui-calendar/src/calendar.js') }}
{{ javascript_include('vendors/fullcalendar/dist/fullcalendar.min.js') }}
{{ javascript_include('vendors/fullcalendar/dist/gcal.js') }}

<!-- other plugins -->
{{ javascript_include('vendors/ng-file-upload/ng-file-upload.min.js') }}
{{ javascript_include('vendors/ng-file-upload/ng-file-upload-shim.js') }}
{{ javascript_include('vendors/moment/min/moment.min.js') }}
{{ javascript_include('vendors/angular-moment/angular-moment.min.js') }}
{{ javascript_include('vendors/angular-xeditable/dist/js/xeditable.min.js') }}
{{ javascript_include('vendors/angular-ui-select/dist/select.js') }}
{{ javascript_include('vendors/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.min.js') }}
{{ javascript_include('vendors/angularjs-toaster/toaster.js') }}
{{ javascript_include('vendors/sweetalert/dist/sweetalert.min.js') }}

<!-- APP -->
{{ javascript_include('be/js/scripts/app.js') }}
{{ javascript_include('be/js/scripts/factory/factory.js') }}
{{ javascript_include('be/js/scripts/controllers/controllers.js') }}
{{ javascript_include('be/js/scripts/directives/directives.js') }}
{{ javascript_include('be/js/scripts/directives/validations.js') }}
{{ javascript_include('be/js/scripts/services/anchorsmoothscroll.js') }}
{{ javascript_include('be/js/scripts/services/services.js') }}
{{ javascript_include('be/js/scripts/config.js') }}

<!-- {{ javascript_include('be/js/jquery/select2/select2.min.js') }} -->
<!-- {{ javascript_include('be/js/modules/ui-select2.js') }} -->

<!-- CKeditor -->
{{ javascript_include('be/js/ckeditor/ckeditor.js') }}
{{ javascript_include('be/js/ckeditor/styles.js') }}


<!-- {{ javascript_include('be/js/jquery/chosen/chosen.jquery.min.js') }} -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script>
<!-- include angular-chosen -->
<script src="/vendors/angular-chosen/angular-chosen.js"></script>

<!-- include the css and sprite -->

<!-- <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular.js"></script>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css">
<script src="https://cdn.rawgit.com/angular-ui/ui-select/master/dist/select.min.js"></script>
<link rel="stylesheet" href="https://cdn.rawgit.com/angular-ui/ui-select/master/dist/select.min.css"> -->



</body>
</html>
