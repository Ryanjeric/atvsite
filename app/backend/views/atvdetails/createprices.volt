{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="imagelist.html">
  <div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="notification.html">
  <div ng-include="'/be/tpl/notification.html'"></div>
</script>
<script type="text/javascript">
  function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create ATV Prices</h1>
</div>
<div class="wrapper-md">
<form name="myForm" ng-submit="Saveatv(atv,rentalhr,price)" id="wewForm">
	<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
	<div class="row">
		<div class="col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading font-bold">
					ATV Details
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="form-group">
							<div class="col-sm-12">
								Title
								<span class="label bg-danger" ng-show="title">Title already taken. <br/></span>
								<input type="text"  id="" name="atv" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="atv.title" ng-change="chkatv(atv.title)" required>
							</div>
						</div>
						<div class="line line-dashed b-b line-lg"></div>
						<div class="form-group">
							<div class="col-sm-12">
								Subtitle (example: 2 Seaters)
								<input type="text"  id="" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="atv.subtitle" required="required">
							</div>
						</div>
						<div class="line line-dashed b-b line-lg"></div>
						<div class="form-group">
							<div class="col-sm-12">
								Features (Note: Separate features with / )
								<textarea class="form-control ng-pristine ng-invalid ng-invalid-required ng-untouched ng-valid" ng-model="atv.features" required></textarea>
							</div>
						</div>
						<div class="line line-dashed b-b line-lg"></div>
						<div class="form-group">
							<div class="col-sm-12">
								Description
								<textarea class="ck-editor" ng-model="atv.desc"></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="panel panel-default">
				<div class="panel-heading font-bold">
					ATV Image and Prices
				</div>
				<div class="panel-body">
					<input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon" required>
					<div class="panel-body">
						<div class="input-group m-b">
							<span class="input-group-btn">
							<a class="btn btn-default" ng-click="showimageList('lg')">Select Image</a>
							</span>
							<input type="text" class="form-control" ng-value="atv.picture = amazonpath " ng-model="atv.picture" placeholder="{[{amazonpath}]}" required readonly>

						</div>
					</div>
					<div class="panel-body" style="margin-top:-30px;">
						<img style="width:100%" ng-if='amazonpath != undefinded' src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/{[{amazonpath}]}" err-SRC="/img/no-image.jpg">
						<img style="width:100%" ng-if='amazonpath == undefinded' src="/img/no-image.jpg">
					</div>
					<div class="line line-dashed b-b line-lg"></div>
					<div class="panel-body">
					Rental Prices
					<div class="line line-dashed b-b line-lg"></div>


					<div ng-repeat="data in listrental">
						<div class="checkbox">
							<label class="i-checks">
								<input type="checkbox" required checked ng-model="rentalhr[$index]" name="list" value="{[{ data.id }]}" ng-true-value="'{[{ data.id }]}'" ng-change="price[$index] = rentalhr[$index] ? '' : price[$index] && $event.stopPropagation()">
								<i></i>
								{[{ data.session }]} : {[{ data.duration }]}Hr/s
							</label><br><br>
							<div class="cols-sm-12">
							<div class="input-group bootstrap-touchspin">
								<span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
								<input ui-jq="TouchSpin" type="text" data-min="1" data-max="99999" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block;" ng-disabled="!rentalhr[$index]" placeholder="$Price" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="price[$index]" required onkeypress='return isNumberKey(event)' onkeypress='return isNumberKey(event)'>
								<span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
							</div>
							</div>
						</div>
						<div class="line line-dashed b-b line-lg"></div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<footer class="panel-footer  bg-light lter">

			<div class="pull-right">
				<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
				<button type="submit" class="btn btn-success" scroll-to="Scrollup">Save</button>
			</div>
			<div style="clear:both;"></div>
		</footer>
	</div>
</form>
</div>
