{{ content() }}
<script type="text/ng-template" id="notification.html">
  <div ng-include="'/be/tpl/notification.html'"></div>
</script>
<script type="text/ng-template" id="notification2.html">
  <div ng-include="'/be/tpl/notification2.html'"></div>
</script>
<script type="text/ng-template" id="saveone.html">
  <div ng-include="'/be/tpl/saveone.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Requirements</h1>
  <a id="top"></a>
</div>

<div class="wrapper-md">
	<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading font-bold">
					Manage Requirements
				</div>

				<div class="panel-body">
					<div class="row wrapper">
                    <div class="col-sm-5 m-b-xs">
                      <form name="rEquired">
                        <div class="input-group">
                          <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" ng-change="search(searchtext)" required>
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
                          </span>
                        </div>
                      </form>
                    </div>
                    <div class="col-sm-5 m-b-xs">
                    	<button class="btn btn-success btn-addon btn-md" ng-click="newrequirements()"><i class="fa fa-plus"></i>ADD</button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th style="width:90%">Requirements</th>
                                <th style="width:10%">ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="mem in list">
                               <td>
                               	<span editable-textarea="mem.requirements" 
                               	onbeforesave="update(mem.id, mem.requirements, $data, $index)" 
                               	e-form="xeditablereq" 
                               	e-required e-rows="2" e-cols="120">
                               	{[{mem.requirements}]}
                               	</span>
                               </td>
                               <td><a ng-click="xeditablereq.$show()" ng-hide="xeditablereq.$visible">
                               <span class="label bg-info">EDIT</span>
                              </a><a ng-click="delete(mem.id)">
                               <span class="label bg-danger">DELETE</span>
                              </a></td>
                            </tr>
                        </tbody>
                    </table>
                    <center>
 					<pagination total-items="TotalItems" ng-model="CurrentPage" max-size="maxSize_tag" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(CurrentPage)"></pagination>
 				</center>
                </div>
				</div>
			</div>
		</div>
	</div>
</div>