{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="notification.html">
  <div ng-include="'/be/tpl/notification.html'"></div>
</script>
<script type="text/ng-template" id="notification2.html">
  <div ng-include="'/be/tpl/notification2.html'"></div>
</script>
<script type="text/ng-template" id="userUpdate.html">
  <div ng-include="'/be/tpl/userUpdate.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage ATV</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      ATV LIST
    </div>
    <div class="row wrapper">
          <div class="col-sm-12">   
                    <div class="col-sm-6">
                    <form name="rEquired">
                      <div class="input-group">
                      
                        <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)"> 
                        <span class="input-group-btn">
                          
                          <button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
                        </span>
                      </div>
                        </form>
                    </div>
                  </div>
    </div>
    <div class="col-sm-12">
      <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    </div>
    <div class="table-responsive">
     <!--  <input type="hidden" ng-init='userdata = {{ data }}'> -->
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th>Title</th>
            <th>Subtitle</th>
            <th>Features</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="list in list">
            <td>{[{ list.title }]}</td>
            <td>{[{ list.subtitle }]}</td>
            <td>{[{ list.features }]}</td>
            <td>
              <button ng-click="update(list.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>

              <button ng-click="delete(list.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
                <div class="panel-body">
                    <footer class="panel-footer text-center bg-light lter">
                      <pagination total-items="TotalItems" ng-model="CurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(CurrentPage)"></pagination>
                    </footer>
                </div>
      </div>
    </footer>
  </div>
</div>