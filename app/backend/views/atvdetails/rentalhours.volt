{{ content() }}
<script type="text/ng-template" id="notification.html">
    <div ng-include="'/be/tpl/notification.html'"></div>
</script>
<script type="text/ng-template" id="notification2.html">
    <div ng-include="'/be/tpl/notification2.html'"></div>
</script>
<script type="text/ng-template" id="newrentalhr.html">
    <div ng-include="'/be/tpl/newrentalhr.html'"></div>
</script>
<script type="text/ng-template" id="updaterentalhr.html">
    <div ng-include="'/be/tpl/updaterentalhr.html'"></div>
</script>

<div id="top" class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Rental Hours</h1>
</div>

<div id="Scrollup"></div>
<div class="wrapper-md">
  <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">
          Manage Rental Hours
        </div>

        <div class="panel-body">
          <div class="row wrapper">
                    <div class="col-sm-5 m-b-xs">
                      <form name="rEquired">
                        <div class="input-group">
                          <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" ng-change="search(searchtext)" required>
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
                          </span>
                        </div>
                      </form>
                    </div>
                    <div class="col-sm-5 m-b-xs">
                      <button class="btn btn-success btn-addon btn-md" ng-click="addrentalhours()"><i class="fa fa-plus"></i>ADD</button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th style="width:15%">Session</th>
                                <th style="width:15%">Hours</th>
                                <th style="width:15%">Start</th>
                                <th style="width:15%">End</th>
                                <th style="width:15%">ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="mem in list">
                               <td>{[{mem.session}]}</td>
                               <td>{[{mem.duration}]}HR</td>
                               <td>{[{mem.starttime}]}</span></td>
                               <td>{[{mem.endtime}]}</span></td>
                               <td>
                                  <button class="label bg-info" ng-click="update(mem.id)">EDIT</button>
                                  <button class="label bg-danger" ng-click="delete(mem.id)">DELETE</button>
                              </td>
                            </tr>
                        </tbody>
                    </table>
                    <center>
          <pagination total-items="TotalItems" ng-model="CurrentPage" max-size="maxSize_tag" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(CurrentPage)"></pagination>
        </center>
                </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">
          Update Rental Message
        </div>
        <div class="panel-body">
          <form ng-submit="Savemsg(msg)">
          <div class="row wrapper">
              <textarea class="ck-editor" ng-model="msg.content" required></textarea>
          </div>
          <div class="row">
            <div class="panel-body">
              <footer class="panel-footer  bg-light lter">

                <div class="pull-right">
                  <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
                  <button type="submit" class="btn btn-success" scroll-to="Scrollup">Save</button>
                </div>
                <div style="clear:both;"></div>
              </footer>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>

</div>
