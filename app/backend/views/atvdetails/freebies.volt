{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="notification.html">
	<div ng-include="'/be/tpl/notification.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">Freebies,Delivery Service Message & Special offers</h1>
	<a id="top"></a>
</div>
<div class="wrapper-md">
	<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
	<div class="row">
		<div class="panel-body">
			<tabset class="tab-container">
				<tab heading="Update Freebies">
					<div class="panel-body">
						<div class="col-sm-12">
							<form ng-submit="Save(msg)">
								<div class="row wrapper">
									<textarea class="ck-editor" ng-model="msg.content" required></textarea>
								</div>
								<div class="row">
									<div class="panel-body">
										<footer class="panel-footer  bg-light lter">

											<div class="pull-right">
												<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
												<button type="submit" class="btn btn-success" scroll-to="Scrollup">Save</button>
											</div>
											<div style="clear:both;"></div>
										</footer>
									</div>
								</div>	
							</form>
						</div>
					</div>
				</tab>
				<tab heading="Update Delivery Service Message">
					<div class="panel-body">
						<div class="col-sm-12">
							<form ng-submit="Save1(msg1)">
								<div class="row wrapper">
									<textarea class="ck-editor" ng-model="msg1.content" required></textarea>
								</div>
								<div class="row">
									<div class="panel-body">
										<footer class="panel-footer  bg-light lter">

											<div class="pull-right">
												<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
												<button type="submit" class="btn btn-success" scroll-to="Scrollup">Save</button>
											</div>
											<div style="clear:both;"></div>
										</footer>
									</div>
								</div>
							</form>
						</div>
					</div>
				</tab>
				<tab heading="Update Special offers">
					<div class="panel-body">
						<div class="col-sm-12">
							<form ng-submit="Save2(msg2)">
								<div class="row wrapper">
									<textarea class="ck-editor" ng-model="msg2.content" required></textarea>
								</div>
								<div class="row">
									<div class="panel-body">
										<footer class="panel-footer  bg-light lter">

											<div class="pull-right">
												<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
												<button type="submit" class="btn btn-success" scroll-to="Scrollup">Save</button>
											</div>
											<div style="clear:both;"></div>
										</footer>
									</div>
								</div>
							</form>
						</div>
					</div>
				</tab>
			</tabset>
		</div>
	</div>
</div>