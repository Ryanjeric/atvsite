{{ content() }}
<style>
.i-switch i:before {
    background-color: gray!important;
    transition: all 0.3s;
}
.status {
  position:relative;
  top:-6.7px;
  display:inline-block;
  min-width:60px;
}

table tbody tr td { vertical-align:middle!important; }
</style>
<div id="Scrollup"></div>
<script type="text/ng-template" id="notification.html">
  <div ng-include="'/be/tpl/notification.html'"></div>
</script>
<script type="text/ng-template" id="userUpdate.html">
  <div ng-include="'/be/tpl/userUpdate.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">User List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Users
    </div>
    <div class="row wrapper">
          <div class="col-sm-12">   
                    <div class="col-sm-6">
                    <form name="rEquired">
                      <div class="input-group">
                      
                        <input class="input-sm form-control" placeholder="Search by name, email and username" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchrole)"> 
                        <span class="input-group-btn">
                          
                          <button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
                        </span>
                        <select ng-model="searchrole" class="form-control input-sm" ng-change="search(searchtext,searchrole)">
                          <option value="" style="display:none">Filter</option>
                          <option value="undefined">List All</option>
                          <option value="superadmin">Administrator</option>
                          <option value="Users">Users</option>
                          <option value="Gallery">Gallery</option>
                          <option value="News">News</option>
                          <option value="Contacts">Contacts</option>
                          <option value="ATV">ATV</option> 
                          <option value="Trails">Trails</option>
                          <option value="Reservations">Reservations</option>
                          <option value="Meta">Meta</option>  
                          <option value="Settings">Settings</option>            
                        </select>
                      </div>
                        </form>
                    </div>
                  </div>
    </div>
    <div class="col-sm-12">
      <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    </div>
    <div class="table-responsive">
     <!--  <input type="hidden" ng-init='userdata = {{ data }}'> -->
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th style="width:10%">Name</th>
            <th style="width:10%">Email</th>
            <th style="width:10%">Username</th>
            <th style="width:10%">Status</th>
            <!-- <th style="width:10%">User Role</th> -->
            <th style="width:10%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="user in data.data" ng-if="user.username != 'superagent' && user.username != loggeduser">
            <td>{[{ user.first_name }]} {[{ user.last_name }]}</td>
            <td>{[{ user.email }]}</td>
            <td>{[{ user.username }]}</td>
            <td>
              <div ng-if="user.status == 1">
                <span class="label bg-success status">Active</span>
                <label class="i-switch bg-info m-t-xs m-r">
                  <input type="checkbox" 
                  ng-click="changestatus(user.id,user.status)" checked>
                  <i></i>
                </label>
              </div>
              <div ng-if="user.status == 0">
                <span class="label bg-danger status">Inactive</span>
                <label class="i-switch bg-info m-t-xs m-r">
                  <input type="checkbox" 
                  ng-click="changestatus(user.id,user.status)">
                  <i></i>
                </label>
              </div>
            </td>
          <!--   <td>
              <span>{[{ user.userrole }]}</span> -->
            <td>
              <button ng-click="update(user.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Edit</button>

              <button ng-click="delete(user.id)" class="btn m-b-xs btn-xs btn-danger btn-addon">Delete</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
                <div class="panel-body">
                    <footer class="panel-footer text-center bg-light lter">
                      <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                    </footer>
                </div>
      </div>
    </footer>
  </div>
</div>