{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="imagelist.html">
  <div ng-include="'/be/tpl/imagelist.html'"></div>
</script>
<script type="text/ng-template" id="notification.html">
  <div ng-include="'/be/tpl/notification.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit User</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="updateData(user)" name="myForm" id="wewForm">
  <div class="wrapper-md" >
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">
          Account Information
        </div>
        <div class="panel-body">
          <input type="hidden" ng-model="user.id">
          <input type="hidden" ng-model="user.profile">
          <div class="form-group">
            <label class="col-sm-3 control-label">Username</label>
            <div class="col-sm-9">

              <span class="label bg-danger" ng-show="usrname">Username already taken. <br/></span>
              <input type="text" ng-Space  class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.username" ng-change="chkusername(user.username)" required="required" pattern=".{4,15}" maxlength="15">
              <em class="text-muted">(allow 'a-zA-Z0-9', 4-15 length)</em>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg"></div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Email Address</label>
            <div class="col-sm-9">
              <span class="label bg-danger" ng-show="usremail">Email Address already taken.</span>
              <input type="email" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.email" ng-change="chkemail(user.email)" required="required" pattern=".{5,200}" maxlength="200">
            </div>
          </div>

          <div class="line line-dashed b-b line-lg"></div>
          CHANGE PASSWORD?
          <label class="i-switch i-switch-md bg-info m-t-xs m-r">
            <input type="checkbox"
            ng-init="isCollapsed = false"
            ng-click="disableditu = !disableditu;
            user.newpassword = undefined;
            user.password_c = undefined;
            user.oldpassword = '';
            isCollapsed = !isCollapsed;">
            <i></i>
          </label>
        </div>
        <div collapse="!isCollapsed" class="panel-body collapse out" style="height: auto;">
         <div class="form-group">
          <label class="col-sm-3 control-label">Old Password</label>
          <div class="col-sm-9">
            <span class="label bg-danger" ng-show="old">Old Password not Match<br/></span>
            <input type="password" name="oldpassword" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.oldpassword" ng-disabled="!disableditu"  maxlength="50">
          </div>
        </div>
        <div class="line line-dashed b-b line-lg pull-in"></div>
        <div class="form-group">
          <label class="col-sm-3 control-label">New Password</label>
          <div class="col-sm-9">
            <span class="label bg-danger" ng-show="pwdshort">Password must be at least 6 Characters!<br/></span>
            <input type="password" name="newpassword" ng-model="user.newpassword" ng-disabled="!disableditu" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-change="confirmpass(user.password_c, user.newpassword)" pattern=".{5,50}" maxlength="50"/>
          </div>
        </div>
        <div class="line line-dashed b-b line-lg pull-in"></div>
        <div class="form-group">
          <label class="col-sm-3 control-label">Repeat Password</label>
          <div class="col-sm-9">
            <span class="label bg-danger" ng-show="pwdconfirm">Password did not match! <br/></span>
            <input type="password" name="password_c" ng-model="user.password_c" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-disabled="!disableditu" ng-change="confirmpass(user.password_c, user.newpassword)" pattern=".{5,50}" maxlength="50"/>
          </div>
        </div>
      </div>
      <div class="line line-dashed b-b line-lg"></div>
      <div class="form-group">
              <label class="col-lg-3 control-label">User Restriction</label>
              <div class="col-sm-9">
               <div class="checkbox">
                <label class="i-checks">
                  <input class="super" type="checkbox" name='list' ng-model="user.superadmin" ng-true-value="'admin'" id="checkSurfaceEnvironment-1" ng-checked="selected['superadmin']" ng-change="checkifchecked() && $event.stopPropagation()">
                  <i></i>
                  Administrator
                </label>
                </div><div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="checkbox">
                  <label class="i-checks">
                    <input class="role" type="checkbox" name="list" ng-model="user.role1" ng-true-value="'Users'" ng-checked="selected['Users']" ng-disabled="adminischeck">
                    <i></i>
                    Users
                  </label>
                </div><div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="checkbox">
                  <label class="i-checks">
                    <input class="role" type="checkbox" name="list" ng-model="user.role2" ng-true-value="'Gallery'" ng-checked="selected['Gallery']" ng-disabled="adminischeck">
                    <i></i>
                    Gallery
                  </label>
                </div><div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="checkbox">
                  <label class="i-checks">
                    <input class="role" type="checkbox" name="list" ng-model="user.role3" ng-true-value="'News'" ng-checked="selected['News']" ng-disabled="adminischeck">
                    <i></i>
                    News
                  </label>
                </div><div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="checkbox">
                  <label class="i-checks">
                    <input class="role" type="checkbox" name="list" ng-model="user.role4" ng-true-value="'Contacts'" ng-checked="selected['Contacts']" ng-disabled="adminischeck">
                    <i></i>
                    Contacts
                  </label>
                </div><div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="checkbox">
                  <label class="i-checks">
                    <input class="role" type="checkbox" name="list" ng-model="user.role5" ng-true-value="'ATV'" ng-checked="selected['ATV']" ng-disabled="adminischeck">
                    <i></i>
                    ATV
                  </label>
                </div><div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="checkbox">
                  <label class="i-checks">
                    <input class="role" type="checkbox" name="list" ng-model="user.role6" ng-true-value="'Trails'" ng-checked="selected['Trails']" ng-disabled="adminischeck">
                    <i></i>
                    Trails
                  </label>
                </div><div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="checkbox">
                  <label class="i-checks">
                    <input class="role" type="checkbox" name="list" ng-model="user.role7" ng-true-value="'Reservations'" ng-checked="selected['Reservations']" ng-disabled="adminischeck">
                    <i></i>
                    Reservations
                  </label>
                </div><div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="checkbox">
                  <label class="i-checks">
                    <input class="role" type="checkbox" name="list" ng-model="user.role8" ng-true-value="'Meta'" ng-checked="selected['Meta']" ng-disabled="adminischeck">
                    <i></i>
                    Meta data
                  </label>
                </div><div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="checkbox">
                  <label class="i-checks">
                    <input class="role" type="checkbox" name="list" ng-model="user.role9" ng-true-value="'Settings'" ng-checked="selected['Settings']" ng-disabled="adminischeck">
                    <i></i>
                    Settings
                  </label>
                </div>
              </div>
            </div>
      <div class="line line-dashed b-b line-lg"></div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="panel panel-default" >
      <div class="panel-heading font-bold">
       User Profile
     </div>
     <div class="form-group">
       <br>
       <label class="col-sm-3 control-label">Profile Picture</label>
       <input id="amzon" type="hidden" name="submain" ng-init="amazon='<?php echo $this->config->application->amazonlink; ?>'" ng-model="amazon" >
       <div class="col-sm-9 panel-body">
         <img class="customimg" ng-if='amazonpath != undefinded' src="<?php echo $this->config->application->amazonlink; ?>/uploads/images/{[{amazonpath}]}"  err-SRC="/img/noimg.jpg" alt="IMAGE PREVIEW">
         <img class="customimg" ng-if='amazonpath == undefinded' src="/img/noimg.jpg" alt="IMAGE PREVIEW">
        <div class="input-group m-b">
          <span class="input-group-btn">
            <a class="btn btn-default listimage" ng-click="showimageList('lg')">Select Image</a>
          </span>
          <input type="hidden" class="form-control" ng-value="user.banner = amazonpath " ng-model="user.banner" placeholder="{[{amazonpath}]}" readonly>
        </div>
      </div>
    </div>
    <div class="line line-dashed b-b line-lg"></div>

    <div class="form-group">
      <label class="col-sm-3 control-label">First Name</label>
      <div class="col-sm-9">
        <input type="text" id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.fname" required="required" pattern=".{1,50}" maxlength="50">
      </div>
    </div>
    <div class="line line-dashed b-b line-lg"></div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Last Name</label>
      <div class="col-sm-9">
        <input type="text" id="" name="" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="user.lname" required="required" pattern=".{1,50}" maxlength="50">
      </div>
    </div>
    <div class="line line-dashed b-b line-lg"></div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Birth Date</label>
      <div class="col-sm-9">
        <div class="input-group w-md">
          <span class="input-group-btn">
            <input type="hidden" required="required" ng-value="user.hiddendate = user.bday" ng-model="user.hiddendate" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" required="required">
            <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="user.bday" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
            <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
          </span>
        </div>
      </div>
    </div>
    <div class="line line-dashed b-b line-lg"></div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Gender</label>
      <div class="col-sm-9">
        <div class="radio">
          <label class="i-checks">
            <input type="radio" name="gender" value="Male" ng-model="user.gender" required="required">
            <i></i>
            Male
          </label>
        </div>
        <div class="radio">
          <label class="i-checks">
            <input type="radio" name="gender" value="Female" ng-model="user.gender" required="required">
            <i></i>
            Female
          </label>
        </div>
      </div>
    </div>
    <div class="line line-dashed b-b line-lg"></div>
    <div class="form-group">
      <label class="col-sm-3 control-label">STATUS</label>
      <div class="col-sm-9">
        <div class="radio">
          <label class="i-checks">
            <input type="radio" name="status" value="1" ng-model="user.status" required="required">
            <i></i>
            Active User
          </label>
        </div>
        <div class="radio">
          <label class="i-checks">
            <input type="radio" name="status" value="0" ng-model="user.status" required="required">
            <i></i>
            Deactivate User
          </label>
        </div>
      </div>
    </div>
    <div class="line line-dashed b-b line-lg"></div>
    <div class="form-group">
      <div class="col-sm-offset-3">
        <button type="button" class="btn btn-default" ui-sref="userlist">Cancel</button>
        <button type="submit" class="btn btn-success" ng-disabled="myForm.$invalid || myForm.$pending || usrname==true || usremail==true" id="disabled"  scroll-to="Scrollup">Update</button>
      </div>
      <div class="line line-lg"></div>
    </div>
  </div>  </div>
</form>
