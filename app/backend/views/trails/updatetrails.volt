{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="notification.html">
	<div ng-include="'/be/tpl/notification.html'"></div>
</script>
<script type="text/javascript">
	$('.decimal').keyup(function(){
		var val = $(this).val();
		if(isNaN(val)){
			val = val.replace(/[^0-9\.]/g,'');
			if(val.split('.').length>2)
				val =val.replace(/\.+$/,"");
		}
		$(this).val(val);
	});
</script>
<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">Update Trails</h1>
</div>
<div class="wrapper-md">
	<form name="myForm" ng-submit="Savetrails(trails,picFile,picFile1,picFile2,rentalhr,map)" id="wewForm">
		<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
		<div class="progress" style="width:97%; margin:0 20px;">
			<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuetext="{[{ wait }]}" aria-valuemin="0" aria-valuemax="100" style="width: {[{ process }]}%;">
				<span class="sr-only">{[{ process }]}% Complete</span>
			</div>
		</div><br>
		<div class="row">
			<div class="col-sm-8">
				<div class="panel panel-default">
					<div class="panel-heading font-bold">
						Trails Details
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="form-group">
								<div class="col-sm-12">
									Category
									<select name="atv" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="trails.category" required="required">
										<option value="" style="display:none">Choose</option>
										<option value="sedona">Sedona</option>
										<option value='cottonwood'>Sedona & Cottonwood</option>
										<option value="jerome">Jerome</option>
										<option value="cornville">Cornville</option>
									</select>
								</div>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group">
								<div class="col-sm-12">
									Title
									<input type="hidden" ng-model="trails.id">
									<span class="label bg-danger" ng-show="title">Title already taken. <br/></span>
									<input type="text" name="trails" class="form-control ng-invalid ng-invalid-required ng-valid-pattern"
													ng-model="trails.title"
													ng-change="ngchangetitle(trails.title, disableslugs = true)" required="required">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group">
								<div class="col-sm-12">
									News Slugs:
									<div class="input-group">
										<input type="text" class="form-control" name="slugs"
												ng-model="trails.slugs"
												ng-disabled="disableslugs"
												ng-pattern="/^([a-z0-9])([a-z0-9-])+[a-z0-9]$/"
												ng-change="ensureuniqueslugs(trails.slugs)"
												required>
										<span class="input-group-btn" ng-show="disableslugs">
											<a class="btn btn-default" type="button" ng-click="disableslugs = false"><i class="icon icon-note"></i></a>
										</span>
										<span class="input-group-btn" ng-hide="disableslugs">
											<a class="btn btn-success" type="button" ng-click="disableslugs = true"><i class="icon icon-pin"></i></a>
										</span>
										<span class="input-group-btn">
											<a class="btn btn-default" type="button" ng-click="ngchangetitle(trails.title, disableslugs = true)"><i class="fa fa-refresh"></i></a>
										</span>
									</div>
									<em class="text-danger" ng-show="myForm.slugs.$error.unique">That Trail Slugs exist, please change it. Save button is disabled!</em>
									<em class="text-danger" ng-show="myForm.slugs.$error.pattern">Starts & ends with alphanumeric. White spaces is not allowed only dashed. No capital letters. Minimum of 3 characters. Save button is disabled!</em>
								</div>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group">
								<div class="col-sm-6">
									Distance in miles
									<input type="text" name="atv" class="form-control ng-invalid ng-invalid-required ng-valid-pattern decimal" ng-model="trails.distance" required="required">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-6">
									Elevation
									<input type="text" name="atv" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="trails.elevation" required="required">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group">
								<div class="col-sm-6">
									Difficulty level from :
									<select name="atv" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="trails.dlf" required="required">
										<option value="" style="display:none">Choose</option>
										<option value="easy">Easy</option>
										<option value='moderate'>Moderate</option>
										<option value="difficult">Difficult</option>
									</select>
								</div>

								<div class="col-sm-6">
									Difficulty level to :
									<select name="atv" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="trails.dlt" required="required">
										<option value="" style="display:none">Choose</option>
										<option value="easy">Easy</option>
										<option value='moderate'>Moderate</option>
										<option value="difficult">Difficult</option>
									</select>
								</div>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
								<div class="form-group">
									<div class="col-sm-12">
										What to bring
										<textarea class="form-control ng-pristine ng-invalid ng-invalid-required ng-untouched ng-valid" ng-model="trails.wtb"></textarea>
									</div>
								</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group">
								<div class="col-sm-12">
									Description
									<textarea class="ck-editor" ng-model="trails.desc"></textarea>
								</div>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group">
								<div class="col-sm-12">
									Meta Title
									<input type="text" name="atv" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="trails.metatitle" required="required">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group">
								<div class="col-sm-12">
									Meta Keyword
									<input type="text" name="atv" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="trails.metakeyword" required="required">
								</div>
							</div>
							<div class="line line-dashed b-b line-lg"></div>
							<div class="form-group">
								<div class="col-sm-12">
									Meta Description
									<textarea class="form-control ng-pristine ng-invalid ng-invalid-required ng-untouched ng-valid" ng-model="trails.metadesc"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
				<div class="panel-heading font-bold">
					Image and Maps (Note : Maximum Size to upload is 2Mb)<br><br>
					(For Best Viewed Pictures Upload Image Size should be 1500px Width and 844px Height)
				</div>
				<div class="panel-body">
					<tabset class="tab-container">
						<tab heading="Featured Image">
							<div class="panel-body">
								<div class="input-group m-b">
									<label class="label_profile_pic btn btn-primary" id="change-picture" ngf-select ng-model="$parent.$parent.picFile" name="file"
									accept="image/*" ngf-max-size="2000000" required="required">
									Select Featured Image
								</label>
							</div>
						</div>
						<div class="panel-body" style="margin-top:-30px;">
							<img style="width:100%" ngf-src="!picFile.$error && picFile || '/img/no-image.jpg'">
						</div>
						</tab>
						<tab heading="1st Image">
							<div class="panel-body">
								<div class="input-group m-b">
									<label class="label_profile_pic btn btn-success" id="change-picture" ngf-select ng-model="$parent.$parent.picFile1" name="file1"
									accept="image/*" ngf-max-size="2000000" required="required">
									Select 1st Image
								</label>
							</div>
						</div>
						<div class="panel-body" style="margin-top:-30px;">
							<img style="width:100%" ngf-src="!picFile1.$error && picFile1 || '/img/no-image.jpg'">
						</div>
						</tab>
						<tab heading="2nd Image">
							<div class="panel-body">
								<div class="input-group m-b">
									<label class="label_profile_pic btn btn-success" id="change-picture" ngf-select ng-model="$parent.$parent.picFile2" name="file2"
									accept="image/*" ngf-max-size="2000000" required="required">
									Select 2nd Image
								 </label>
							</div>
						</div>
						<div class="panel-body" style="margin-top:-30px;">
							<img style="width:100%" ngf-src="!picFile2.$error && picFile2 || '/img/no-image.jpg'">
						</div>
						</tab>
					</tabset>

					<div class="panel-body">
					Maps (Note : Any Image size upload)
					<div class="line line-dashed b-b line-lg"></div>
					<div ng-repeat="data in listrental">
						<div class="checkbox">
							<label class="i-checks">
								<input type="checkbox" ng-model="rentalhr[data.id]" name="list" value="{[{ data.id }]}" ng-true-value="'{[{ data.id }]}'" ng-checked="selected[data.id]" ng-click="selected[data.id] = !selected[data.id];remove(data.id)" >  <!-- ng-change="map[data.id] = rentalhr[data.id] ? '' : map[data.id] && $event.stopPropagation()" -->
								<i></i>
								{[{ data.session }]} : {[{ data.hours }]}Hr/s
							</label><br><br>
							<div class="cols-sm-12" ng-show="selected[data.id]"> <!--  -->
								<div class="line line-dashed b-b line-lg"></div>
								<div class="panel-body">
									<div class="input-group m-b">
										<input type="file" class="label_profile_pic btn btn-success" id="{[{ data.id }]}" ngf-select ng-disabled="!selected[data.id]" ng-model="map[data.id]"
										 name="file[$index]"
										accept="image/*" ngf-max-size="2000000" ng-required="map[data.id] != map1[data.id]" onchange="angular.element(this).scope().setFile(this)">
									</label>
									</div>
								</div>
								<div class="panel-body" style="margin-top:-30px;">
									<img style="width:100%" ngf-src="!map[data.id].$error && map[data.id] || '/img/no-image.jpg'">
								</div>
								<div class="line line-dashed b-b line-lg"></div>
							</div>
						</div>
						<div class="line line-dashed b-b line-lg"></div>
					</div>
					</div>
				</div>

			</div>
		</div>
		<div class="panel-body">
		<footer class="panel-footer  bg-light lter">

			<div class="pull-right">
				<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
				<button type="submit" class="btn btn-success" scroll-to="Scrollup" ng-disabled="myForm.slugs.$invalid">Save</button>
			</div>
			<div style="clear:both;"></div>
		</footer>
		</div>
	</form>
</div>
