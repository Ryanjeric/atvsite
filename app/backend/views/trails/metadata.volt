{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="metadata.html">
  <div ng-include="'/be/tpl/metatrails.html'"></div>
</script>
<script type="text/ng-template" id="notification2.html">
  <div ng-include="'/be/tpl/notification2.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Trails Category Meta data</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Meta data
    </div>
    <div class="col-sm-12">
      <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    </div>
    <div class="table-responsive">
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th>Category</th>
            <th>Title</th>
            <th>Meta Title</th>
            <th>Meta Keywords</th>
            <th>Meta Description</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="data in list">
              <td><b>{[{ data.trailcategory }]}</b></td>
              <td>{[{ data.title }]}</td>
              <td>{[{ data.metatitle }]}</td>
              <td>{[{ data.metakeyword }]}</td>
              <td>{[{ data.metadescription }]}</td>
            <td>
              <button ng-click="update(data.id)" class="btn m-b-xs btn-xs btn-info btn-addon">Update</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
                <div class="panel-body">
                    <footer class="panel-footer text-center bg-light lter">
                      <pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></pagination>
                    </footer>
                </div>
      </div>
    </footer>
  </div>
</div>