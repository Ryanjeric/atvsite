<!DOCTYPE html>
<html lang="en" data-ng-app="app">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  {{ stylesheet_link('be/css/bootstrap.css') }}
  {{ stylesheet_link('be/css/animate.css') }}
  {{ stylesheet_link('be/css/font-awesome.min.css') }}
  {{ stylesheet_link('be/css/simple-line-icons.css') }}
  {{ stylesheet_link('be/css/font.css') }}
  {{ stylesheet_link('be/css/app.css') }}
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="be/js/html5shim.js"></script>
  <![endif]-->

  
  <link rel="shortcut icon" href="/img/favicon.ico">
</head>

<body ng-controller="Forgotpasswordctrl">
  <div class="container w-xxl w-auto-xs">
  <a href class="navbar-brand block m-t">RIO VERDE ATV RENTAL</a>
  <div class="m-b-lg">
    <div class="wrapper text-center">
      <a href="/atvadmin"><img src='/img/atvlogo.png'></a>
    </div>
    <div class="wrapper text-center" ng-show="authError">
        <h3>{[{ alerts }]}</h3>
    </div>
      <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="send(forgot)" name="formNews" id="formNews" ng-show="validform">
      <div class="text-danger wrapper text-center" ng-show="authError">
     
      </div>
      <div class="list-group list-group-sm">
        <div class="list-group-item">
          <input type="email" class="form-control" ng-model="forgot.email" name="forgot.email" required="required" placeholder="Email Address">
        </div>
      </div>
      <button type="submit" class="btn btn-lg btn-primary btn-block" ng-disabled='form.$invalid'>Send Password Reset</button>
      <div class="line line-dashed"></div>
      <p class="text-center"><small>Dont share your password to anyone.</small></p>
    </form>
  </div>
 
</div>
<!-- JS -->
{{ javascript_include('be/js/jquery/jquery.min.js') }}
<!-- angular -->
{{ javascript_include('be/js/angular/angular.min.js') }}

<!-- APP -->

 
</body>
</html>
<script type="text/javascript">
'use strict';

var app = angular.module('app', [
    ])
  .config(function ($interpolateProvider){

     $interpolateProvider.startSymbol('{[{');
     $interpolateProvider.endSymbol('}]}');

   })
</script>
{{ javascript_include('be/js/scripts/config.js') }}
<script type="text/javascript">
  app.controller('Forgotpasswordctrl', function ($scope,$http,Config){
    $scope.validform =true;

     $scope.send = function(forgot){
        $http({
          url: Config.ApiURL +"/forgotpassword/send/yo/"+forgot.email,
          method: "POST",
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
        }).success(function (data, status, headers, config) {
          $scope.authError = true;
          $scope.alerts = data.msg;
          $scope.forgot.email = "";
          if(data.msg == 'No account found with that email address.')
          {
             $scope.validform =true;
          }
          else
          {
            $scope.validform =false;
          }
        }).error(function(data, status, headers, config) {

        });
      }   

  })
  ;
</script>