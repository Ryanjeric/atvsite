{{ content() }}
<script type="text/ng-template" id="notification.html">
  <div ng-include="'/be/tpl/notification.html'"></div>
</script>
<script type="text/ng-template" id="reservationimglist.html">
  <div ng-include="'/be/tpl/reservationimglist.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Page Banners</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="row">
    <div class="col-sm-12">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index, 'main')">{[{ alert.msg }]}</alert>
    </div>
    <div class="panel-body">
      <tabset class="tab-container">

        <tab heading="{[{ page.page }]}" ng-repeat="page in pages">
          <div class="panel-body">
            <div class="col-sm-8">
              <button type="button" class="btn btn-default btn-xs pull-right" ng-click="showimageList('lg', $index)">Media Gallery</button>
              <p>Click select to add image or select an existing image.</p>
              <div style="background-image: url('{[{ amazonlink }]}/uploads/images/{[{ page.banner }]}'); height: 250px;" ng-if="page.banner" class="bg-image"></div>
              
              <div ng-if="page.banner" class="preview">
                <div class="line line-dashed b-b line-lg"></div>
                <h4>Preview <small>({[{ preview }]})</small>
                  <button class="btn btn-default btn-sm pull-right" ng-class="{ 'active' : (preview=='Mobile')}" ng-click="preview = 'Mobile'"><i class="fa fa-mobile"></i></button>
                  <button class="btn btn-default btn-sm pull-right" ng-class="{ 'active' : (preview=='Desktop')}" ng-click="preview = 'Desktop'"><i class="fa fa-desktop"></i></button>
                </h4>

                <div class="full-width" ng-if="preview=='Desktop'">
                  <div class="header" style="background-image: url('{[{ amazonlink }]}/uploads/images/{[{ page.banner }]}');">
                    <div class="header_tip" style="background: {[{ page.bgcolor }]}" ng-class="{ 'left' : (page.alignment == 'left'), 'center' : (page.alignment == 'center'), 'right' : (page.alignment == 'right'), 'top-45' : (page.id!=1)}"
                    ng-if="page.title != undefined || page.subtitle != undefined || page.btntxt != undefined">
                      <span class="title" style="color: {[{ page.txtcolor }]}">{[{ page.title }]}</span>
                      <br>
                      <span class="subtitle" style="color: {[{ page.txtcolor }]}">{[{ page.subtitle }]}</span>
                      <br>
                      <span class="reserve" ng-if="page.btntxt">
                          <a href="{[{ page.link }]}" target="_blank" class="reserve_label">{[{ page.btntxt }]}</a>
                      </span>
                    </div>

                  </div>
                </div>

                <div class="mobile" ng-if="preview=='Mobile' && page.page !='Home'">
                  <div class="header bg-image" style="background-image: url('{[{ amazonlink }]}/uploads/images/{[{ page.banner }]}');">
                    <div class="header_tip bot" style="background: {[{ page.bgcolor }]}" ng-if="page.title != undefined || page.subtitle != undefined || page.btntxt != undefined">
                      <span class="title" style="color: {[{ page.txtcolor }]}">{[{ page.title }]}</span>
                      <br>
                      <span class="subtitle" style="color: {[{ page.txtcolor }]}">{[{ page.subtitle }]}</span>
                      <br>
                      <span class="reserve" ng-if="page.btntxt">
                        <a href="<?php echo $reservation;?>"class="btn btn-lg glyphicon glyphicon-calendar reserve_icon" aria-hidden="true" title="RESERVE YOUR RIDE!"></a>
                      </span>
                    </div>
                  </div>
                </div>

                <div class="mobile bot" ng-if="preview=='Mobile' && page.page =='Home'">
                  <div class="header bg-image bg-right" style="background-image: url('{[{ amazonlink }]}/uploads/images/{[{ page.banner }]}');">
                    <div class="header_tip" style="background: {[{ page.bgcolor }]}" ng-if="page.title != undefined || page.subtitle != undefined || page.btntxt != undefined">
                      <span class="title" style="color: {[{ page.txtcolor }]}">{[{ page.title }]}</span>
                      <br>
                      <span class="subtitle" style="color: {[{ page.txtcolor }]}">{[{ page.subtitle }]}</span>
                      <br>
                      <span class="reserve" ng-if="page.btntxt">
                        <a href="<?php echo $reservation;?>"class="btn btn-lg glyphicon glyphicon-calendar reserve_icon" aria-hidden="true" title="RESERVE YOUR RIDE!"></a>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  <div class="panel-title">Banner details</div>
                </div>
                <div class="panel-body">
                  Title (maxlength of 60)<em>(Optional)</em>
                  <input type="text" id="imagethumbsubtitle" name="imagethumbsubtitle" class="form-control" ng-model="page.title" maxlength="60">
                  <div class="line line-dashed b-b line-lg"></div>

                  Sub title (maxlength of 255)<em>(Optional)</em>
                  <textarea type="text" id="imagethumbsubtitle" name="imagethumbsubtitle" class="form-control" ng-model="page.subtitle" rows="4"></textarea>
                  <div class="line line-dashed b-b line-lg"></div>

                  <div class="form-group hiddenoverflow">
                    <div class="col-sm-4 control-label">
                      <label for="title">Banner Alignment</label>
                    </div>
                    <div class="col-sm-8">
                      <div class="btn-group">
                        <label class="btn btn-success" ng-model="page.alignment" btn-radio="'left'">Left</label>
                        <label class="btn btn-success" ng-model="page.alignment" btn-radio="'center'">Center</label>
                        <label class="btn btn-success" ng-model="page.alignment" btn-radio="'right'">Right</label>
                      </div>
                    </div>
                  </div>

                  <div class="line line-dashed b-b line-lg"></div>
                  <div class="form-group hiddenoverflow">
                    <div class="col-sm-4 control-label">
                      <label for="title">Text Color</label>
                    </div>
                    <div class="col-sm-8 colorpickerinp">
                      <input colorpicker="hex" type="text" ng-model="page.txtcolor" class="form-control pull-left" />
                      <div class="color" style="background:{[{ page.txtcolor }]}"></div>
                    </div>
                  </div>

                  <div class="line line-dashed b-b line-lg"></div>
                  <div class="form-group hiddenoverflow">
                    <div class="col-sm-4 control-label">
                      <label for="title">Box Color</label>
                    </div>
                    <div class="col-sm-8 colorpickerinp">
                      <input colorpicker="rgba" type="text" ng-model="page.bgcolor" class="form-control pull-left" />
                      <div class="color" style="background:{[{ page.bgcolor }]}"></div>
                    </div>
                  </div>
                  
                  <div class="line line-dashed b-b line-lg"></div>
                  Button <em>(Optional)</em>
                  <input type="text" id="imagethumbsubtitle" name="imagethumbsubtitle" class="form-control" ng-model="page.btntxt" maxlength="60">
                  <div class="line line-dashed b-b line-lg"></div>
                  Button Link <em>(Optional)</em>
                  <input type="text" id="imagethumbsubtitle" name="imagethumbsubtitle" class="form-control" ng-model="page.link" maxlength="60">
                  <div class="line line-dashed b-b line-lg"></div>
                  <div class="col-sm-12">
                    <a ui-sref="dashboard" class="btn btn-default" href="/dashboard"> Cancel </a>
                    <button type="submit" class="btn btn-success pull-right" ng-disabled="!page.banner" ng-click="updatebanner(page);">Submit</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </tab>

      </tabset>
    </div>
  </div>
</div>