{{ content() }}
<script type="text/ng-template" id="homeimg.html">
  <div ng-include="'/be/tpl/homeimg.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Home</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">
          Home page images
        </div>

        <div class="panel-body">
          <div class="row">
            <div class="col-sm-4">
              <div class="toppageimg">
                <div style="background-image: url('{[{ amazonlink }]}/uploads/images/{[{ home.img1 }]}'); height:295px;" class="bg-image"></div>
                <h4 class="text-center">{[{ home.title1 }]}</h4>
                <div class="groupbtn">
                  <button class="btn btn-primary" ng-click="updateimg(1)"><i class="fa fa-cog"></i></button>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="toppageimg">
                <div style="background-image: url('{[{ amazonlink }]}/uploads/images/{[{ home.img2 }]}'); height:295px;" class="bg-image"></div>
                <h4 class="text-center">{[{ home.title2 }]}</h4>
                <div class="groupbtn">
                  <button class="btn btn-primary" ng-click="updateimg(2)"><i class="fa fa-cog"></i></button>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="toppageimg">
                <div style="background-image: url('{[{ amazonlink }]}/uploads/images/{[{ home.img3 }]}'); height:295px;" class="bg-image"></div>
                <h4 class="text-center">{[{ home.title3 }]}</h4>
                <div class="groupbtn">
                  <button class="btn btn-primary" ng-click="updateimg(3)"><i class="fa fa-cog"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>