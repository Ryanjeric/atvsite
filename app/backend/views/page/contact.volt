<script type="text/ng-template" id="reservationimglist.html">
  <div ng-include="'/be/tpl/reservationimglist.html'"></div>
</script>
<script type="text/ng-template" id="notification.html">
  <div ng-include="'/be/tpl/notification.html'"></div>
</script>

<div id="top" class="bg-gray b-b wrapper-md">
  <h1 class="m-n font-thin h3 text-light">Contact</h1>
</div>

<!-- <form editable-form class="form-validation form-horizontal  ng-invalid ng-invalid-required ng-scope ng-valid-date ng-dirty ng-valid-parse ng-valid-maxlength ng-valid-date-disabled" name="formnews" id="formnews"> -->
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <form class="form-validation form-horizontal ng-invalid ng-invalid-required ng-scope ng-valid-date ng-dirty ng-valid-parse ng-valid-maxlength ng-valid-date-disabled" ng-submit="saveContactInfo(contact)" name="formnews" id="formnews">
        <!-- ngRepeat: alert in alerts -->
        <div class="row">
          <div class="col-sm-12">
            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index, 'main')">{[{ alert.msg }]}</alert>
          </div>
          
          <div class="col-sm-6">
            <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Contact Information
              </div>

              <div class="panel-body">
                <div class="form-group">
                  <div class="col-sm-2 control-label">
                    <label for="title">Email</label>
                  </div>
                  <div class="col-sm-10">
                    <input type="email" id="email" name="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="contact.email"required>
                  </div>
                </div>

                <div class="line line-dashed b-b line-lg"></div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">
                    Address
                  </label>
                  <div class="col-sm-10">
                    <input type="text" id="address" name="address" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="contact.address"required>
                  </div>
                </div>

                <div class="line line-dashed b-b line-lg"></div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">
                    Contact No.
                  </label>
                  <div class="col-sm-10">
                    <input type="text" id="contactno" name="contactno" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="contact.contactno"required>
                  </div>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Map
              </div>

              <div class="panel-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">
                    Longitude
                  </label>
                  <div class="col-sm-10">
                    <input type="text" id="longitude" name="longitude" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="contact.longitude"required>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">
                    Latitude
                  </label>
                  <div class="col-sm-10">
                    <input type="text" id="latitude" name="latitude" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="contact.latitude"required>
                  </div>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Social Media
              </div>

              <div class="panel-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">
                    Facebook
                  </label>
                  <div class="col-sm-10">
                    <input type="text" id="facebook" name="facebook" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="contact.facebook"required>
                  </div>
                </div>

                <div class="line line-dashed b-b line-lg"></div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">
                    Google Plus
                  </label>
                  <div class="col-sm-10">
                    <input type="text" id="gplus" name="gplus" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="contact.gplus"required>
                  </div>
                </div>

                <div class="line line-dashed b-b line-lg"></div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">
                    Youtube
                  </label>
                  <div class="col-sm-10">
                    <input type="text" id="youtube" name="youtube" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="contact.youtube"required>
                  </div>
                </div>
              </div>
            </div>
          </div>
        
          <div class="col-sm-6">
            <div class="panel panel-default">
              <div class="panel-heading font-bold">
                Contact us page settings
              </div>

              <div class="panel-body">
                <div class="form-group">
                  <div class="col-sm-12">
                    <div ng-if="amazonpath" style="background-image: url('{[{ amazonlink }]}/uploads/reservation/{[{ amazonpath }]}');height: 270px;margin:20px;" class="bg-image"></div>
                    <button type="button" class="btn btn-success pull-right" style="margin-right:20px;" ng-click="showimageList('lg')">Select Image</button>
                  </div>
                </div>

                <div class="line line-dashed b-b line-lg"></div>

                <div class="form-group">
                  <div class="col-sm-3 control-label">
                    <label for="title">Reservation text</label>
                  </div>
                  <div class="col-sm-12">
                    <textarea name="reservation" id="reservation" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" style="min-height: 134px;" ng-model="contact.reservationtext" required></textarea>
                  </div>
                </div>              
              </div>
            </div>
          </div>

        </div>
        <div class="row">
          <div class="panel-body">
              <footer class="panel-footer text-right bg-light lter">
                <a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
                <button type="submit" class="btn btn-success pull-right" ng-disabled="formnews.$invalid">Submit</button>
              </footer>
          </div>
        </div>
          
      </form>
    </div>
  </fieldset>
<!-- </form> -->