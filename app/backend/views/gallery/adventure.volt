{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="notification.html">
	<div ng-include="'/be/tpl/notification.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">Exiting Adventures Await</h1>
	<a id="top"></a>
</div>
<div class="wrapper-md">
  <form ng-submit="Save(msg)">
	<div class="col-sm-12">
		<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
	</div>
	<div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">
        	Exiting Adventures Await
       	</div>
       	<div class="panel-body">
			<div class="row">
				<div class="panel-body">
					<div class="col-sm-12">
						<div class="form-group hiddenoverflow">
			                <div class="col-sm-2 control-label">
			                	<label for="title">Title</label>
			                </div>
			                <div class="col-sm-10">
			                	<input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-untouched" ng-model="msg.title" required>
			                </div>
			            </div>
						
						<div class="line line-dashed b-b line-lg"></div>
						
						<div class="form-group hiddenoverflow">
			                <div class="col-sm-2 control-label">
			                	<label for="Description">Description</label>
			                </div>
			                <div class="col-sm-12">
								<textarea class="ck-editor" ng-model="msg.content" required></textarea>
			                </div>
			            </div>
					</div>
				</div>
			</div>
		</div>
	  </div>
	</div>

	<div class="col-sm-12">
		<footer class="panel-footer  bg-light lter">
				<div class="pull-right">
					<a ui-sref="dashboard" class="btn btn-default"> Cancel </a>
					<button type="submit" class="btn btn-success" scroll-to="Scrollup">Save</button>
				</div>
				<div style="clear:both;"></div>
			</footer>
		</div>
	</div>
  
  </form>
</div>