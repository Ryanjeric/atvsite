{{ content() }}
<script type="text/ng-template" id="notification.html">
  <div ng-include="'/be/tpl/notification.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Gallery</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">
          <div class="btn-group" ng-init="mediaGallery='images'">
            <label class="btn btn-success ng-valid ng-dirty ng-untouched ng-binding" ng-model="mediaGallery" btn-radio="'images'"><icon class="fa fa-file-image-o"> </icon> Images ({[{ countimg }]})</label>
            <label class="btn btn-success ng-valid ng-dirty ng-untouched ng-binding active ng-valid-parse" ng-model="mediaGallery" btn-radio="'videos'"><icon class="fa fa-file-movie-o"> </icon> Videos ({[{ countvid }]})</label>
          </div>  
        </div>

        <div class="panel-body">
          <div class="loader" ng-show="imageloader">
            <div class="loadercontainer">
              <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
              </div>
              Uploading your images please wait...
            </div>
          </div>
          <div ng-show="imagecontent && mediaGallery=='images'">
            <div class="col-sml-12">
              <div class="dragdropcenter">
                <div ngf-drop ngf-select class="drop-box" 
                ngf-drag-over-class="dragover" ngf-multiple="false" ngf-allow-dir="true"
                accept="image/*" ng-model="file" ngf-change="upload($files)">Drop images here or click to upload</div>

              </div>
            </div>

            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
            <div style=" height:500px; overflow-y:scroll;">
              <div ng-show="noimage"><center>IMAGE GALLERY IS EMPTY</div>
              <div class="line line-dashed b-b line-lg"></div>
              <div class="col-sm-3" ng-repeat="data in imagelist" ng-show="imggallery" style="margin-bottom:15px;">

                <button class=" btn btn-rounded btn btn-icon btn-danger" ng-click="delete(data.id)" style="margin:10px 5px -50px; 5px"><i class="fa fa-trash-o"></i></button>
                <input id="pathimage" type="hidden" id="title" name="title" class="form-control" ng-init="imgamazonpath=amazonpath+'/uploads/images/'+ data.filename" onClick="this.setSelectionRange(0, this.value.length)" ng-model="imgamazonpath">
                <div type="button" class="imagegallerystyle" style="background-image: url('<?php echo $this->config->application->amazonlink; ?>/uploads/images/{[{data.filename}]}');" ng-click="path(imgamazonpath)">
                </div>
              </div>
            </div>
          </div>
          <div ng-show="mediaGallery=='videos'">
          <div class="col-sml-12" style="margin: 20px;" >
            <div class="input-group col-sm-5 align-center">
              <input class="form-control ng-pristine ng-untouched ng-valid" type="text" ng-model="video" placeholder="Paste embed youtube here and click save...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button" ng-click="savevid(video); video = ''">Save</button> <br><br>
              </span>
            </div>
            <span ng-show="invalidvideo" class="ng-hide">Invalid youtube embed link.</span>
            <div class="line line-dashed b-b line-lg"></div>
            <alert ng-repeat="alert in alertz" type="{[{alert.type }]}" close="closeAlertz($index)">{[{ alert.msg }]}</alert>
            <div ng-show="novid"><center>VIDEO GALLERY IS EMPTY</div>
            <div class="col-sm-3 ng-scope" ng-repeat="data in videolist" ng-show="mediaGallery=='videos' && !novid" style="margin-bottom:15px;">
              <div  class="col-sm-3 imagegallerystyle mediagallerythumb" style="background-image: url('{[{ data.videourl }]}')" ng-click="set(data)">
                <i class="fa fa-check-circle pull-left check-icon" ng-show="data.id == currentSelected.id"></i>
                <div class="youtube-play"><img src="/img/youtubeplay.png"/></div>
                <div class="galleryactionbutton">
                  <button class="btn btn-default btn-xs pull-right" ng-click="deletevideo(data.id, $event); currentDeleting=data.id"><icon class="fa fa-times-circle"></icon></button>
                  <a href="https://www.youtube.com/watch?v={[{ data.youtubeid }]}" ng-click="$event.stopPropagation()" target="_blank" class="btn btn-default btn-xs pull-right"><icon class="fa fa-external-link"></icon></a>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>






          </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>