{{ content() }}
<div id="Scrollup"></div>
<script type="text/ng-template" id="notification.html">
	<div ng-include="'/be/tpl/notification.html'"></div>
</script>
<script type="text/ng-template" id="viewreservationCTRL.html">
  <div ng-include="'/be/tpl/viewreservationCTRL.html'"></div>
</script>
<script type="text/ng-template" id="viewreservation1CTRL.html">
  <div ng-include="'/be/tpl/viewreservation1CTRL.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md invi">
	<h1 class="m-n font-thin h3">Reservations</h1>
</div>
<style>
	@media print {
		.invi {
			visibility: hidden;
		}
		.printhide{
			display: none;
		}
		.printpreview{
			font-size: 10px;
		}
		.modal{
			position: absolute;
			left: 0;
			top: 0;
			margin: 0;
			padding: 0;
			min-height:550px
		}
	}
</style>
<div class="wrapper-md invi">
		<div class="row wrapper">
<!-- 			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						Reservations List
					</div>
					<div class="panel-body">
						<div class="col-sm-12" ng-show="!table">
							<div class="col-sm-6">
								<form name="rEquired">
									<div class="input-group">

										<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)">
										<span class="input-group-btn">
											<button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
										</span>
									</div>
								</form>
							</div>
						</div>
						<div class="col-sm-12">
							<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
						</div>
							<div class="table-responsive">
								<table class="table table-striped b-t b-light">
									<thead>
										<tr>
											<th>Invoice Number</th>
											<th>Cutomer Name</th>
											<th>Schedule Date</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="list in list">
											<td>{[{ list.invoiceno }]}</td>
											<td>{[{ list.fname }]} {[{ list.lname }]}</td>
											<td>{[{ list.reservationdate | date:'EEEE, MMMM d, y'}]}</td>
											<td>
												<button ng-click="viewreservation(list.invoiceno)" class="btn m-b-xs btn-xs btn-info btn-addon">VIEW</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<footer class="panel-footer">
								<div class="row">
									<div class="panel-body">
										<footer class="panel-footer text-center bg-light lter">
											<pagination total-items="TotalItems" ng-model="CurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(CurrentPage)"></pagination>
										</footer>
									</div>
								</div>
							</footer>
					</div>
				</div>
			</div>

			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						Calendar View
					</div>
					<div class="panel-body">
						<div ng-show="calendar" class="pos-rlt">
							<div class="col-sm-12"><div class="pull-right">
								<button type="button" class="btn btn-sm btn-default" ng-click="today('calendar1')">today</button>
								<br><br></div></div>
								<div class="col-sm-12">
									<div class="pos-rlt">
										<div class="fc-overlay">
											<div class="panel bg-white b-a pos-rlt">
												<span class="arrow left pull-up"></span>
												<div class="h4 font-thin m-b-sm ng-binding">
													{[{ event.title }]}
												</div>
												<div class="line b-b b-light">{[{ event.atv }]}</div>
												<div class="ng-binding"><i class="icon-calendar text-muted m-r-xs"></i>{[{ event.datereserve | date:'EEEE, MMMM d, y' }]}</div>
												<div class="ng-binding"><i class="icon-envelope  text-muted m-r-xs"></i>{[{ event.email }]}</div>
												<div class="ng-binding"><i class="icon-call-out  text-muted m-r-xs"></i>{[{ event.phonenumber }]}</div>
												<div><i class="icon-wallet text-muted m-r-xs"></i><a href="" ng-click="openinvoice(event.invoiceno)" class="ng-binding">{[{ event.invoiceno }]}</a></div>
												<div class="m-t-sm ng-binding">Date Created: {[{ event.datecreated }]}</div>
											</div>
										</div>
									</div>
									<div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
								</div>
						</div>
					</div>
				</div>
			</div> -->

						<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Reservations List
					</div>
					<div class="panel-body">
						<div class="col-sm-12" ng-show="!table">
							<div class="col-sm-6">
								<form name="rEquired">
									<div class="input-group">

										<input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext" required ng-change="search(searchtext,searchcenter)">
										<span class="input-group-btn">
											<button type="button" class="btn btn-sm btn-bg btn-default" data-toggle="tooltip" data-placement="bottom" data-title="Refresh" data-original-title="" title="RESET" ng-click="resetsearch()"><i class="fa fa-refresh"></i></button>
										</span>
									</div>
								</form>
							</div>
						</div>
						<div class="col-sm-12">
							<alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
						</div>
							<div class="table-responsive">
								<!--  <input type="hidden" ng-init='userdata = {{ data }}'> -->
								<table class="table table-striped b-t b-light">
									<thead>
										<tr>
											<th>Invoice Number</th>
											<th>Customer Name</th>
											<th>Schedule Date</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="list in list1">
											<td>{[{ list.invoiceno }]}</td>
											<td>{[{ list.fname }]} {[{ list.lname }]}</td>
											<td>{[{ list.rentaldate | date:'EEEE, MMMM d, y'}]}</td>
											<td>
												<div class="btn-group" dropdown="" is-open="status.isopen">
													<button type="button" class="btn btn-primary dropdown-toggle" ng-disabled="disabled" aria-haspopup="true" aria-expanded="false">
														Change Status <span class="caret"></span>
													</button>
													<ul class="dropdown-menu" role="menu">
														<li ng-if="list.status!=0"><a ng-click="status0(list.id)" href="">New</a></li>
														<li ng-if="list.status!=1"><a ng-click="status1(list.id)" href="">Reserved</a></li>
														<li ng-if="list.status!=2"><a ng-click="status2(list.id)" href="">Done</a></li>
														<li ng-if="list.status!=3"><a ng-click="status3(list.id)" href="">Void</a></li>
														<li ng-if="list.status!=4"><a ng-click="status4(list.id)" href="">Unattended</a></li>
													</ul>
												</div>

												<span ng-if="list.status==0" class="bg-warning btn btn-xs" >New</span>
												<span ng-if="list.status==1" class="bg-primary btn btn-xs" >Reserved</span>
												<span ng-if="list.status==2" class="bg-success btn btn-xs" >Done</span>
												<span ng-if="list.status==3" class="bg-danger btn btn-xs" >Void</span>
												<span ng-if="list.status==4" class="bg-warning btn btn-xs" >Unattended</span>
											</td>

											<td>
												<button ng-click="view(list.id)" class="btn m-b-xs btn-xs btn-info btn-addon">VIEW</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<footer class="panel-footer">
								<div class="row">
									<div class="panel-body">
										<footer class="panel-footer text-center bg-light lter">
											<pagination total-items="TotalItems" ng-model="CurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(CurrentPage)"></pagination>
										</footer>
									</div>
								</div>
							</footer>
					</div>
				</div>
			</div>


			<toaster-container toaster-options="{'time-out': 3000, 'close-button':true, 'animation-class': 'toast-bottom-right', 'position-class': 'toast-bottom-right'}"></toaster-container>


		</div>
</div>
