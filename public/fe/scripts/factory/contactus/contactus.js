app.factory('Contact', function($http, $q, appConfig){
	return {
		data: {},
		send: function(send,callback)
		{
			$http({
				url: appConfig.ApiURL + "/fe/contact/send",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data:$.param(send)

			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
	}
})