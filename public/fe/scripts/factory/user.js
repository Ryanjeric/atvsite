app.factory('User', function($http, store, jwtHelper, appConfig){
    return {
        usernameExists: function(data, callback) {

            $http({
                url: appConfig.ResourceUrl + '/user/usernameexist/' + data.username,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
            return;
        },
        emailExists: function(data, callback) {
            $http({
                url: appConfig.ResourceUrl + '/user/emailexist/' + data.email,
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
            return;
        },
        register:function(user, callback){
            $http({
                url: appConfig.ResourceUrl+'/user/register',
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(user)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
            return;
        },
        login: function(log, callback){
            $http({
                url: appConfig.ResourceUrl+'/user/login',
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(log)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback({'error':true});
            });
            return;
        }
    }
});