app.factory('News', function($http, $q, appConfig){
	return {
		data: {},
		send: function(send,callback)
		{
			$http({
				url: appConfig.ApiURL+"/fe/contact/send",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data:$.param(send)

			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		list: function(catid,archieve,tags,callback)
		{
			$http({
				url: appConfig.ApiURL+"/blog/listall/"+catid+"/"+archieve+"/"+tags,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		listcat: function(callback)
		{
			$http({
				url: appConfig.ApiURL+"/news/loadcategories/",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		listarchieve: function(callback)
		{
			$http({
				url: appConfig.ApiURL+"/blog/listarchieve",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		listtags: function(callback)
		{
			$http({
				url: appConfig.ApiURL+"/news/loadtags",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		homeblogs: function(callback) {
			$http({
				url: appConfig.ApiURL+"/home/blogs",
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		authornewslist: function(authorid, items, callback) {
			$http({
				url: appConfig.ApiURL+"/fe/blog/author/"+authorid+"/"+items,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
		preview: function(assets, callback) {
			$http({
				url: appConfig.ApiURL+"/fe/blog/preview",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(assets)
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		}
	}
})
