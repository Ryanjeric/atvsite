app.factory('Reservation', function($http, $q, appConfig){
	return {
		data: {},
		send: function(send,callback)
		{
			$http({
				url: appConfig.ApiURL+"/fe/contact/send",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data:$.param(send)

			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});
		},
        listatv: function(callback){
            $http({
                url: appConfig.ApiURL + "/atv/felist",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data) {
                callback(data);
            });
        },
		filteratvlist: function(rentalhoursid, callback){
				$http({
						url: appConfig.ApiURL + '/atv/fefilterlist/' + rentalhoursid,
						method: "GET",
						headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data) {
						callback(data);
				});
		},
        atvinfo: function(id,callback){
        	$http({
        		url: appConfig.ApiURL + "/atv/reservation/" + id,
        		method: "GET",
        		headers:{
        			'Content-Type': 'application/x-www-form-urlencoded'
        		}
        	}).success(function(data) {
                callback(data);

            })
        },
        trails: function(callback){
        	$http({
        		url: appConfig.ApiURL + "/trails/felist",
        		method: "GET",
        		headers:{
        			'Content-Type': 'application/x-www-form-urlencoded'
        		}
        	}).success(function(data) {
                callback(data);

            })
        },
        rentalselected: function(id,callback){
            $http({
                url: appConfig.ApiURL + "/rental/edit/" + id,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
        trailsselected : function(id,callback){
            $http({
                url: appConfig.ApiURL + "/trails/info/" + id,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
        getprice :  function(atvid,rentalid,callback){
            $http({
                url: appConfig.ApiURL + "/atv/getprice/" + atvid + "/" + rentalid,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
        savereservation: function(asset,callback)
        {
            $http({
                url: appConfig.ApiURL+"/reservation/savedata",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data:$.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        getevent :  function(atvid,callback){
            $http({
                url: appConfig.ApiURL + "/reservation/getevent/" + atvid,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            });
        },
        listrentals :  function(callback){
            $http({
                // url: appConfig.ApiURL + '/atv/listrentalhours',
                url: appConfig.ApiURL + '/get/atv/sessions',
                method: 'GET',
                headers:{ 'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data) {
                callback(data);
            });
        },
         savereservation1: function(asset,callback)
        {
            $http({
                url: appConfig.ApiURL + '/reservation/savedata1',
                method: 'POST',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data:$.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        viewatv :  function(atvid,callback){
            $http({
                url: appConfig.ApiURL + "/atv/reservation/" + atvid,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            })
        },


	}
})
