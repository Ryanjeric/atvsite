'use strict';
var init_module = [
    'ngAnimate',
    'ngCookies',
    'angular-storage',
    'ngResource',
    'uuid',
    'ngStorage',
    // 'angularFileUpload',
    'ui.router',
    'ui.bootstrap',
    'ui.bootstrap.datepicker',
    'uiGmapgoogle-maps',
    'vcRecaptcha',
    'angular-jwt',
    'videosharing-embed',
    'ui.calendar',
    'toaster',
    'loading',
    'ngMessages'
];



var app = angular.module('atvapp',init_module)
    .run([ 'loadingProgress','$rootScope', '$state', '$stateParams' , 'store', 'jwtHelper' , '$http',
        function (loadingProgress,$rootScope,   $state,   $stateParams, store, jwtHelper, $http) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            // Init loadingProgress service
            loadingProgress.init();

            // Reset loading queue on route change
            $rootScope.$on('$routeChangeSuccess', loadingProgress.reset);
            $rootScope.$on('$routeUpdate', loadingProgress.reset);
        }
    ])
    .config(
    [          '$logProvider', '$httpProvider', '$locationProvider','$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$interpolateProvider','$parseProvider','$resourceProvider', 'jwtInterceptorProvider', 'uiGmapGoogleMapApiProvider',
        function ($logProvider, $httpProvider, $locationProvider, $stateProvider,   $urlRouterProvider,   $controllerProvider,   $compileProvider,   $filterProvider,   $provide, $interpolateProvider, $parseProvider, $resourceProvider, jwtInterceptorProvider, uiGmapGoogleMapApiProvider) {
            $interpolateProvider.startSymbol('{[{');
            $interpolateProvider.endSymbol('}]}');
            // $resourceProvider.defaults.stripTrailingSlashes = false;
            // $httpProvider.interceptors.push('loadingInterceptor');
            // $urlRouterProvider.otherwise('/');
            //
            //
            // $httpProvider.interceptors.push(['$q', '$location', 'store', function($q, $location, store) {
            //     return {
            //         'request': function (config) {
            //             config.headers = config.headers || {};
            //             if (store.get('jwt')) {
            //                 $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + store.get('jwt');
            //             }
            //             return config;
            //         },
            //         'responseError': function(response) {
            //             if(response.status === 401 || response.status === 403) {
            //                 $location.path('/user/log');
            //             }
            //             return $q.reject(response);
            //         }
            //     };
            // }]);
            //
            // $stateProvider
                // .state('success', {
                //     url: "/success",
                //     templateUrl: "/registration/success"
                // })

                // .state('blogview', {
                //     url: '/blog/:newsslugs',
                //     controller: 'blogviewCtrl',
                //     templateUrl: '/atvadmin/blog/view',
                //     resolve: {
                //         deps: ['uiLoad',
                //         function( uiLoad ){
                //             return uiLoad.load( [
                //                 '/fe/js/scripts/controllers/gallery/gallery.js',
                //                 '/fe/js/scripts/factory/gallery/gallery.js'
                //                 ]);
                //         }]
                //     }
                // })

            uiGmapGoogleMapApiProvider.configure({
                key: 'AIzaSyAkgfUylY29OvxzKm5uh8j2NHc9hhBMjW0',
                v: '3.17',
                libraries: 'weather,geometry,visualization'
            });

        }
    ]
);

angular.module('loading', [])

  .factory('loadingInterceptor', function($q, loadingProgress) {
    var pendingRequests = [];

    function onRequest(request) {
      loadingProgress.queue(request);

      console.log('Pending request: ' + request.url);

      return request;
    }

    function onResponse(response) {
      loadingProgress.dequeue(response.config);

      console.log('Completed request: ' + response.config.url);

      return response;
    }

    function onError(response) {
      loadingProgress.dequeue(response.config);

      console.log('Failed request: ' + response.config.url);

      return $q.reject(response);
    }

    return {
      request: onRequest,
      response: onResponse,
      requestError: onError,
      responseError: onError
    };
  })

  .factory('loadingProgress', function($rootScope, $timeout) {
    return {
      init: function() {
        this.pendingRequests = [];
      },

      reset: function() {
        this.pendingRequests.length = 0;
        this.loaded = false;
      },

      queue: function(request) {
        // Add to queue if it is a GET request
        if (request.method === 'GET') {
          this.pendingRequests.push(request.url);
          this.loaded = false;
        }
      },

      dequeue: function(request) {
        var index = this.pendingRequests.indexOf(request.url);

        // Remove from queue
        if (index !== -1) {
          this.pendingRequests.splice(index, 1);
        }

        // Wait for the next digest cycle
        $timeout(function() {
          // If there are no pending requests, notify child scopes
          if (this.pendingRequests.length === 0 && !this.loaded) {
            this.notify('pageReady');
            this.loaded = true;
          }
        }.bind(this));
      },

      notify: function() {
        $rootScope.$broadcast.apply($rootScope, arguments);
      }
    };
  });
