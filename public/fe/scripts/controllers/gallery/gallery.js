app.controller('galCtrl',  function(appConfig, $scope, $http, $state, News,$timeout,$window, store){
     
    $scope.limit = 8;


    var loadthis = function(){
        $http({
            url: appConfig.ApiURL+"/gallery/list",
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function (data, status, headers, config) {
            $scope.loading = false;
            if($scope.limit>=data.length){
              $scope.hideloadmore = true;
            }else{
              $scope.hideloadmore = false;
            }
            $scope.imgs = data;
        }).error(function (data, status, headers, config) {
        });
    }

    loadthis();

    var limitStep = 8;

    $scope.showmoregal = function() {
      $scope.loading = true;
      loadthis();
      $scope.limit += limitStep;
    };

});
app.directive('lightgallery', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      if (scope.$last) {
        element.parent().lightGallery();
        setTimeout(function () {
            if(element.parent().lightGallery()){
                element.parent().data('lightGallery').destroy(true); // destroy gallery
            }
            element.parent().lightGallery(); //re-initiate gallery
        }, 100);

      }
    }
  };
});
