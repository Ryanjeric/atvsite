app.controller('ContactCtrl',  function($scope, $http, $state, Contact, $timeout, AtvService){
	$scope.notify = false;
	$scope.err =false;
	$scope.dismember = false;
		
	var captcha = document.getElementById("txtCaptcha").value;

	$scope.send = function(senddata, form){

		if(form.$invalid) {
			AtvService.swal("Warning", "inc");
			return AtvService.setTouched(form.$error);
		}

		if(captcha == $scope.str2) {

			AtvService.swalLoaderOnConfirm('Send Email', function(){

				Contact.send(senddata, function(data) {

					if(data.hasOwnProperty('err')) {
						return AtvService.swal('Error', data.err);
					} else {
						AtvService.swal('Success', 'Message Sent');
						setTimeout("location.reload(true);",1000);
					}

		           	// $scope.jb = "";
		           	// $scope.message.$setPristine(true);
		           	// $scope.notify = true;
		           	// $scope.err =false;
		           	// $scope.msg = "MESSAGE SENT!";
		           	
				});

			});
			
		} else {

			AtvService.swal('Warning', 'Captcha is incorrect');

		}

	};

});
