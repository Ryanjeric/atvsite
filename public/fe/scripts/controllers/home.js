'use strict';

/* Controllers */

app.controller('homeCtrl', function(appConfig, $scope, $sce, News){
    $scope.s3 = appConfig.amazonlink+'/uploads/news/';

    News.homeblogs(function(data){
      angular.forEach(data.blogs,function(value,key){
        if(data.blogs[key].featuredtype == 'vid') {
          data.blogs[key].featured = $sce.trustAsHtml(data.blogs[key].featured);
        }
      })
      $scope.blogs = data.blogs;
    })


})
