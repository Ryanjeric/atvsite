app.controller('ReserveCtrl',  function($scope, $http, $state,$sce, Reservation,$timeout,appConfig,$modal, toaster,uiCalendarConfig){
	console.log("--RESERVATION--");
	var s3 = appConfig.amazonlink+'/uploads/images/';
	var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    console.log(d);
    console.log(moment().month());
    console.log(y);
    $scope.wew = false;

    $scope.picture = '/img/no-image.jpg';
    Reservation.listatv(function(data){
    	$scope.listatv = data.data;
    });
    $scope.reservelist = [];
    $scope.reserved = [];

    $scope.stepone = true;
    $scope.steptwo = false;
    $scope.steptri = false;

    $scope.showreservelist = false;
    $scope.atvtxt = '';
    $scope.rentaltxt = '';

	$scope.selduration = function(rentalhoursid) {
		Reservation.filteratvlist(rentalhoursid, function(data) {
			$scope.listatv = data.data;
			console.log(data);
		});
	};

    $scope.atvselected = function(atv){
    	$scope.rentaltxt = '';
    	$scope.rentalhr = null;
    	$scope.wew = true;

    	Reservation.atvinfo(atv,function(data){
    		$scope.atvtxt = data.data.title;
    		$scope.subtitle = data.data.subtitle;
    		$scope.features = data.data.features;
    		$scope.desc = $sce.trustAsHtml(data.data.desc);
    		$scope.picture = s3 + data.data.picture;
    		$scope.listrental = data.data2;
    		uiCalendarConfig.calendars['calendar1'].fullCalendar( 'removeEvents');
    		Reservation.getevent(atv,function(data1){
    				console.log(data1);
    				if(data1!=0){
	    				for(var i = 0; i < data1.length; i++){
	    					$scope.events.push({title: 'RESERVED',start: moment(data1[i]['reservationdate'])});
	    					$scope.reserved.push({atvid: data1[i]['atvid'],datereserve: data1[i]['reservationdate']});
	    				}
	    			}
   					$timeout(function() {
    				$scope.eventSources = [$scope.events];},100);
    		});
    	});
    	console.log($scope.events);
    };

    $scope.rentalselected = function(id){
    	console.log(id);
    	Reservation.rentalselected(id,function(data){
    		$scope.rentaltxt = data.data.session + "-"+ data.data.start+" to "+ data.data.end +"(" + data.data.duration + "HR/S)";
    	});
    	Reservation.getprice($scope.atv,id,function(data){
    		$scope.presyo = data.price;
    		console.log(data);
    	});
    };

    $scope.savereservation = function(res){
    	var asset = {
    		customer : res,
    		reslist : $scope.reservelist
    	};
    	Reservation.savereservation(asset,function(data){
    		$scope.steptri = true;
	    	$scope.steptwo = false;
	    	console.log(asset);
	    	toaster.pop('success', "Reservation successfully added to our Waiting list wait for a phone call to confirm your reservation Thank you!.");
    	});
    };

    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.reservelist.length; i++){
            var service = $scope.reservelist[i];
            total += parseFloat(service.price);
        }
        return total;
    };

    $scope.deleteService = function(index, startdatetime){
    	console.log(index);
        $scope.reservelist.splice(index, 1);
        console.log($scope.events);
        $scope.reserved.splice(index, 1);
        $scope.events.splice(index, 1);
        toaster.pop('success', "The schedule has been Deleted to your reservation list.");
        $timeout(function() {
    				$scope.eventSources = [$scope.events];},100);
    };

    /* event source that contains custom events on the scope */

    /* event source that calls a function on every view switch */

    function IsDateHasEvent(date) {
    	var allEvents = [];
    	allEvents = uiCalendarConfig.calendars['calendar1'].fullCalendar('clientEvents');
    	var event = $.grep(allEvents, function (v) {
    		return +v.start === +date;
    	});
    	return event.length < 0;
    }

    function checkIfArrayIsUnique(obj,atvtext,dtclk)
	{
    	for (var i = 0; i < Object.keys(obj).length; i++)
    	{
       		if(obj[i]['atv'] == atvtext && obj[i]['datereserve'] == dtclk){
       			return true;
       		}
    	}
    	return false;   // this means not unique
	}
	function checkifreserved(obj,atvid,dtclk){
		for (var i = 0; i < Object.keys(obj).length; i++)
    	{
       		if(obj[i]['datereserve'] == dtclk && obj[i]['atvid'] == atvid){
       			return true;
       		}
    	}
    	return false;
	}


    $scope.renderCalender = function(calendar) {
      if(uiCalendarConfig.calendars[calendar]){
        uiCalendarConfig.calendars[calendar].fullCalendar('render');
      }
    };
    $scope.dayClick = function(date, jsEvent, view) {
    	console.log(date);
    	if(IsDateHasEvent(date)){
    		toaster.pop('warning', "THIS DATE IS ALREADY IN YOUR RESERVATION LIST CHANGE ATV INSTEAD.");
    	}else{

    		var dateclicked = date.format('YYYY-MM-DD');
    		console.log($scope.rentalhr);
    		if($scope.rentaltxt!=''){
    			if(date.unix() >= moment().unix()){

    				$(".fc-state-highlight").removeClass("fc-state-highlight");
    				$("td[data-date="+date.format('YYYY-MM-DD')+"]").addClass("fc-state-highlight");

    				var modalInstance = $modal.open({
    					templateUrl: 'reserve.html',
    					controller: function($scope, $modalInstance) {
    						$scope.message = 'Are you sure you want to add this to your reservation list?';

    						$scope.cancel = function() {
    							$modalInstance.dismiss();
    						}
    						$scope.ok = function() {
    							$scope.dateclicked  = dateclicked;
    							if(checkIfArrayIsUnique($scope.reservelist,$scope.atvtxt,dateclicked) == true){
    								toaster.pop('warning', "This date is already in your reservation list, Change ATV Instead.");
    								$modalInstance.dismiss();
    							}
    							else if(checkifreserved($scope.reserved,$scope.atv,dateclicked) == true){
    								toaster.pop('warning', "The schedule you are trying to add is already reserved to another client..");
    								$modalInstance.dismiss();
    							}
    							else{
    								$scope.reservelist.unshift(
    									{
    										atv:      $scope.atvtxt,
    										rentalhr: $scope.rentaltxt,
    										price:    $scope.presyo,
    										datereserve:dateclicked,
    										atvid:    $scope.atv,
    										rentalid: $scope.rentalhr,
    									});
    								$scope.getTotal();
    								toaster.pop('success', "The schedule has been added to your reservation list.");
    								$modalInstance.close($scope.reservelist);
    							}
    							console.log($scope.events);
    						}
    					},
    					scope: $scope,
    					resolve: {
    						dateclicked: function(){
    							return dateclicked;
    						}
    					}
    				}).result.then(function(data){
    					$scope.showreservelist = true;
    					var n = {title: 'RESERVED',start: date,textColor: '#000',backgroundColor: '#fff',};
    					$scope.events.unshift(n);
    					console.log(data);
    				});
    			}else{
    				toaster.pop('warning', "These date have already passed.");
    			}
    		}
    		else if($scope.atvtxt == ''){
    			toaster.pop('warning', "Select ATV 1st.");
    		}
    		else if($scope.rentaltxt ==''){
    			toaster.pop('warning', "Please Select a Rental Hour.");
    		}
    	}
    }


    /* config object */
    $scope.uiConfig = {
      calendar:{
        height: 440,
        editable: false,
        header:{
          left: 'title',
          center: '',
          right: 'today prev,next'
        },
        dayClick: $scope.dayClick,
        timeFormat: ' ',
        events: $scope.eventSources,
      }
    };

    $scope.events = [];
    /* event sources array*/

    $scope.eventSources = [$scope.events ];


    ////////////////////////////////////////////...
    $scope.dismember = false;

    Reservation.listrentals(function(data){
        $scope.listrenthr = data;
        console.log(data);
    });

    $scope.view = function(atvid){
        var modalInstance = $modal.open({
            templateUrl: 'atv.html',
            controller: viewatvCTRL,
            size: 'lg',
            resolve: {
                atvid: function () {
                    return atvid;
                }
            }
        });
    };

     var viewatvCTRL = function($scope, $modalInstance, atvid, $sce) {
       var loadlist = function(){
        Reservation.viewatv(atvid,function(data){
            $scope.s3 = appConfig.amazonlink+'/uploads/images/';
            $scope.atv = data.data;
            $scope.desc = $sce.trustAsHtml(data.data.desc);
            $scope.rentalprice = data.data2;
        });
       };
       loadlist();
       $scope.cancel = function() {
            $modalInstance.dismiss('cancel');

        };
    };



    function onSubmit()
    {
      var fields = $("input[name='list']").serializeArray();
      if (fields.length == 0)
      {
        toaster.pop('danger', "Please Select ATV.");
        // cancel submit
        return false;
        }
        else
        {
            $scope.savereservationz = function(res){
                $scope.dismember = true;
                var asset = {
                    customer : res,
                }
                console.log(res);
                Reservation.savereservation1(asset,function(data){
                    console.log(data);
                    $scope.steptri = true;
                    $scope.stepone = false;
                    toaster.pop('success', "Reservation successfully added to our Waiting list wait for a phone call to confirm your reservation Thank you!.");
                })
            }
        }
    }

    $('#wewForm').submit(onSubmit)

});
