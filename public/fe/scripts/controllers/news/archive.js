app.controller('archiveCtrl',  function(appConfig, $scope, $http, $location, $state, News, $timeout, $window, loadingProgress, store){
  $scope.s3 = appConfig.amazonlink+'/uploads/news/';
  $scope.BaseURL = appConfig.BaseURL;

  var year = store.get("archivepage").year;
  var month = store.get("archivepage").month;
  var archieve = store.get("archivepage").datepublished;
  var catid = undefined;
  var tags = undefined;

  $scope.limit = 5;
  $scope.newstitle = 'Archive : '+ month + ' ' + year;
  $scope.pagetitle = month + '>' + year + '| Rio Verde ATV';
  $scope.month = month;
  $scope.year = year;

  var loadthat=function(){
    News.list(catid,archieve,tags,function(data){
      if(!data.error){
        $scope.meron = true;
        $scope.list = data;
        $scope.loading = false;
        if($scope.limit>=data.length){
          $scope.hideloadmore = true;
        }else{
          $scope.hideloadmore = false;
        }
      }else{
        $scope.meron = false;
        $scope.hideloadmore = true;
      }
    })
  };
  loadthat();

  var limitStep = 5;

  $scope.showmorenews = function() {
      $scope.loading = true;
      loadthat();
      $scope.limit += limitStep;
    };

    $scope.redirectNews = function(slug){
      $window.location.href = '/blog/' + slug;
    }

    News.listcat(function(data){
      $scope.categ = data.categories;
    });

    News.listarchieve(function(data){
      $scope.archieve = data;
    })

    News.listtags(function(data){
      $scope.listtags = data.tags;
    })

    $scope.changecat = function(id,title){
      // catid = id;
      // $scope.newstitle = 'News Category : '+ title;
      // archieve = undefined;
      // tags = undefined;
      // loadthat();
    };

    // $scope.archievedate = function(datepublished,month,year){
    //   archieve = datepublished;
    //   $scope.newstitle = 'Archive : '+ month + ' ' + year;
    //   catid = undefined;
    //   tags = undefined;
    //   loadthat();
    // };

    $scope.tagstags = function(id,title) {
      tags = id;
      $scope.newstitle = 'Tag : '+ title;
      archieve = undefined;
      catid = undefined;
      loadthat();
    };

    /*$scope.$on('pageReady', function() {
      console.log('Completed all http requests');
    });
    $scope.$emit('response', loadingProgress);*/

    $scope.gotocategory = function(catid, catname) {
      var category = {id:catid,name:catname};
      store.set("categorypage", category);
      $window.location.href = "/blog/category/" + catname;
    }

    $scope.gotoarchive = function(datepublished, month, year) {
      var archive = {datepublished:datepublished,month:month, year:year};
      store.set("archivepage", archive);
      $window.location.href = "/blog/archive/" + year + "/" + month;
    }

    $scope.gototag = function(tagid, tagname) {
      var tag = {id:tagid,name:tagname};
      store.set("tagpage", tag);
      $window.location.href = "/blog/tags/" + tagname;
    }

  })
  .filter('dateToISO', function() {
  return function(input) {
    return new Date(input).toISOString();
  };
  });
