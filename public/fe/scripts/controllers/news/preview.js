app.controller('blogpreviewCtrl',  function(appConfig, $state, $stateParams, $scope, $sce, $localStorage, News, $window, store){
  $scope.BaseURL = appConfig.BaseURL;
  $scope.amazonlink = "https://rioverdeatv.s3.amazonaws.com/uploads/news/";
  $scope.authors3 = "https://rioverdeatv.s3.amazonaws.com/uploads/authors/";
  var preview = $localStorage.atvnewspreview;
  $scope.news = {
    title : preview.title,
    summary : preview.summary,
    datepublished : preview.datepublished,
    featuredtype : preview.featuredtype,
    body : $sce.trustAsHtml(preview.body)
  };

  var preview_assets = {
      authorid:preview.author,
      categoriesid: preview.categories,
      tagsid: preview.tags,
      imgid:preview.featimage,
      vidid:preview.featvideo,
      featuredtype:preview.featuredtype
  };

  News.preview(preview_assets, function(data){
    $scope.news.name = data.author.name;
    data.author.about = $sce.trustAsHtml(data.author.about);
    $scope.author = data.author;
    $scope.news.categories = data.categories;
    $scope.news.tags = data.tags;
    $scope.featimage = $scope.amazonlink + data.featured;
    $scope.news.video = $sce.trustAsHtml(data.featured);
    console.log(data)
  })

  console.log($localStorage.atvnewspreview)
  News.listcat(function(data){
    $scope.categ = data.categories;
  });

  News.listarchieve(function(data){
    $scope.archieve = data;
  })

  News.listtags(function(data){
    $scope.listtags = data.tags;
  })
  $scope.limit = 5;
  $scope.archieveshow = true;
  var catid = undefined;
  var archieve = undefined;
  var tags = undefined;
  var loadthat=function(){
    News.list(catid,archieve,tags,function(data){
      if(!data.error){
        $scope.meron = true;
        $scope.list = data;
        $scope.loading = false;
        if($scope.limit>=data.length){
          $scope.hideloadmore = true;
        }else{
          $scope.hideloadmore = false;
        }
      }else{
        $scope.meron = false;
        $scope.hideloadmore = true;
      }

    })
  };

  loadthat();

  var limitStep = 5;

  $scope.showmorenews = function() {
      $scope.loading = true;
      loadthat();
      $scope.limit += limitStep;
    };

  // $scope.archievedate = function(datepublished,month,year){
  //   archieve = datepublished;
  //   $scope.archieveshow = false;
  //   $scope.newstitle = 'Archive : '+ month + ' ' + year;
  //   catid = undefined;
  //   tags = undefined;
  //   loadthat();
  // };

  $scope.redirectNews = function(slug){
    console.log(slug);
    $window.location.href = '/blog/' + slug;
  }

  $scope.gotocategory = function(catid, catname) {
    var category = {id:catid,name:catname};
    store.set("categorypage", category);
    $window.location.href = "/blog/category/" + catname;
  }

  $scope.gotoarchive = function(datepublished, month, year) {
    var archive = {datepublished:datepublished,month:month, year:year};
    store.set("archivepage", archive);
    $window.location.href = "/blog/archive/" + year + "/" + month;
  }

  $scope.gototag = function(tagid, tagname) {
    var tag = {id:tagid,name:tagname};
    store.set("tagpage", tag);
    $window.location.href = "/blog/tags/" + tagname;
  }
});
