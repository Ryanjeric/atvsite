app.controller('blogviewCtrl',  function(appConfig, $state, $stateParams, $scope, $http, $state, News, $timeout, $window, store){
  $scope.BaseURL = appConfig.BaseURL;

  News.listcat(function(data){
    $scope.categ = data.categories;
  });

  News.listarchieve(function(data){
    $scope.archieve = data;
  })

  News.listtags(function(data){
    $scope.listtags = data.tags;
  })
  $scope.limit = 5;
  $scope.archieveshow = true;
  var catid = undefined;
  var archieve = undefined;
  var tags = undefined;
  var loadthat=function(){
    News.list(catid,archieve,tags,function(data){
      if(!data.error){
        $scope.meron = true;
        $scope.list = data;
        $scope.loading = false;
        if($scope.limit>=data.length){
          $scope.hideloadmore = true;
        }else{
          $scope.hideloadmore = false;
        }
      }else{
        $scope.meron = false;
        $scope.hideloadmore = true;
      }

    })
  };

  loadthat();

  var limitStep = 5;

  $scope.showmorenews = function() {
      $scope.loading = true;
      loadthat();
      $scope.limit += limitStep;
    };

  // $scope.archievedate = function(datepublished,month,year){
  //   archieve = datepublished;
  //   $scope.archieveshow = false;
  //   $scope.newstitle = 'Archive : '+ month + ' ' + year;
  //   catid = undefined;
  //   tags = undefined;
  //   loadthat();
  // };

  $scope.redirectNews = function(slug){
      console.log(slug);
      $window.location.href = '/blog/' + slug;
    }

  $scope.gotocategory = function(catid, catname) {
    var category = {id:catid,name:catname};
    store.set("categorypage", category);
    $window.location.href = "/blog/category/" + catname;
  }

  $scope.gotoarchive = function(datepublished, month, year) {
    var archive = {datepublished:datepublished,month:month, year:year};
    store.set("archivepage", archive);
    $window.location.href = "/blog/archive/" + year + "/" + month;
  }

  $scope.gototag = function(tagid, tagname) {
    var tag = {id:tagid,name:tagname};
    store.set("tagpage", tag);
    $window.location.href = "/blog/tags/" + tagname;
  }
});
