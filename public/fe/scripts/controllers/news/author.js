app.controller('authorCtrl',  function(appConfig, $scope, $location, $window, $sce, anchorSmoothScroll, News){
	$scope.items = 10;
	var items = 10;
	$scope.authors3 = "https://rioverdeatv.s3.amazonaws.com/uploads/authors/";
	$scope.newss3 = "https://rioverdeatv.s3.amazonaws.com/uploads/news/";
	// /fe/blog/author/' . $authorid . '/10

	var configulr = appConfig.BaseURL;
  	var re = new RegExp(configulr+'\/(.*)');
  	var url = window.location.href;
 	var auth = url.match(re);

  var wholeurl = auth[1].replace("#", "");
  var removelastslash = wholeurl.replace(/\/$/, '');
	var authorid = removelastslash.substring(removelastslash.lastIndexOf('/')+1)

	var myblogs = function(items){
		News.authornewslist(authorid, items, function(data){
			data.author.about = $sce.trustAsHtml(data.author.about);
			$scope.author = data.author;
			$scope.myblogs = data.myblogs;
			$scope.totalblogs = data.mytotalblogs.length;
      $scope.featured = data.featured;

			$scope.categories = data.categories;
			$scope.datepublished = data.datepublished;

			$scope.ngdata = true;
			$scope.items = data.items;

		})

	}
	myblogs(items);

	$scope.showmorenews = function(items) {
		items = parseInt(items) + 10;
		myblogs(items);
	}

	$scope.goto = function(blogslugs) {
		$window.location.href = '/blog/' + blogslugs;
	}

})
.filter('dateToISO', function() {
  return function(input) {
    return new Date(input).toISOString();
  };
});
