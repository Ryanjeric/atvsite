'use strict';


// Declare app level module which depends on filters, and services
var app = angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'ui.select',
    'pascalprecht.translate',
    'app.factory',
    'app.directives',
    'ngFileUpload',
    'app.controllers',
    'xeditable', //inline editing
    'angularMoment',
    'ui.calendar',
    'angular.chosen',
    'toaster',
    'colorpicker.module'
  ])
.run(
  [          '$rootScope', '$state', '$stateParams', 'editableOptions',
    function ($rootScope,   $state,   $stateParams,  editableOptions) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        editableOptions.theme = 'bs3';
    }
  ]
)
.config(
  [          '$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$interpolateProvider','$parseProvider' ,
    function ($stateProvider,   $urlRouterProvider,   $controllerProvider,   $compileProvider,   $filterProvider,   $provide, $interpolateProvider, $parseProvider ) {
        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.factory    = $provide.factory;
        app.constant   = $provide.constant;
        app.value      = $provide.value;

        $interpolateProvider.startSymbol('{[{');
        $interpolateProvider.endSymbol('}]}');

        $urlRouterProvider
            .otherwise('/dashboard');

        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: '/atvadmin/admin/dashboard',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/dashboard.js'
                            ]);
                        }]
                }
            })

            .state('usercreate', {
                url: '/usercreate',
                controller: 'Adduserctrl',
                templateUrl: '/atvadmin/users/usercreate',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/users/create.js',
                                '/be/js/scripts/factory/users/user.js'
                            ]);
                        }]
                }
            })

            .state('userlist', {
                url: '/userlist',
                controller: 'Userlistctrl',
                templateUrl: '/atvadmin/users/userlist',
                resolve: {
                    deps: ['uiLoad',
                        function( uiLoad ){
                            return uiLoad.load( [
                                '/be/js/scripts/controllers/users/list.js',
                                '/be/js/scripts/factory/users/user.js'
                            ]);
                        }]
                }
            })

            .state('updateuser', {
                url: '/updateuser/:userid',
                controller: 'Updateuser',
                templateUrl: '/atvadmin/users/updateuser',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/users/update.js',
                            '/be/js/scripts/factory/users/user.js'
                            ]);
                    }]
                }
            })
            .state('updateprofile', {
                url: '/updateprofile',
                controller: 'Updateprofile',
                templateUrl: '/atvadmin/users/updateprofile',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/users/update.js',
                            '/be/js/scripts/factory/users/user.js'
                            ]);
                    }]
                }
            })

            .state('gallery', {
                url: '/gallery',
                controller: 'Gallery',
                templateUrl: '/atvadmin/gallery/index',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/gallery/gallery.js',
                            '/be/js/scripts/factory/gallery/gallery.js'
                            ]);
                    }]
                }
            })

            .state('contacts', {
                url: '/contacts',
                controller: 'Contacts',
                templateUrl: '/atvadmin/contacts/index',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/contacts/contacts.js',
                            '/be/js/scripts/factory/contacts/contacts.js'
                            ]);
                    }]
                }
            })

            .state('createnews', {
                url: '/createnews',
                controller: 'createnewsCtrl',
                templateUrl: '/atvadmin/news/createnews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/news/createnews.js',
                            '/be/js/scripts/directives/directives.js',
                            '/be/js/scripts/factory/news.js'
                            ]);
                    }]
                }
            })

            .state('categorytags', {
                url: '/category&tags',
                controller: 'categorytagsCtrl',
                templateUrl: '/atvadmin/news/categorytags',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/news/categorytags.js',
                            '/be/js/scripts/factory/news.js'
                            ]);
                    }]
                }
            })


            .state('newauthor', {
                url: '/news/author',
                controller: 'newauthorCtrl',
                templateUrl: '/atvadmin/news/newauthor',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/news/authors.js',
                            '/be/js/scripts/factory/news.js'
                        ]);
                    }]
                }
            })

            .state('manageauthors', {
                url: '/news/manage/authors',
                controller: 'manageauthorsCtrl',
                templateUrl: '/atvadmin/news/manageauthors',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/news/authors.js',
                            '/be/js/scripts/factory/news.js'
                        ]);
                    }]
                }
            })

            .state('updateauthor', {
                url: '/news/update/author/:authorid',
                controller: 'updateauthorCtrl',
                templateUrl: '/atvadmin/news/updateauthor',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/news/authors.js',
                            '/be/js/scripts/factory/news.js'
                        ]);
                    }]
                }
            })

            .state('freebies', {
                url: '/freebies&DeliveryService',
                controller: 'Freebies',
                templateUrl: '/atvadmin/atvdetails/freebies',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/atv/freebies.js',
                            '/be/js/scripts/factory/atv/freebies.js'
                        ]);
                    }]
                }
            })

             .state('requirements', {
                url: '/requirements',
                controller: 'Requirements',
                templateUrl: '/atvadmin/atvdetails/requirements',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/atv/requirements.js',
                            '/be/js/scripts/factory/atv/requirements.js'
                            ]);
                    }]
                }
            })

              .state('rentalhours', {
                url: '/rentalhours',
                controller: 'Rentalhours',
                templateUrl: '/atvadmin/atvdetails/rentalhours',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/atv/rentalhours.js?ver=1.1',
                            '/be/js/scripts/factory/atv/rentalhours.js?ver=1.1'
                            ]);
                    }]
                }
            })

                .state('createprices', {
                url: '/createprices',
                controller: 'Createprices',
                templateUrl: '/atvadmin/atvdetails/createprices',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/atv/createprices.js',
                            '/be/js/scripts/factory/atv/createprices.js'
                            ]);
                    }]
                }
            })

                .state('manageprices', {
                url: '/manageprices',
                controller: 'Manageprices',
                templateUrl: '/atvadmin/atvdetails/manageprices',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/atv/manageprices.js',
                            '/be/js/scripts/factory/atv/manageprices.js'
                            ]);
                    }]
                }
            })

            .state('updateprices', {
                url: '/updateprices/:asset',
                controller: 'Updateprices',
                templateUrl: '/atvadmin/atvdetails/updateprices',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/atv/updateprices.js',
                            '/be/js/scripts/factory/atv/updateprices.js',
                            '/be/js/scripts/factory/atv/createprices.js'
                            ]);
                    }]
                }
            })

            .state('createtrails', {
                url: '/createtrails',
                controller: 'Createtrails',
                templateUrl: '/atvadmin/trails/createtrails',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/trails/createtrails.js',
                            '/be/js/scripts/factory/trails/trails.js',
                            ]);
                    }]
                }
            })
            .state('managetrails', {
                url: '/managetrails',
                controller: 'Managetrails',
                templateUrl: '/atvadmin/trails/managetrails',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/trails/managetrails.js',
                            '/be/js/scripts/factory/trails/trails.js',
                            ]);
                    }]
                }
            })

            .state('updatetrails', {
                url: '/updatetrails/:asset',
                controller: 'Updatetrails',
                templateUrl: '/atvadmin/trails/updatetrails',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                           '/be/js/scripts/controllers/trails/updatetrails.js',
                            '/be/js/scripts/factory/trails/trails.js',
                            ]);
                    }]
                }
            })

            .state('reservations', {
                url: '/reservations',
                controller: 'ReservationsCtrl',
                templateUrl: '/atvadmin/reservations/index',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                           '/be/js/scripts/controllers/reservations/reservations.js',
                            '/be/js/scripts/factory/reservations/reservations.js',
                            ]);
                    }]
                }
            })

            .state('managenews', {
                url: '/news/manage',
                controller: 'managenewsCtrl',
                templateUrl: '/atvadmin/news/managenews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/news/managenews.js',
                            '/be/js/scripts/directives/directives.js',
                            '/be/js/scripts/factory/news.js'
                            ]);
                    }]
                }
            })

            .state('updatenews', {
                url: '/news/update/:newsid',
                controller: 'updatenewsCtrl',
                templateUrl: '/atvadmin/news/updatenews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/news/updatenews.js',
                            '/be/js/scripts/directives/directives.js',
                            '/be/js/scripts/factory/news.js'
                            ]);
                    }]
                }
            })

            .state('metadata', {
                url: '/metadata/manage',
                controller: 'MetadataCtrl',
                templateUrl: '/atvadmin/metadata/index',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/metadata/metadata.js',
                            '/be/js/scripts/directives/directives.js',
                            '/be/js/scripts/factory/metadata/metadata.js'
                            ]);
                    }]
                }
            })

            .state('trailsmeta', {
                url: '/trails/categorymeta',
                controller: 'MetatrailsCtrl',
                templateUrl: '/atvadmin/trails/metadata',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/trails/metadata.js',
                            '/be/js/scripts/directives/directives.js',
                            '/be/js/scripts/factory/trails/metadata.js'
                            ]);
                    }]
                }
            })


            .state('adventure', {
                url: '/exiting-adventrures-await',
                controller: 'Adventure',
                templateUrl: '/atvadmin/gallery/adventure',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/gallery/adventure.js',
                            '/be/js/scripts/factory/gallery/gallery.js'
                            ]);
                    }]
                }
            })

            .state('reservationnotice', {
                url: '/reservation-notice',
                controller: 'Reservationnotice',
                templateUrl: '/atvadmin/reservations/reservationnotice',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/reservations/notice.js',
                            '/be/js/scripts/factory/reservations/notice.js'
                            ]);
                    }]
                }
            })


             //SETTINGS
            .state('settings', {
                url: '/settings',
                controller: 'Settings',
                templateUrl: '/atvadmin/settings/settings',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/settings/settings.js',
                            '/be/js/scripts/factory/settings/settings.js',
                            ]);
                    }]
                }
            })

            // manage home details
            .state('homedetails', {
                url: '/page/manage/home/details',
                controller: 'HomeCtrl',
                templateUrl: '/atvadmin/page/home',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/page/home.js',
                            '/be/js/scripts/directives/directives.js',
                            '/be/js/scripts/factory/page/details.js'
                        ]);
                    }]
                }
            })

            // manage contact details
            .state('contactdetails', {
                url: '/page/manage/contact/details',
                controller: 'ContactDetailsCtrl',
                templateUrl: '/atvadmin/page/contact',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/be/js/scripts/controllers/page/contact.js',
                            '/be/js/scripts/directives/directives.js',
                            '/be/js/scripts/factory/page/details.js'
                        ]);
                    }]
                }
            })

            //page banners
            .state('pagebanners', {
                url: '/page/manage/banners',
                controller: 'PageBannerCtrl',
                templateUrl: '/atvadmin/page/banner',
                resolve: {
                    deps: ['uiLoad', function(uiLoad) {
                        return uiLoad.load([
                            '/be/js/scripts/controllers/page/banner.js',
                            '/be/js/scripts/directives/directives.js',
                            '/be/js/scripts/factory/page/details.js'
                        ]);
                    }]
                }
            });

    }
  ]
)

.config(['$translateProvider', function($translateProvider){

  // Register a loader for the static files
  // So, the module will search missing translation tables under the specified urls.
  // Those urls are [prefix][langKey][suffix].
  $translateProvider.useStaticFilesLoader({
    prefix: '/be/js/jsons/',
    suffix: '.json'
  });

  // Tell the module what language to use by default
  $translateProvider.preferredLanguage('en');

  // Tell the module to store the language in the local storage
  $translateProvider.useLocalStorage();

}])

/**
 * jQuery plugin config use ui-jq directive , config the js and css files that required
 * key: function name of the jQuery plugin
 * value: array of the css js file located
 */
.constant('JQ_CONFIG', {
    easyPieChart:   ['/be/js/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
    sparkline:      ['/be/js/jquery/charts/sparkline/jquery.sparkline.min.js'],
    plot:           ['/be/js/jquery/charts/flot/jquery.flot.min.js',
                        '/be/js/jquery/charts/flot/jquery.flot.resize.js',
                        '/be/js/jquery/charts/flot/jquery.flot.tooltip.min.js',
                        '/be/js/jquery/charts/flot/jquery.flot.spline.js',
                        '/be/js/jquery/charts/flot/jquery.flot.orderBars.js',
                        '/be/js/jquery/charts/flot/jquery.flot.pie.min.js'],
    slimScroll:     ['/be/js/jquery/slimscroll/jquery.slimscroll.min.js'],
    sortable:       ['/be/js/jquery/sortable/jquery.sortable.js'],
    nestable:       ['/be/js/jquery/nestable/jquery.nestable.js',
                        '/be/js/jquery/nestable/nestable.css'],
    filestyle:      ['/be/js/jquery/file/bootstrap-filestyle.min.js'],
    slider:         ['/be/js/jquery/slider/bootstrap-slider.js',
                        '/be/js/jquery/slider/slider.css'],
    chosen:         ['/be/js/jquery/chosen/chosen.jquery.min.js',
                        '/be/js/jquery/chosen/chosen.css'],
    TouchSpin:      ['/be/js/jquery/spinner/jquery.bootstrap-touchspin.min.js',
                        '/be/js/jquery/spinner/jquery.bootstrap-touchspin.css'],
    wysiwyg:        ['/be/js/jquery/wysiwyg/bootstrap-wysiwyg.js',
                        '/be/js/jquery/wysiwyg/jquery.hotkeys.js'],
    dataTable:      ['/be/js/jquery/datatables/jquery.dataTables.min.js',
                        '/be/js/jquery/datatables/dataTables.bootstrap.js',
                        '/be/js/jquery/datatables/dataTables.bootstrap.css'],
    vectorMap:      ['/be/js/jquery/jvectormap/jquery-jvectormap.min.js',
                        '/be/js/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
                        '/be/js/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
                        '/be/js/jquery/jvectormap/jquery-jvectormap.css'],
    footable:       ['/be/js/jquery/footable/footable.all.min.js',
                        '/be/js/jquery/footable/footable.core.css']
    }
)


.constant('MODULE_CONFIG', {
    select2:        ['/be/js/jquery/select2/select2.css',
                        '/be/js/jquery/select2/select2-bootstrap.css',
                        '/be/js/jquery/select2/select2.min.js',
                        '/be/js/modules/ui-select2.js']
    }
)
;
