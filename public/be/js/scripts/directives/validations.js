app.directive('ensureUnique', ['$http','Config', function($http, Config) {
  return {

    require: 'ngModel',
    link: function(scope, ele, attrs, c) {

      scope.$watch(attrs.ngModel, function() {
        $http({
          method: 'POST',
          url: Config.ApiURL + '/validation/author/uniquename',
          headers: {"Content-Type" : "application/x-www-form-urlencoded"},
          data: $.param({'field': c.$viewValue})
        }).success(function(data, status, headers, cfg) {
          c.$setValidity('unique', data.isUnique);
        }).error(function(data, status, headers, cfg) {
          console.log('there is an error in authors name validation');
        });
      });
    }
  }
}])
.directive('ensureUniqueNewsslugs', ['$http','Config', function($http, Config) {
  return {

    require: 'ngModel',
    link: function(scope, ele, attrs, c) {

      scope.$watch(attrs.ngModel, function() {
        $http({
          method: 'POST',
          url: Config.ApiURL + '/validation/news/uniqueslugs',
          headers: {"Content-Type" : "application/x-www-form-urlencoded"},
          data: $.param({'field': c.$viewValue})
        }).success(function(data, status, headers, cfg) {
          c.$setValidity('unique', data.isUnique);
        }).error(function(data, status, headers, cfg) {
          console.log('there is an error in authors name validation');
        });
      });
    }
  }
}])
.directive('ensureUniqueTrailslugs', ['$http','Config', function($http, Config) {
  return {

    require: 'ngModel',
    link: function(scope, ele, attrs, c) {

      scope.$watch(attrs.ngModel, function() {
        $http({
          method: 'POST',
          url: Config.ApiURL + '/validation/trail/uniqueslugs',
          headers: {"Content-Type" : "application/x-www-form-urlencoded"},
          data: $.param({'field': c.$viewValue})
        }).success(function(data, status, headers, cfg) {
          c.$setValidity('unique', data.isUnique);
        }).error(function(data, status, headers, cfg) {
          console.log('there is an error in authors name validation');
        });
      });
    }
  }
}])
