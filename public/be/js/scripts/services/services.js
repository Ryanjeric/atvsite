app.service("AtvService", function($http, Config, $modal, Upload, $window, toaster){
    return {
        msg: function(code) {
            if(typeof code === "string") {
                code = code.split("/");
                errCode = code[0];
            } else {
                errCode = code;
            }

            var msg;
            switch(errCode) {
                case 200 : msg = "Success"; break;
                case "err" : msg = "Something went wrong. Please try again."; break;
                case "formErr" : msg = "Please fix all errors before saving"; break;
                case "inc" : msg = "Please complete the form before saving"; break;
                case "inv" : msg = "Please enter a valid " + code[1]; break;
                case "new" : msg = "New " + code[1] + " has been successfully added"; break;
                case "add" : msg = "New " + code[1] + " has been successfully added"; break;
                case "added" : msg = "New " + code[1] + " has been successfully added"; break;
                case "update" : msg = code[1] + " has been successfully updated"; break;
                case "updated" : msg = code[1] + " has been successfully updated"; break;
                case "delete" : msg = code[1] + " has been successfully deleted"; break;
                case "deleted" : msg = code[1] + " has been successfully deleted"; break;
                case "scheduled" : msg = "New " + code[1] + " has been successfully scheduled"; break;
                case "errGt2mb" : msg = "Unable to process image with 2mb in size."; break;
                case "dateUnavailable" : msg = "This Date is unavailable"; break;
                default: msg = errCode;
            }
            return msg;
        },
        serial: function(value) {
            if(value === undefined ) {
                return "";
            } else {
                value = value.replace(/[^\w -]+/g,'');
                return value.replace(/ +/g,'-');
            }
        },
        setTouched: function(formErrors){
            angular.forEach(formErrors, function (field) {
                angular.forEach(field, function(errField) {
                    return errField.$setTouched();
                });
            });
        },
        modal: function(tplUrl, size, ctrl) {
          var modalInstance = $modal.open({
              templateUrl: tplUrl,
              controller: ctrl,
              size: size
          });
          return modalInstance;
        },
        upload: function(path, file, callback) {
            var filename = (file.hasOwnProperty("customName") ? file.customName : file.name );
            var promise = Upload.upload({
                url: Config.amazonlink, //S3 upload url including bucket name
                method: 'POST',
                transformRequest: function (data, headersGetter) {
                                  //Headers change here
                                  var headers = headersGetter();
                                //   delete headers['Authorization'];
                                  delete headers.Authorization;
                                  return data;
                                },
                fields : {
                    key: path + filename, // the key to store the file on S3, could be file name or customized
                    AWSAccessKeyId: Config.AWSAccessKeyId,
                    acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                    policy: Config.policy, // base64-encoded json policy (see article below)
                    signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                    "Content-Type": file.type !== '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                },
                file: file
            });
            callback(promise);
        },
        blob: function(dataURI) {
            var binary = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
            var array = [];
            for(var i = 0; i < binary.length; i++) {
                array.push(binary.charCodeAt(i));
            }
            return new Blob([new Uint8Array(array)], {type: mimeString});
        },
        getTime: function() {
            var d = new Date();
            return d.getTime();
        },
        toaster: function(title, body) {
            body = this.msg(body);
            return toaster.pop({
                type: title.toLowerCase(),
                title: title,
                body: body,
                showCloseButton: true
            });
        },
        newDate: function(date, format) {
            var newDate = new Date(date);

            //month starts with index 0, 
            //make always 2digits
            // parameters {t = type(day || month), n = value}
            function n(n, t){
                if(t == 'd') {
                    return n  > 9 ? "" + n : "0" + n ;
                } else if (t == 'm') {
                    return (n + 1)  > 9 ? "" + n : "0" + (n + 1);
                }
            }

            var dF; //dateFormat
            switch(format) {
                case "min" : dF = newDate.getMinutes();
                    break;
                case "d" : dF = n(newDate.getDate(), 'd');
                    break;
                case "m" : dF = n(newDate.getMonth(), 'm'); 
                    break;
                case "y" : dF = newDate.getFullYear(); 
                    break;
                case "ymd" : dF = newDate.getFullYear() + "-" + n(newDate.getMonth(), 'm') + "-" + n(newDate.getDate(),'d'); 
                    break;
                default: dF = date;
            }

            return dF;
        },
        swal: function(type, title) {
            msg = this.msg(title);
            swal({
                title: type, 
                text: msg, 
                type: type.toLowerCase(),
                allowOutsideClick: true,
                timer: 7000
            });
        },
        swalLoaderOnConfirm: function(title, action) {
            swal({   
                    title: title,   
                    text: "Do you really want to proceed?",   
                    type: "info",
                    showCancelButton: true,   
                    closeOnConfirm: false,   
                    showLoaderOnConfirm: true, 
                }, function() {
                    setTimeout(function(){     
                        action();
                    }, 100);
                }
            );
        },
        swalRedirect: function(title, action) {
            swal({   
                    title: title,   
                    text: "Do you really want to proceed?",   
                    type: "info",
                    showCancelButton: true 
                }, function() {   
                    action();
                }
            );
        },
        months: function () {
            return [
                {name: 'January', val: '01'},
                {name: 'February', val: '02'},
                {name: 'March', val: '03'},
                {name: 'April', val: '04'},
                {name: 'May', val: '05'},
                {name: 'June', val: '06'},
                {name: 'July', val: '07'},
                {name: 'August', val: '08'},
                {name: 'September', val: '09'},
                {name: 'October', val: '10'},
                {name: 'November', val: '11'},
                {name: 'December', val: '12'}
            ];
        },
        yearForFilter: function() {
            var d = new Date();
            var year = d.getFullYear();

            var gap = 5; //

            var years = [];
            for(var o = year - gap; o <= year; o++) {
                years.push(o);
            }

            return years;
        }
    };
});
