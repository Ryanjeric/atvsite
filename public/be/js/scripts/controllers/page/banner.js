'use strict';

app.controller("PageBannerCtrl", function($scope, Config, $modal, $state, $location, anchorSmoothScroll, Upload, PageFactory) {
	$scope.amazonlink = Config.amazonlink;
	$scope.pages = [];
	$scope.preview = "Desktop";
	$scope.alerts = [];

	var loadlist = function() {
		PageFactory.loadbanner(function(data){
			console.log(data);
			$scope.pages = data;
		});
	}

	loadlist();

	$scope.closeAlert = function(){ 
		$scope.alerts = [];
	}

	$scope.showimageList = function(size, index){
		$scope.pageindex = index;
	    var amazon = $scope.amazon;
	    var modalInstance = $modal.open({
	        templateUrl: 'reservationimglist.html',
	        controller: imagelistCTRL,
	        size: size,
	        resolve: {
	            path: function() {
	                return amazon
	            }
	        }

	    });
	}

	$scope.stat="1";
	var pathimage = "";

	var pathimages = function(){
	    $scope.amazonpath=pathimage;
	    $scope.pages[$scope.pageindex].banner = pathimage;
	}

	var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
	    $scope.amazonpath = Config.amazonlink + "/uploads/images/";
	    $scope.imageloader=false;
	    $scope.imagecontent=true;
	    $scope.noimage = false;
	    $scope.imggallery = false;

	    var loadimages = function() {
	        $http({
	            url: Config.ApiURL + "/pagedetails/banner/imglist",
	            method: "GET",
	            headers: {
	                'Content-Type': 'application/x-www-form-urlencoded'
	            }
	        }).success(function(data) {
	            if(data.error == "NOIMAGE" ){
	              $scope.imggallery=false;
	              $scope.noimage = true;
	            }else{
	              $scope.noimage = false;
	              $scope.imggallery=true;
	              $scope.imagelist = data;
	            }
	        }).error(function(data) {
	        });
	    }
	    loadimages();

	    $scope.path=function(path){
	      var texttocut = Config.amazonlink + '/uploads/images/';
	      var newpath = path.substring(texttocut.length); 
	      pathimage = newpath;
	      pathimages();
	      $modalInstance.dismiss('cancel');
	    }



	    $scope.upload = function(files) {
	       $scope.upload(files);  
	    };

	    $scope.delete = function(id){
	          var modalInstance = $modal.open({
	            templateUrl: 'notification.html',
	            controller: deleteCTRL,
	            resolve: {
	              imgid: function() {
	                return id
	              }
	            }
	          });
	       }
	 
	 var deleteCTRL = function($scope, $modalInstance, imgid) {

	  $scope.alerts = [];

	    $scope.closeAlert = function (index) {
	        $scope.alerts.splice(index, 1);
	    };

	  $scope.message="Are you sure do you want to delete this Photo?";
	    $scope.ok = function() {
	     $http({
	     url: Config.ApiURL+"/pagedetails/banner/deleteimage/"+ imgid,
	      method: "get",
	      headers: {
	        'Content-Type': 'application/x-www-form-urlencoded'
	      },
	    }).success(function(data, status, headers, config) {
	      $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
	      loadimages();
	      $modalInstance.close();
	    }).error(function(data, status, headers, config) {
	      loadimages();
	      $modalInstance.close();
	      $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
	    });
	  };

	  $scope.cancel = function() {
	    $modalInstance.dismiss('cancel');
	  };
	  }

	  $scope.upload = function (files) 
	  {
	    var filename;
	    var filecount = 0;
	    if (files && files.length) 
	    {
	      $scope.imageloader=true;
	      $scope.imagecontent=false;

	      for (var i = 0; i < files.length; i++) 
	      {
	        var file = files[i];

	        if (file.size >= 2000000)
	        {
	          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
	          filecount = filecount + 1;
	          
	          if(filecount == files.length)
	          {
	            $scope.imageloader=false;
	            $scope.imagecontent=true;
	          }
	        }
	        else
	        {
	          var promises;

	          var fileExtension = '.' + file.name.split('.').pop();

	                    // rename the file with a sufficiently random value and add the file extension back
	                    var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + i + fileExtension;
	                    
	                    promises = Upload.upload({
	                      
	                                 url:Config.amazonlink, //S3 upload url including bucket name
	                                 method: 'POST',
	                                 transformRequest: function (data, headersGetter) {
	                                  //Headers change here
	                                  var headers = headersGetter();
	                                  delete headers['Authorization'];
	                                  return data;
	                                },
	                                fields : {
	                                    key: 'uploads/images/' + renamedFile, // the key to store the file on S3, could be file name or customized
	                                    AWSAccessKeyId: Config.AWSAccessKeyId,
	                                    acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
	                                    policy: Config.policy, // base64-encoded json policy (see article below)
	                                    signature: Config.signature, // base64-encoded signature based on policy string (see article below)
	                                    "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
	                                  },
	                                  file: file
	                                })
	          promises.then(function(data){

	            filecount = filecount + 1;

	            filename = data.config.file.name;
	            var fileout = {
	              'imgfilename' : renamedFile
	            };
	            $http({
	              url: Config.ApiURL + "/pagedetails/savebanner",
	              method: "POST",
	              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	              data: $.param(fileout)
	            }).success(function (data, status, headers, config) {
	              loadimages();
	              if(filecount == files.length)
	              {
	                $scope.imageloader=false;
	                $scope.imagecontent=true;
	              }
	              
	            }).error(function (data, status, headers, config) {
	              $scope.imageloader=false;
	              $scope.imagecontent=true;
	            });
	            
	          });
	        }
	      }
	    }
	  };
	  $scope.cancel = function() {
	      $modalInstance.dismiss('cancel');

	  };
	};

	$scope.updatebanner = function(page) {
		PageFactory.updatebanner(page, function(data) {
			$scope.closeAlert();
			if(data.hasOwnProperty('success')){
				$scope.alerts.push({ type: 'success', msg: data.success });
			} else {
				$scope.alerts.push({ type: 'danger', msg: 'Something went wrong, please try again later.' });
			}
			anchorSmoothScroll.scrollTo('top');
		});
	}
});
