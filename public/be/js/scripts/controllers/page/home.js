'use strict';

app.controller("HomeCtrl", function($scope, Config, $modal, $state, $location, anchorSmoothScroll, Upload, PageFactory) {
	$scope.amazonlink = Config.amazonlink;

	var loadhome = function() {
		PageFactory.gethome(function(data) {
			$scope.home = data;
		});
	}

	loadhome();

	$scope.updateimg = function(num) {
		var modalInstance = $modal.open({
			templateUrl: 'homeimg.html',
			controller: function($modalInstance, $scope, img) {
				$scope.tpl_title = "Image settings";
				$scope.amazonlink = Config.amazonlink;
				$scope.file = {};
				$scope.alertss = [];

				switch(num){
					case 1: 
						$scope.img = { title : img.title1, filename : img.img1, link : img.link1};
						break;
					case 2: 
						$scope.img = { title : img.title2, filename : img.img2, link : img.link2};
						break;
					case 3: 
						$scope.img = { title : img.title3, filename : img.img3, link : img.link3};
						break;
				}

				$scope.cancel = function() {
					$modalInstance.dismiss();
				}

				var updateimg = function(num, img) {
					PageFactory.updatehomeimg(num, img, function(data) {
						if(data.hasOwnProperty('success')){
							loadhome();
							$modalInstance.dismiss();
						}
					});
				}

				$scope.ok = function(img) {
					if($scope.file.length){
						$scope.loading = true;
						var file = $scope.file[0];
						console.log(file.size);
						if (file.size >= 2000000) {
							$scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});	
							$scope.loading = false;	          
						} else {
							var promises = Upload.upload({
	                 url:Config.amazonlink, //S3 upload url including bucket name
	                 method: 'POST',
	                 transformRequest: function (data, headersGetter) {
	                  //Headers change here
	                  var headers = headersGetter();
	                  delete headers['Authorization'];
	                  return data;
	                },
	                fields : {
	                    key: 'uploads/images/' + file.name, // the key to store the file on S3, could be file name or customized
	                    AWSAccessKeyId: Config.AWSAccessKeyId,
	                    acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
	                    policy: Config.policy, // base64-encoded json policy (see article below)
	                    signature: Config.signature, // base64-encoded signature based on policy string (see article below)
	                    "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
	                  },
	                  file: file
	                })
		          promises.then(function(data){
		            img['filename'] = file.name;
		            updateimg(num, img);
		          });

						}
					} else {
						updateimg(num, img);
					}					
				}

				$scope.setimg = function(file) {
					$scope.file = file;
				}
			},
			resolve: {
				num: function() {
					return num;
				},
				img: function() {
					return $scope.home;
				}
			}
		});
	}
});
