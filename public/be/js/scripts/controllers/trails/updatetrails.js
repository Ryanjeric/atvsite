'use strict';

/* Controllers */

app.controller('Updatetrails', function($scope, $state, $http, Config, $modal , $stateParams, Trails){
	var trailid = $stateParams.asset;
	$scope.trails = {};
	$scope.disableslugs = true;

  var s3 = Config.amazonlink+'/uploads/images/';
	$scope.alerts = [];
  var mapfilename = {};
  var filemap = [];
  $scope.map = {};
  $scope.map1 = {};
  $scope.rentalhr = {};
   $scope.closeAlert = function (index) {
        $scope.process =0;
        $scope.alerts.splice(index, 1);
    };

    $scope.setFile = function(element,index) {
        $scope.$apply(function($scope) {
            mapfilename[element.id] = element.files[0].name;
            filemap.push(element.files[0]);
            console.log(mapfilename);
        });
    };

    $scope.remove = function(index){
         mapfilename[index] = '';
         console.log(mapfilename);
    }

    $scope.selected = {};
    var checkThis = function(arrayVal){

    for(var n in arrayVal){
      console.log(arrayVal[n]['rentalid']);
      $scope.selected[arrayVal[n]['rentalid']]=true;
      $scope.rentalhr[arrayVal[n]['rentalid']] = arrayVal[n]['rentalid'];
      $scope.map[arrayVal[n]['rentalid']] = s3+arrayVal[n]['map'];
      $scope.map1[arrayVal[n]['rentalid']] = s3+arrayVal[n]['map'];
      mapfilename[arrayVal[n]['rentalid']] = arrayVal[n]['map'];
    }
  }

	$scope.ensureuniqueslugs = function(slugs) {
		ensureUniqueSlugs(slugs);
	}

	var ensureUniqueSlugs = function(slugs) {
		Trails.ensureuniqueslugs(trailid, slugs, function(data){
			$scope.myForm.slugs.$setValidity("unique", data.isUnique);
			console.log(data)
		});
	}

  Trails.info($stateParams.asset,function(data){
    $scope.trails = data.data;
    $scope.defaulttitle = data.data.title;
    $scope.picFile= s3+data.data.featuredimage;
    $scope.picFile1= s3+data.data.firstimage;
    $scope.picFile2= s3+data.data.secondimage;
    $scope.picFilez= s3+data.data.featuredimage;
    $scope.picFilez1= s3+data.data.firstimage;
    $scope.picFilez2= s3+data.data.secondimage;
    $scope.map = {};
    $scope.rentalhr = {};
    checkThis(data.data2);
    console.log( $scope.map);
  });

Trails.listrentals(function(data){
      $scope.listrental = data;
});

$scope.ngchangetitle = function(title) {
	if(title=="" || title==null) {
		$scope.trails.slugs = " ";
	} else {
		var sanitize = title.replace(/[^\w ]+/g,'');
		$scope.trails.slugs = angular.lowercase(sanitize.replace(/ +/g,'-'));
		ensureUniqueSlugs($scope.trails.slugs);
	}
}

function onSubmit()
{
  var fields = $("input[name='list']").serializeArray();
  if (fields.length == 0)
  {
    alert('Please Select Rental Hour and Upload a Map');
    // cancel submit
    return false;
  }
  else
  {
   $scope.Savetrails= function(trails,picFile,picFile1,picFile2,rentalhr,map){
    var file1,file2,file3,mp;
    $scope.process = 10;
    $scope.wait = "Uploading Photos! PLEASE WAIT";
    var cuttext = function(url){
     var texttocut = Config.amazonlink + '/uploads/images/';
     var newpath = url.substring(texttocut.length);
     return newpath;
    }

    $scope.flag = 0;

    if(picFile == $scope.picFilez){file1 = cuttext($scope.picFilez); $scope.flag +=1; }
    if(picFile != $scope.picFilez){
      file1 = picFile.name;
      Trails.uploadpic(picFile,function(log){
        console.log('hit 1');
        $scope.flag +=1;
      });
    }
    if(picFile1 == $scope.picFilez1){ file2 = cuttext($scope.picFilez1); $scope.flag +=1;}
    if(picFile1 != $scope.picFilez1){
      file2 = picFile1.name;
      Trails.uploadpic(picFile1,function(log){
        console.log('hit 2');
        $scope.flag +=1;
      });
    }
    if(picFile2 == $scope.picFilez2){file3 = cuttext($scope.picFilez2); $scope.flag +=1;}
    if(picFile2 != $scope.picFilez2){
      file3 = picFile2.name;
      Trails.uploadpic(picFile2,function(log){
        console.log('hit 3');
        $scope.flag +=1;
      });
    }

    function getObjectLength(obj)
    {
      var length = 0;
      for ( var p in obj )
      {
        if ( obj.hasOwnProperty( p ) )
        {
          length++;
        }
      }
      return length;
    }

    if(getObjectLength(mapfilename) != 0){mp =  mapfilename;
      if(filemap.length == 0){
        $scope.flag +=1;
      }else{ Trails.uploadpic2(filemap,function(log){
        console.log('hit 4');
        $scope.flag +=1;
      })}
    }

   var req = {
        trails: trails,
        featured: file1,
        stpicture : file2,
        ndpicture : file3,
        rentalhr : rentalhr,
        map : mapfilename
    }

    $scope.$watch('flag', function(data){
        console.log(data);
        if(data == 1){
            $scope.process = 20;
        }
        if(data == 2){
            $scope.process = 40;
        }
        if(data == 3){
            $scope.process = 60;
        }
        if(data == 4){
          $scope.process = 100;
          $scope.wait = "DONE";
          Trails.update(req,function(data){
            if(!data.error ){
              $scope.alerts.splice(0, 1);
              $scope.alerts.push({ type: 'success', msg: 'Trails successfully Updated!' });
            }else{
              $scope.alerts.splice(0, 1);
              $scope.alerts.push({ type: 'warning', msg: 'Something went wrong,Please Check all input before saving' });
            }
          });
        }
    });
   }
  }
}

$('#wewForm').submit(onSubmit)



});
