'use strict';

/* Controllers */

app.controller('MetatrailsCtrl', function($scope, $state, Upload ,$q, $http, Config, $modal ,Metatrails ){
	console.log("--Metatrails--");
  $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };


    var num_c = 10;
    var off_c = 1;
    var keyword_c = null;

    var paginate = function() {
      Metatrails.list(num_c,off_c,keyword_c,function(data){
        console.log(data.data);
        $scope.list = data.data;
        $scope.maxSize = 5;
        $scope.bigTotalItems = data.total_items;
        $scope.bigCurrentPage = data.index;
      })
    }
    paginate();

    $scope.setPage = function (off) {
      paginate();
    };


     var notificationCTRL = function($scope, element, $modalInstance) {
        $scope.message = element.msg;
        $scope.type = element.type;
        $scope.okay= function() {
            $modalInstance.close();
        }
    }



     $scope.update= function(id){
        var modalInstance = $modal.open({
            templateUrl: 'metadata.html',
            controller: updateCTRL,
            resolve: {
                id: function () {
                  return id;
              }}
        });
    }

    var updateCTRL = function($scope, $modalInstance, id) {
        $scope.catalerts = [];
        $scope.tpl_title = "Update Trails Meta Data";

        Metatrails.edit(id, function(data) {
            $scope.asset = data;
        });

        $scope.cancel = function() {
            $modalInstance.close();
        }
        $scope.ok = function(asset) {
            Metatrails.update(asset, function(data) {
                $modalInstance.close();
                paginate();
                var modalInstance = $modal.open({
                    templateUrl: 'notification2.html',
                    controller: notificationCTRL,
                    resolve: {
                        element: function() {
                            var element = {'msg' : data.msg, 'type' : data.type };
                            return element
                        }
                    }
                });
            });
        }
    }    



});