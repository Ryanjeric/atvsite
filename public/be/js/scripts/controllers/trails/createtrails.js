'use strict';

/* Controllers */

app.controller('Createtrails', function($scope, $state, Upload ,$q, $http, Config, $modal ,Trails ){
		$scope.disableslugs = true;
		$scope.trails = {};

    $scope.base_url = Config.BaseURL;
    $scope.alerts = [];
    var mapfilename = [];
    var filemap = [];
    $scope.setFile = function(element,index) {
        $scope.$apply(function($scope) {
            mapfilename[element.id] = element.files[0].name;
            filemap.push(element.files[0]);
            console.log(filemap);
        });
    };

    $scope.remove = function(index){
         mapfilename[index] = '';
    }

		// $scope.$watch("trails.title", function(val) {
		// 	if(val=="" || val==null) {
		// 		$scope.trails.slugs = " ";
		// 	} else {
		// 		var sanitize = val.replace(/[^\w ]+/g,'');
		// 		$scope.trails.slugs = angular.lowercase(sanitize.replace(/ +/g,'-'));
		// 	}
		// })
		$scope.ngchangetitle = function(val) {
			if(val=="" || val==null) {
				$scope.trails.slugs = " ";
			} else {
				var sanitize = val.replace(/[^\w ]+/g,'');
				$scope.trails.slugs = angular.lowercase(sanitize.replace(/ +/g,'-'));
			}
		}
		$scope.refreshslugs = function(title) {
			if(title=="" || title==null) {
				$scope.trails.slugs = " ";
			} else {
				var sanitize = title.replace(/[^\w ]+/g,'');
				$scope.trails.slugs = angular.lowercase(sanitize.replace(/ +/g,'-'));
			}
		}

    $scope.closeAlert = function (index) {
        $scope.process =0;
        $scope.alerts.splice(index, 1);
    };

    Trails.listrentals(function(data){
      $scope.listrental = data;
  });
    $scope.map = {};
    $scope.rentalhr = {};

    function onSubmit()
    {
      var fields = $("input[name='list']").serializeArray();
      if (fields.length == 0)
      {
        alert('Please Select Rental Hour and Upload a Map');
        // cancel submit
        return false;
       }
    else
    {
    $scope.Savetrails= function(trails,picFile,picFile1,picFile2,rentalhr,map){
        $scope.process = 5;
    if(picFile == undefined || picFile1 == undefined || picFile2 == undefined){
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: 'warning', msg: 'Something went wrong,Please Check all fields before saving' });
    }else{
        $scope.process = 10;
    $scope.wait = "Uploading Photos! PLEASE WAIT";
    var req = {
        trails: trails,
        featured: picFile.name,
        stpicture : picFile1.name,
        ndpicture : picFile2.name,
        rentalhr : rentalhr,
        map : mapfilename
    }

    $scope.flag = 0;
    Trails.uploadpic(picFile,function(log){
        console.log('hit 1');
        $scope.flag +=1;
    });

    Trails.uploadpic(picFile1,function(log){
        console.log('hit 2');
        $scope.flag +=1;
    });

    Trails.uploadpic(picFile2,function(log){
        console.log('hit 3');
        $scope.flag +=1;
    });
    Trails.uploadpic2(filemap,function(log){
        console.log('hit 4');
        $scope.flag +=1;
    })


    $scope.$watch('flag', function(data){
        console.log('hitting changes');
        if(data == 1){

            $scope.process = 20;
        }
        if(data == 2){
            $scope.process = 40;
        }
        if(data == 3){
            $scope.process = 60;
        }
        if(data == 4){
            console.log('hit only once if flag is 4');
                $scope.process = 100;
                $scope.wait = "DONE";
                Trails.saveme(req,function(data){
                    if(!data.error ){
                        $scope.alerts.splice(0, 1);
                        $scope.alerts.push({ type: 'success', msg: 'Trails successfully added!' });
                        $scope.trails = "";
                        $scope.picFile = undefined;
                        $scope.picFile1 = undefined;
                        $scope.picFile2 = undefined;
                        $scope.rentalhr = {};
                        $scope.map = {};
                        filemap = [];
                        mapfilename = [];
                        req = {};
                        $scope.flag = 0;
                        $scope.myForm.$setPristine(true);
                        $scope.process = 0;
                    }else{
                     $scope.alerts.splice(0, 1);
                     $scope.alerts.push({ type: 'warning', msg: 'Something went wrong,Please Check all input before saving' });
                 }
                });
        }
    });
    }


    }
}
}

$('#wewForm').submit(onSubmit)

});
