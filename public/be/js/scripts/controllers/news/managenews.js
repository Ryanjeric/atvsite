app.controller("managenewsCtrl", function($scope, $modal, $state, $location, anchorSmoothScroll, $timeout, Config, NewsFactory, moment) {
  $scope.mainalerts = [];

	var items = 10;
	var page = 1;
	var keyword = null;
	$scope.maxSize = 5;

	var newsPagination = function(page, keyword) {
		var asset = {items:items, page:page, keyword:keyword};
		NewsFactory.newspagination(asset, function(data){
      angular.forEach(data.news, function(value, key) {
        data.news[key].datepublished = moment(value.datepublished).format("MMMM DD, YYYY");
      })
			$scope.news = data.news;
			$scope.TotalItems = data.TotalItems;
			$scope.CurrentPage = data.index;
		});
	}
	newsPagination(page, keyword);

	$scope.setPage = function(page, keyword) {
		newsPagination(page, keyword);
	}
	$scope.search = function (newkeyword) {
		newsPagination(page, newkeyword);
	}
	$scope.clearsearch = function() {
		newsPagination(page, keyword);
		$scope.keyword = "";
	}

  $scope.newstatus = function(id, status, keyword, page) {
    console.log(id, status, keyword, page);
    var newsstatus = {id:id, status:status};
    NewsFactory.changenewsstatus(newsstatus, function(data){
      newsPagination(page, keyword);
    })
  }

  $scope.update = function(newsid) {
		var modal_data = {'templateUrl':'updatedelete',
					'size':'md',
					'controller':actionCtrl,
					'type' : 'info',
					'heading':'Action Nofication',
					'body':'Do you want to update this News?',
					'cancel':'Cancel',
					'ok':'Yes',
					'mode':'update',
					'id':newsid
					}
		MODAL(modal_data);
	}
	$scope.delete = function(newsid, keyword, page) {
		var modal_data = {'templateUrl':'updatedelete',
					'size':'md',
					'controller':actionCtrl,
					'type' : 'info',
					'heading':'Action Nofication',
					'body':'Do you want to delete this News?',
					'cancel':'Cancel',
					'ok':'Yes',
					'mode':'delete',
					'id':newsid,
					'keyword':keyword,
					'page':page
					}

		MODAL(modal_data);
	}

  // close Alerts
  $scope.closeAlert = function(index) {
    $scope.mainalerts.splice(index, 1);
  }

  // MODAL
  var MODAL = function(data) {
    var modalInstance = $modal.open({
      templateUrl: data.templateUrl,
      controller: actionCtrl,
      size: data.size,
      resolve: {
        asset: function() {
          return data
        }
      }
    }).result.then(function(data){ // if close not dismiss
        console.log(data)
        $scope.mainalerts[0] = {type:data.type, msg:data.msg};
        $location.hash('top');
        anchorSmoothScroll.scrollTo('top');

        //Hide after 7 seconds
        $timeout(function() {
          $scope.mainalerts.splice(0, 1);
        }, 7000)
    });
  }

  var actionCtrl = function($scope, $modalInstance, $timeout, $state, asset) {
    $scope.mdl = asset;
    $scope.ok = function(mode) {
      if(mode == 'update') {
        $timeout(function() {
          $state.go('updatenews', {newsid:asset.id});
        }, 0);
        $modalInstance.dismiss('cancel');
      } else if (mode == 'delete') {
        NewsFactory.deletenews(asset.id, function(data) {
          newsPagination(asset.page, asset.keyword);
          $modalInstance.close(data);
        })
      }
    }

    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    }
  }
});
