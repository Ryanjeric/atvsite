app.controller("categorytagsCtrl", function($scope, $modal, $timeout, Config, NewsFactory) {

	var notificationCTRL = function($scope, element, $modalInstance) {
		$scope.message = element.msg;
		$scope.type = element.type;
		$scope.okay= function() {
			$modalInstance.close();
		}
	}

	// =========================================================>>>>>>>> CATEGORY
	$scope.categoryalerts = [];
	$scope.closecategoryAlert = function(index) {
		$scope.categoryalerts.splice(index,1);
	}
	$scope.newcategory= function(){
		var modalInstance = $modal.open({
			templateUrl: 'saveone',
			controller: newcategoryCTRL,
		}).result.then(function(data){
			NewsFactory.addnewscategory(data.newcategory, function(data) {
				$scope.categoryalerts[0] = {type:data.type, msg:data.msg};
				$timeout(function(){
					$scope.categoryalerts.splice(0,1);
				},7000)
				paginate_categorylist(num_c, off_c, keyword_c);
			});
		});
	}
	var newcategoryCTRL = function($scope, $modalInstance) {
		$scope.catalerts = [];
		$scope.tpl_title = "Add News Category";
		$scope.placeholder = "Category Name";
		$scope.tru = "Save";
		$scope.fols = "Cancel";

		$scope.cancel = function() {
			$modalInstance.dismiss();
		}
		$scope.ok = function(asset) {
			$modalInstance.close({newcategory:asset});
		}
	}
	$scope.updatecategory = function(id, oldname, newname,index) {
		var asset = {id:id, name:newname};
		NewsFactory.updatecategory(asset, function(data) {
			var modalInstance = $modal.open({
				templateUrl: 'notification2',
				controller: notificationCTRL,
				resolve: {
					element: function() {
						var element = {'msg' : data.msg, 'type' : data.type };
						return element
					}
				}
			});
			if(data.type=='danger') {
				$scope.categorylist[index].categoryname = oldname;
			}
		})
	}

	var num_c = 10;
	var off_c = 1;
	var keyword_c = null;

	var paginate_categorylist = function(num, off,keyword) {
		$scope.categoryspinner = true;
		NewsFactory.paginate_categorylist(num,off,keyword,function(data){
			$scope.categorylist = data.list;
			$scope.maxSize_category = 5;
			$scope.TotalItems_category = data.total_items;
			$scope.CurrentPage_category = data.index;
			$scope.categoryspinner = false;
		})
	}
	paginate_categorylist(num_c, off_c, keyword_c);

	$scope.setPage_category = function (off) {
		// var searchito = $scope.searchtext; bukas na to
		paginate_categorylist(num_c, off, keyword_c);
	};
	$scope.search_category = function (keyword) {
		if(keyword != '') {
			paginate_categorylist(num_c, off_c, keyword);
		} else {
			paginate_categorylist(num_c, off_c, keyword_c);
		}

	}

	$scope.removecategory = function(categoryname, id, keyword) {
		var modalInstance = $modal.open({
			templateUrl: 'deletecategory',
			size:'lg',
			controller: deletecategoryCTRL,
			resolve: {
					element: function() {
						var element = {
							'msg' : 'Are you sure you want to remove the "'+categoryname+'" category?',
							'type' : 'info',
							'categoryid':id,
							'keyword' : keyword
						};
						return element
					}
				}
		});
	}

	var deletecategoryCTRL = function($scope, $modalInstance, element) {
		$scope.delcat_alerts = [];
		$scope.delcat_closeAlert = function(index) {
			$scope.delcat_alerts.splice(index, 1);
		}

		$scope.del_maxSize = 5;
		var page = 1;

		$scope.hasNews = true;
		var attempt = function(page) {
			NewsFactory.attemptdeletecategory(page, element.categoryid, function(data) {
				if(data.totalitems == 0) {
					$scope.hasNews = false;
					console.log("false")
				} else {
					$scope.hasNews = true;
					$scope.editablenews = data.editable_news;
					$scope.categories = data.categorylist;
					$scope.del_TotalItems = data.totalitems;
					$scope.del_CurrentPage = data.index;
					console.log(data)
				}
			});
		}
		attempt(page);
		$scope.del_setPage = function(page) {
			attempt(page);
		}

		$scope.newsupdatecategory = function(news) {
			if(news.categories != undefined) {
				NewsFactory.newsupdatecategory(news, function(data){
					$scope.delcat_alerts[0] = { type:data.type, msg:data.msg };
					$timeout(function(){
						$scope.delcat_alerts.splice(0, 1);
					},7000)
					attempt($scope.del_CurrentPage);
					console.log(data);
				})
			}
		}

		$scope.ok = function() {
				$modalInstance.dismiss();
				NewsFactory.removecategory(element.categoryid, function(data) {
					var modalInstance = $modal.open({
						templateUrl: 'notification2',
						controller: notificationCTRL,
						resolve: {
							element: function() {
								var element = {'msg' : data.msg, 'type' : data.type };
								return element
							}
						}
					});
					paginate_categorylist(num_c, off_c, element.keyword);
				});
		}
		$scope.cancel = function() {
				$modalInstance.dismiss();
		}
	}

	// =========================================================>>>>>>>> TAGS
	$scope.tagalerts = [];
	$scope.closetagAlert = function(index) {
		$scope.tagalerts.splice(index,1);
	}
	$scope.newtag= function(){
		var modalInstance = $modal.open({
			templateUrl: 'saveone',
			controller: newtagCTRL,
		}).result.then(function(data){ // if close not dismiss
				NewsFactory.addnewstag(data.newtag, function(data) {
					$scope.tagalerts[0] = {type:data.type, msg:data.msg};
					$timeout( function(){
						$scope.tagalerts.splice(0,1);
					},7000);
					paginate_taglist(num_t, off_t, keyword_t);
				});

		});
	}
	var newtagCTRL = function($scope, $modalInstance, NewsFactory) {
		$scope.catalerts = [];
		$scope.tpl_title = "Add News Tag";
		$scope.placeholder = "Tag Name";
		$scope.tru = "Save";
		$scope.fols = "Cancel";

		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		}
		$scope.ok = function(asset) {
			$modalInstance.close({newtag:asset});
		}
	}

	$scope.updatetag = function(id, oldname, newname,index) {
		var asset = {id:id, name:newname};
		NewsFactory.updatetag(asset, function(data) {
			var modalInstance = $modal.open({
				templateUrl: 'notification2',
				controller: notificationCTRL,
				resolve: {
					element: function() {
						var element = {'msg' : data.msg, 'type' : data.type };
						return element
					}
				}
			});
			if(data.type=='danger') {
				$scope.tagslist[index].tagname = oldname;
			}
		})
	}

	var num_t = 10;
	var off_t = 1;
	var keyword_t = null;

	var paginate_taglist = function(num, off,keyword) {
		$scope.tagspinner = true;
		NewsFactory.paginate_taglist(num,off,keyword,function(data){
			$scope.tagslist = data.list;
			$scope.maxSize_tag = 5;
			$scope.TotalItems_tag = data.total_items;
			$scope.CurrentPage_tag = data.index;
			$scope.tagspinner = false;
		})
	}
	paginate_taglist(num_t, off_t, keyword_t);

	$scope.setPage_tag = function (off) {
		// var searchito = $scope.searchtext; bukas na to
		paginate_taglist(num_c, off, keyword_c);
	};
	$scope.search_tag = function (keyword) {
		if(keyword != '') {
			paginate_taglist(num_t, off_t, keyword);
		} else {
			paginate_taglist(num_t, off_t, keyword_c);
		}
	}

	$scope.removetag = function(tagname, id, page, keyword) {
		var modalInstance = $modal.open({
			templateUrl: 'deletetag',
			size:'lg',
			controller: deletetagCTRL,
			resolve: {
					element: function() {
						var element = {
							'msg' : 'Are you sure you want to remove the "'+tagname+'" tag?',
							'type' : 'info',
							'tagid':id,
							'page': page,
							'keyword' : keyword
						};
						return element
					}
				}
		});
	}

	//CONTROLLER OF DELETING A TAG
	var deletetagCTRL = function($scope, $modalInstance, element) {
		$scope.deltag_alerts = [];
		$scope.deltag_closeAlert = function(index) {
			$scope.deltag_alerts.splice(index, 1);
		}

		$scope.del_maxSize = 5;
		var page = 1;

		$scope.hasNews = true;
		var attempt = function(page) {
			NewsFactory.attemptdeletetag(page, element.tagid, function(data) {
				if(data.totalitems == 0) {
					$scope.hasNews = false;
					console.log("false")
				} else {
					$scope.hasNews = true;
					$scope.editablenews = data.editable_news;

					var tagnames = [];
					data.taglist.map(function(val) {
						tagnames.push(val.tagname);
					})
					$scope.tagnames = tagnames;

					$scope.del_TotalItems = data.totalitems;
					$scope.del_CurrentPage = data.index;
					console.log(data)
				}
			});
		}
		attempt(page);
		$scope.del_setPage = function(page) {
			attempt(page);
		}

		$scope.newsupdatetag = function(news) {
			if(news.tags != undefined) {
				NewsFactory.newsupdatetag(news, function(data){
					$scope.deltag_alerts[0] = { type:data.type, msg:data.msg };
					$timeout(function(){
						$scope.deltag_alerts.splice(0, 1);
					},7000)
					attempt($scope.del_CurrentPage);
					console.log(data);
				})
			}
		}

		$scope.ok = function() {
			$modalInstance.dismiss();
			NewsFactory.removetag(element.tagid, function(data) {
				var modalInstance = $modal.open({
					templateUrl: 'notification2',
					controller: notificationCTRL,
					resolve: {
						element: function() {
							var element = {'msg' : data.msg, 'type' : data.type };
							return element
						}
					}
				});
				paginate_taglist(num_t, element.page, element.keyword);
			});
		}
		$scope.cancel = function() {
				$modalInstance.dismiss();
		}
	}

});
