'use strict';

app.controller("newauthorCtrl", function($scope, Config, $modal, $state, $location, anchorSmoothScroll, Upload, NewsFactory, moment) {
	$scope.datenow = moment().format('YYYY-MM-DD');
	$scope.master = {
		name : "",
		location: "",
		occupation: "",
		since: $scope.datenow,
		about: "",
		photo: ""
	}
	$scope.mainalerts = [];
	$scope.modalalerts = [];
	$scope.author = {};
	$scope.hasChooseImg = false;

	$scope.media = function(size) {
		var data = {'templateUrl':'authorsgallery',
					'size':size,
					'controller':authorsgalleryCtrl,
					'imgid': $scope.author.imgid}
		MODAL(data);
	}

	$scope.saveAuthor = function(author) {
		NewsFactory.saveauthor(author, function(data){
			$scope.author = angular.copy($scope.master, $scope.author);
			$scope.authorFORM.$setPristine(true);
			$scope.hasChooseImg = false;
			$scope.mainalerts[0] = {type:data.type, msg:data.msg};

			var modal_data = {'templateUrl':'neworlist',
					'size':'md',
					'controller':neworlistCtrl,
					'type' : data.type,
					'heading':'Author Notification',
					'body':data.msg,
					'cancel':'Create Another',
					'ok':'Authors List'}
			MODAL(modal_data);
		})
	}

	$scope.removephoto = function() {
		$scope.author.photo = "";
		$scope.hasChooseImg = false;
	}

	var AUTHORIMAGES = function() {
		NewsFactory.authorimages(function(data){
			$scope.author_images = data;
		})
	}

	//ALERTS CLOSE FUNCTION
	$scope.closeAlert = function(type, index) {
		if(type == 'main') {
			$scope.mainalerts.splice(index, 1);
		} else if (type == 'modal') {
			$scope.modalalerts.splice(index, 1);
		}
	}

	// MODAL
	var MODAL = function(data) {
		var modalInstance = $modal.open({
			templateUrl: data.templateUrl,
			controller: data.controller,
			size: data.size,
			resolve: {
				asset: function() {
					return data
				}
			}
		}).result.then(function(data){ // if close not dismiss

			if (data.Ctrl == 'authorsgallery') {
				if(data.imgid == undefined) {
					if($scop.author.photo != undefined) {
						$scope.hasChooseImg = true;
						$scope.author.photo = $scope.author.photo;
						$scope.author.imgid = $scope.author.imgid;
					}
				} else {
					$scope.hasChooseImg = true;
					$scope.author.photo = data.photo; // data from OK button
					$scope.author.imgid = data.imgid;
				}

			}
		});
	}

	var neworlistCtrl = function($scope, $modalInstance, $timeout, asset) {
		$scope.mdl = asset;
		$scope.ok = function(statego) {
			$timeout(function() {
				$state.go('manageauthors');
			}, 0);
			$modalInstance.dismiss('cancel');
		}
		$scope.cancel = function() {
			//scroll to top
			$location.hash('top');
			anchorSmoothScroll.scrollTo('top');
			$modalInstance.dismiss('cancel');
		}
	};

	var authorsgalleryCtrl = function($scope, $modalInstance, asset, NewsFactory, Config) {
		$scope.amazonlink = Config.amazonlink + "/uploads/authors/";
		$scope.imageloader = false;
		$scope.imagecontent = true;
		$scope.greencheck = false;

		$scope.modalalerts = [];
		$scope.closeAlert = function(type, index) {
			if(type == 'modal') {
				$scope.modalalerts.splice(index, 1);
			}
		}

		$scope.chosenimage = function(imageid, imagename) {
			$scope.greencheck = true;
			$scope.chosenimagename = imagename; //in ok
			$scope.chosenimageid = imageid;
			angular.element(".chosenimg").hide(); // remove the green check button
			angular.element(".authors").removeClass("chosen"); //remove the green border
			angular.element("#"+imageid).show(); // display the new green check button
			angular.element("#author"+imageid).addClass("chosen"); // display the new green border
		}

		var AUTHORIMAGES = function() {
			NewsFactory.authorimages(function(data){
				$scope.author_images = data;
			})
		}
		AUTHORIMAGES();

		$scope.upload = function(files) {
			var filename;
			var filecount = 0;
			if (files && files.length) {
			    $scope.imageloader=true;
			    $scope.imagecontent=false;
			    for (var i = 0; i < files.length; i++) {
			      var file = files[i];

			      	if (file.size >= 2000000) {
			        	$scope.modalalerts.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
								$scope.imageloader=false;
								$scope.imagecontent=true;
			      	} else {
				        var promises;
				        var fileExtension = '.' + file.name.split('.').pop();
				        // rename the file with a sufficiently random value and add the file extension back
				        var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + i + fileExtension;

				        promises = Upload.upload({
				          	url:Config.amazonlink, //S3 upload url including bucket name
		                   	method: 'POST',
		                   	transformRequest: function (data, headersGetter) {
			                    //Headers change here
			                    var headers = headersGetter();
			                    delete headers['Authorization'];
			                    return data;
		                  	},
		                  	fields : {
		                      	key: 'uploads/authors/' + renamedFile, // the key to store the file on S3, could be file name or customized
		                      	AWSAccessKeyId: Config.AWSAccessKeyId,
		                      	acl: 'private', // sets the access to the uploaded file in the bucket: private or public
		                      	policy: Config.policy, // base64-encoded json policy (see article below)
		                      	signature: Config.signature, // base64-encoded signature based on policy string (see article below)
		                      	"Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
		                    },
		                    file: file
		                })
						promises.then(function(data){
						  	filecount = filecount + 1;
						  	filename = data.config.file.name;
						  	var fileout = {
						    	'imgfilename' : renamedFile,
						    	'category' : 'author'
						  	};
						  	NewsFactory.upload_author(fileout, function(data) {
						  		AUTHORIMAGES();
						  		$scope.imageloader=false;
									$scope.imagecontent=true;
						  	})
						}); // PROMISES THEN end
					} // ELSE end
				} // FOR end
			} // IF end
		} // Upload end

		$scope.author = {};
		$scope.ok = function(chosenimageid, chosenimagename) {
			if(chosenimagename != undefined) {
				var imgsrc = Config.amazonlink + "/uploads/authors/" + chosenimagename;
				angular.element("#authorphoto").attr('src',imgsrc);
			}

			var data = {Ctrl:'authorsgallery', photo:chosenimagename, imgid: chosenimageid};
			$modalInstance.close(data);
		}
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		}

		$scope.deleteimg = function(imageid) {
			var deletedata = {
				keyid				: imageid,
				msg					: "Do you want to delete this image?",
				mode				: 'deleteimg'
			};

			var modalInstance = $modal.open({
				templateUrl: "yesno",
				controller: yesnoCTRL,
				size: "md",
				resolve: {
					asset: function() {
						return deletedata
					}
				}
			}).result.then(function(data) { // if close not dismiss
				if(data.mode == 'deleteimg') {
					NewsFactory.deleteauthorimage(data.keyid, function(data){
						$scope.modalalerts.push({type: data.type, msg: data.msg});
						$scope.greencheck = false;
						angular.element(".chosenimg").hide(); // remove the green check button
						AUTHORIMAGES();
					});
				}
			});
		}
	}

	var yesnoCTRL = function($scope, $modalInstance, asset) {
		$scope.mdl = asset;
		$scope.cancel = function() {
			$modalInstance.dismiss();
		}
		$scope.ok = function() {
			$modalInstance.close({mode:asset.mode, keyid: asset.keyid});
		}
	}

	//DATE PICKER
	$scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      class: 'datepicker'
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
})

app.controller("manageauthorsCtrl", function($scope, $state, $modal, $timeout, $location, Config, anchorSmoothScroll,  NewsFactory, moment) {
	$scope.mainalerts = [];

	var items = 10;
	var page = 1;
	var keyword = null;
	$scope.maxSize = 5;

	var authorsPagination = function(page, keyword) {
		var asset = {items:items, page:page, keyword:keyword};
		NewsFactory.authorspagination(asset, function(data) {

			angular.forEach(data.authors, function(value, key){
				data.authors[key].since = moment(value.since).format("MMMM DD, YYYY");
			})

			$scope.authors = data.authors;
			$scope.TotalItems = data.TotalItems;
			$scope.CurrentPage = data.index;
			console.log(data)
		});
	}
	authorsPagination(page, keyword);

	$scope.setPage = function(page, keyword) {
		authorsPagination(page, keyword);
	}
	$scope.search = function (newkeyword) {
		authorsPagination(page, newkeyword);
	}
	$scope.clearsearch = function() {
		authorsPagination(page, keyword);
		$scope.keyword = "";
	}

	$scope.update = function(authorid) {
		var modal_data = {'templateUrl':'updatedelete',
					'size':'md',
					'controller':actionCtrl,
					'type' : 'info',
					'heading':'Action Nofication',
					'body':'Do you want to update the Author?',
					'cancel':'Cancel',
					'ok':'Yes',
					'mode':'update',
					'id':authorid
					}
		MODAL(modal_data);
	}
	$scope.delete = function(authorid, keyword, page) {
		var modal_data = {'templateUrl':'deleteauthor',
					'size':'lg',
					'controller':deleteCtrl,
					'mode':'delete',
					'id':authorid,
					'keyword':keyword,
					'page':page
					}
		MODAL(modal_data);
	}

	// close Alerts
	$scope.closeAlert = function(index) {
		$scope.mainalerts.splice(index, 1);
	}

	// MODAL
	var MODAL = function(data) {
		var modalInstance = $modal.open({
			templateUrl: data.templateUrl,
			controller: data.controller,
			size: data.size,
			resolve: {
				asset: function() {
					return data
				}
			}
		}).result.then(function(data){ // if close not dismiss
				$scope.mainalerts[0] = {type:data.type, msg:data.msg};
				$location.hash('top');
				anchorSmoothScroll.scrollTo('top');

				//Hide after 7 seconds
				$timeout(function() {
					$scope.mainalerts.splice(0, 1);
				}, 7000)
		});
	}

	var actionCtrl = function($scope, $modalInstance, $timeout, $state, asset) {
		$scope.mdl = asset;
		$scope.ok = function(mode) {
			if(mode == 'update') {
				$timeout(function() {
					$state.go('updateauthor', {authorid:asset.id});
				}, 0);
				$modalInstance.dismiss('cancel');
			}
			// else if (mode == 'delete') {
			// 	NewsFactory.deleteauthor(asset.id, function(data) {
			// 		authorsPagination(asset.page, asset.keyword);
			// 		$modalInstance.close(data);
			// 	})
			// }
		}
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		}
	}

	var deleteCtrl = function($scope, $modalInstance, $timeout, asset) {
		$scope.authordeletealerts = [];
		$scope.authdelcloseAlert = function(index) {
			$scope.authordeletealerts.splice(index, 1);
		}

		$scope.an_maxSize = 5;
		var page = 1;

		var attempt = function(page) {
			NewsFactory.attemptdeleteauthor(page, asset.id, function(data) {
				if(data.authornews == undefined) {
					$scope.hasNews = false;
				} else {
					$scope.hasNews = true;
					$scope.mynews = data.authornews;
					$scope.authors = data.authorlist;
					$scope.an_TotalItems = data.totalitems;
					$scope.an_CurrentPage = data.index;
				}
			});
		}
		attempt(page);

		$scope.an_setPage = function(page) {
			attempt(page);
		}

		$scope.updatenewsauthor = function(news) {
			NewsFactory.newsupdateauthor(news, function(data){
				$scope.authordeletealerts[0] = { type:data.type, msg:data.msg };
				$timeout(function(){
					$scope.authordeletealerts.splice(0, 1);
				},7000)
				attempt(page);
			})
		}

		$scope.ok = function() {
			NewsFactory.deleteauthor(asset.id, function(data) {
				authorsPagination(asset.page, asset.keyword);
				$modalInstance.close(data);
			});
		}
		$scope.cancel = function () {
			$modalInstance.dismiss();
		}
	}

}); //manageauthorsCtrl END

app.controller("updateauthorCtrl", function($scope, $state, $stateParams, $modal, $timeout, $location, Config, Upload, anchorSmoothScroll,  NewsFactory) {
	var authorid = $stateParams.authorid;
	$scope.mainalerts = [];

	var loadauthor = function() {
		NewsFactory.loadauthor(authorid, function(data){
			$scope.author = data.author;
			if(data.author.photo == null || data.author.photo == "") {
				$scope.hasChooseImg = false;
			} else {
				$scope.hasChooseImg = true;
				$scope.image = Config.amazonlink + "/uploads/authors/" + data.author.photo;
			}
		})
	}
	loadauthor();

	$scope.updateAuthor = function(author) {
		author['id'] = authorid;
		NewsFactory.updateauthor(author, function(data){
			$scope.mainalerts[0] = {type:data.type, msg:data.msg};
			loadauthor();
			var modal_data = {'templateUrl':'stayorlist',
					'size':'md',
					'controller':stayorlistCtrl,
					'type' : data.type,
					'heading':'Author Notification',
					'body':data.msg,
					'cancel':'Review/Update',
					'ok':'Authors List'}
			MODAL(modal_data);
		})
	}

	$scope.ensureUnique = function(authorname) {
		var author = {name:authorname, id:authorid}
		NewsFactory.ensureuniqueauthorname(author, function(data) {
			console.log(data)
			if(data.unique == false) {
				$scope.authorFORM.name.$setValidity("unique",false);
			} else {
				$scope.authorFORM.name.$setValidity("unique",true);
			}
		})
	}

	$scope.media = function(size) {
		var data = {'templateUrl':'authorsgallery',
					'size':size,
					'controller':authorsgalleryCtrl,
					'imgid': $scope.author.imgid}
		MODAL(data);
	}

	$scope.removephoto = function() {
		$scope.author.photo = "";
		$scope.hasChooseImg = false;
	}

	// close Alerts
	$scope.closeAlert = function(index) {
		$scope.mainalerts.splice(index, 1);
	}

	// MODAL
	var MODAL = function(data) {
		var modalInstance = $modal.open({
			templateUrl: data.templateUrl,
			controller: data.controller,
			size: data.size,
			resolve: {
				asset: function() {
					return data
				}
			}
		}).result.then(function(data){ // if close not dismiss
			if (data.Ctrl == 'authorsgallery') {
				$scope.hasChooseImg = true;
				$scope.author.photo = data.photo; // data from OK button
				$scope.author.imgid = data.imgid;
			} else if (data.Ctrl == 'stayorlist') {
				//scroll to top
				$location.hash('top');
				anchorSmoothScroll.scrollTo('top');

				$timeout( function() {
						$scope.mainalerts.splice(0, 1);
				},7000);
			}
		});
	}

	var stayorlistCtrl = function($scope, $modalInstance, $timeout, $state, asset) {
		$scope.mdl = asset;
		$scope.ok = function() {
			$timeout(function() {
				$state.go('manageauthors');
			}, 0);
			$modalInstance.dismiss('cancel');
		}
		$scope.cancel = function() {
			var data = {Ctrl:'stayorlist'};
			$modalInstance.close(data);
		}
	};

	var authorsgalleryCtrl = function($scope, $modalInstance, asset, NewsFactory, Config) {
		$scope.amazonlink = Config.amazonlink + "/uploads/authors/";
		$scope.imageloader = false;
		$scope.imagecontent = true;
		$scope.greencheck = false;

		$scope.modalalerts = [];
		$scope.closeAlert = function(type, index) {
			if(type == 'modal') {
				$scope.modalalerts.splice(index, 1);
			}
		}

		$scope.chosenimage = function(imageid, imagename) {
			$scope.greencheck = true;
			$scope.chosenimagename = imagename; //in ok
			$scope.chosenimageid = imageid;
			angular.element(".chosenimg").hide(); // remove the green check button
			angular.element(".authors").removeClass("chosen"); //remove the green border
			angular.element("#"+imageid).show(); // display the new green check button
			angular.element("#author"+imageid).addClass("chosen"); // display the new green border
		}

		var AUTHORIMAGES = function() {
			NewsFactory.authorimages(function(data){
				$scope.author_images = data;
			})
		}
		AUTHORIMAGES();

		$scope.upload = function(files) {
			var filename;
			var filecount = 0;
			if (files && files.length) {
			    $scope.imageloader=true;
			    $scope.imagecontent=false;
			    for (var i = 0; i < files.length; i++) {
			      var file = files[i];

			      	if (file.size >= 2000000) {
			        	$scope.modalalerts.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
								$scope.imageloader=false;
						    $scope.imagecontent=true;
			      	} else {
				        var promises;
				        var fileExtension = '.' + file.name.split('.').pop();
				        // rename the file with a sufficiently random value and add the file extension back
				        var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + i + fileExtension;

				        promises = Upload.upload({
				          	url:Config.amazonlink, //S3 upload url including bucket name
		                   	method: 'POST',
		                   	transformRequest: function (data, headersGetter) {
			                    //Headers change here
			                    var headers = headersGetter();
			                    delete headers['Authorization'];
			                    return data;
		                  	},
		                  	fields : {
		                      	key: 'uploads/authors/' + renamedFile, // the key to store the file on S3, could be file name or customized
		                      	AWSAccessKeyId: Config.AWSAccessKeyId,
		                      	acl: 'private', // sets the access to the uploaded file in the bucket: private or public
		                      	policy: Config.policy, // base64-encoded json policy (see article below)
		                      	signature: Config.signature, // base64-encoded signature based on policy string (see article below)
		                      	"Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
		                    },
		                    file: file
		                })
						promises.then(function(data){
						  	filecount = filecount + 1;
						  	filename = data.config.file.name;
						  	var fileout = {
						    	'imgfilename' : renamedFile,
						    	'category' : 'author'
						  	};
						  	NewsFactory.upload_author(fileout, function(data) {
						  		AUTHORIMAGES();
						  		$scope.imageloader=false;
								$scope.imagecontent=true;
						  	})
						}); // PROMISES THEN end
					} // ELSE end
				} // FOR end
			} // IF end
		}

		$scope.author = {};
		$scope.ok = function(chosenimageid, chosenimagename) {
			var imgsrc = Config.amazonlink + "/uploads/authors/" + chosenimagename;
			angular.element("#authorphoto").attr('src',imgsrc);

			var data = {Ctrl:'authorsgallery', photo:chosenimagename, imgid: chosenimageid};
			$modalInstance.close(data);
		}
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		}

		$scope.deleteimg = function(imageid) {
			var deletedata = {
				keyid				: imageid,
				msg					: "Do you want to delete this image?",
				mode				: 'deleteimg'
			};

			var modalInstance = $modal.open({
				templateUrl: "yesno",
				controller: yesnoCTRL,
				size: "md",
				resolve: {
					asset: function() {
						return deletedata
					}
				}
			}).result.then(function(data) { // if close not dismiss
				if(data.mode == 'deleteimg') {
					NewsFactory.deleteauthorimage(data.keyid, function(data){
						$scope.modalalerts.push({type: data.type, msg: data.msg});
						$scope.greencheck = false;
						angular.element(".chosenimg").hide(); // remove the green check button
						AUTHORIMAGES();
					});
				}
			});
		}
	}

	var yesnoCTRL = function($scope, $modalInstance, asset) {
		$scope.mdl = asset;
		$scope.cancel = function() {
			$modalInstance.dismiss();
		}
		$scope.ok = function() {
			$modalInstance.close({mode:asset.mode, keyid: asset.keyid});
		}
	}

	//DATE PICKER
	$scope.today = function() {
			$scope.dt = new Date();
		};
		$scope.today();

		$scope.clear = function () {
			$scope.dt = null;
		};

		// Disable weekend selection
		$scope.disabled = function(date, mode) {
			return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
		};

		$scope.toggleMin = function() {
			$scope.minDate = $scope.minDate ? null : new Date();
		};
		$scope.toggleMin();

		$scope.open = function($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened = true;
		};

		$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1,
			class: 'datepicker'
		};

		$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
		$scope.format = $scope.formats[0];
});
