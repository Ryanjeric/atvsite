app.controller("updatenewsCtrl", function($scope, $modal, $state, $stateParams, $location, anchorSmoothScroll, $timeout, $sce, $localStorage, $window, Config, NewsFactory, Upload, moment) {
	$scope.datenow = moment().format('YYYY-MM-DD');
	var newsid = $stateParams.newsid;

	$scope.news = {};
	$scope.disableslugs = true;

	NewsFactory.createnews_resources(function(data){
		$scope.authors = data.authors;
		$scope.categories = data.categories;

		$scope.tagnames = [];
		data.tags.map(function(val){
			$scope.tagnames.push(val.tagname);
		})

	})

	NewsFactory.newsinfo(newsid, function(data){
		$scope.news = data.newsinfo;
		$scope.news.categories = data.categories;
		var tagnames = [];
		data.tags.map(function(val){
			tagnames.push(val.tagname);
		});
		$scope.news.tags = tagnames;

		$scope.featuredtype = data.newsinfo.featuredtype;

		if(data.newsinfo.featuredtype == 'img') {
			var imgsrc = Config.amazonlink + "/uploads/news/" + data.featimage;
			angular.element("#featuredimage").attr('src',imgsrc);
		} else if (data.newsinfo.featuredtype == 'vid') {
			$scope.featuredvid = $sce.trustAsHtml(data.featvideo);
		}
	})

	$scope.ensureuniqueslugs = function(newsslugs) {
		ensureUniqueSlugs(newsslugs);
	}

	var ensureUniqueSlugs = function(newsslugs) {
		NewsFactory.ensureuniqueslugs(newsid, newsslugs, function(data){
			$scope.formnews.slugs.$setValidity("unique", data.isUnique);
		});
	}

	var loadcategories = function() {
		NewsFactory.loadcategories(function(data){
			$scope.categories = data.categories;
		})
	}
	var loadtags = function() {
		NewsFactory.loadtags(function(data){
			$scope.tags = data.tags;
		})
	}

	$scope.test = function(data) {
		console.log(data)
	}

	$scope.onnewstitle = function(title) {
		if(title=="" || title==null) {
			$scope.news.slugs = " ";
		} else {
			var sanitize = title.replace(/[^\w ]+/g,'');
			$scope.news.slugs = angular.lowercase(sanitize.replace(/ +/g,'-'));
			ensureUniqueSlugs($scope.news.slugs);
		}
	}

	$scope.addcategory = function() {
		$scope.tpl_title = "Add News Category";
		$scope.placeholder = "Category Name";
		$scope.tru = "Save";
		$scope.fols = "Cancel";

		var modal_data = {
			templateUrl : "saveone",
			controller	: newcategoryCTRL,
			size				: "md",
			tpl_title		: "Add News Category",
			placeholder	: "Category Name",
			tru					: "Save",
			fols				: "Cancel"
		};
		MODAL(modal_data);
	}

	$scope.addtag = function() {
		var modal_data = {
			templateUrl : "saveone",
			controller	: newtagCTRL,
			size				: "md",
			tpl_title		: "Add News Tag",
			placeholder	: "Tag Name",
			tru					: "Save",
			fols				: "Cancel"
		};
		MODAL(modal_data);
	}

	$scope.media = function(type) {
		if(type == 'content') {
			var modal_data = {
				templateUrl : "medialibrary",
				controller	: medialibraryCTRL,
				size				: "lg",
			};
		} else if (type == 'featured') {
			var modal_data = {
				templateUrl : "featuredmedialibrary",
				controller	: medialibraryCTRL,
				size				: "lg",
			};
		}
		MODAL(modal_data);
	}

	var NEWSIMAGES = function() {
		NewsFactory.newsimages(function(data){
			$scope.images = data;
		})
	}
	NEWSIMAGES();

	//ALERTS
	$scope.categoryalerts = [];
	$scope.tagalerts = [];
	$scope.closeAlert = function(index, type) {
		if(type == 'category') {
			$scope.categoryalerts.splice(0,1);
		} else if (type == 'tag') {
			$scope.tagalerts.splice(0,1);
		} else if (type == 'main') {
			$scope.mainalerts.splice(0,1);
		}
	}

	$scope.modalalerts = [];
	// MODAL
	var MODAL = function(data) {
		var modalInstance = $modal.open({
			templateUrl: data.templateUrl,
			controller: data.controller,
			size: data.size,
			resolve: {
				asset: function() {
					return data
				}
			}
		}).result.then(function(data) { // if close not dismiss
			if(data.ctrl == "newcategory") {
				NewsFactory.addnewscategory(data.newcategory, function(data) {
					$scope.categoryalerts[0] = {type:data.type, msg:data.msg};
					$timeout(function(){
						$scope.categoryalerts.splice(0,1);
					},7000);
					loadcategories(); //load newlist
				});
			} else if (data.ctrl == "newtag") {
				NewsFactory.addnewstag(data.newtag, function(data) {
					$scope.tagalerts[0] = {type:data.type, msg:data.msg};
					$timeout(function(){
						$scope.tagalerts.splice(0,1);
					},7000);
					loadtags(); //load newlist
				});
			} else if (data.ctrl == "medialibrary") {
				$scope.featuredtype = data.featuredtype;

				if(data.featured == undefined) {
					if($scope.news.featuredtype != undefined) {
						$scope.hasChooseFeatured = true;
						$scope.news.featuredtype = $scope.news.featuredtype;
						$scope.news.featvideo = $scope.news.featvideo;
						$scope.news.featimage = $scope.news.featimage;
					}
				} else {
					$scope.hasChooseFeatured = true;
					$scope.news.featuredtype = data.featuredtype
					$scope.news.featimage = data.featuredid;
					$scope.news.featvideo = undefined;
				}

				if(data.featuredtype == 'vid') {
					$scope.featuredvid = $sce.trustAsHtml(data.featured);
					$scope.news.featvideo = data.featuredid;
					$scope.news.featimage = undefined;
				}

			} else if (data.ctrl == 'stayorlist') {
				//scroll to top
				$location.hash('top');
				anchorSmoothScroll.scrollTo('top');

				$timeout( function() {
						$scope.mainalerts.splice(0, 1);
				},7000);
			}
		}); //END OF RESULT THEN
	}

	var medialibraryCTRL = function($scope, $modalInstance, Config, asset) {
		$scope.amazonlink = Config.amazonlink + "/uploads/news/";

		$scope.modalalerts = [];
		$scope.closeAlerts = function(index) {
			$scope.modalalerts.splice(index, 1);
		}

		$scope.cancel = function() {
			$modalInstance.dismiss();
		}
		$scope.ok = function(featuredid, featured, featuredtype) {
			if(featured != undefined) {
				if(featuredtype == 'img') {
					var imgsrc = Config.amazonlink + "/uploads/news/" + featured;
					angular.element("#featuredimage").attr('src',imgsrc);
				}
			}
			$modalInstance.close({ctrl:'medialibrary', featuredid:featuredid, featured:featured, featuredtype:featuredtype});
		}

		$scope.urlimg = function(url) {
			var urlimgdata = {
				copy				: url,
				title			: "News Image URL"
			};

			var modalInstance = $modal.open({
				templateUrl: "copypaste",
				controller: copypasteCTRL,
				size: "lg",
				resolve: {
					asset: function() {
						return urlimgdata
					}
				}
			})
		}

		$scope.deleteimg = function(imgid) {
				var deletedata = {
					keyid				: imgid,
					msg					: "Do you want to delete this image?",
					mode				: 'deleteimg'
				};

				var modalInstance = $modal.open({
					templateUrl: "yesno",
					controller: yesnoCTRL,
					size: "md",
					resolve: {
						asset: function() {
							return deletedata
						}
					}
				}).result.then(function(data) { // if close not dismiss
					if(data.mode == 'deleteimg') {
						NewsFactory.deletenewsimage(data.keyid, function(data){
							$scope.modalalerts[0] = {type: data.type, msg: data.msg};
							NEWSIMAGES();
						});
					}
				});
		} //deleteimg ENDS

		$scope.video = {};
		$scope.saveembed = function (embed) {
			if(embed){
				var x = embed.match(/<iframe width="(.*?)" height="(.*?)" src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
				if(x==null){
					$scope.modalalerts[0] = {type: "danger", msg: "Invalid youtube embed code!"};
						$scope.video.embed = "";
				} else {
						var asset = {embed:embed, category:'news'};
						NewsFactory.saveembed(asset,function(data){

						$scope.modalalerts[0] = {type: data.type, msg: data.msg};
						$scope.video.embed = "";
						NEWSVIDEOS();
					});
				}
			}
		}

		$scope.embedvid = function(embed) {
			var embedviddata = {
				copy			: embed,
				title			: "Youtube video embed"
			};

			var modalInstance = $modal.open({
				templateUrl: "copypaste",
				controller: copypasteCTRL,
				size: "lg",
				resolve: {
					asset: function() {
						return embedviddata
					}
				}
			})
		}

		$scope.deletevid = function(vidid) {
			var deletedata = {
				keyid				: vidid,
				msg					: "Do you want to delete this video?",
				mode				: 'deletevid'
			};

			var modalInstance = $modal.open({
				templateUrl: "yesno",
				controller: yesnoCTRL,
				size: "md",
				resolve: {
					asset: function() {
						return deletedata
					}
				}
			}).result.then(function(data) { // if close not dismiss
				if(data.mode == 'deletevid') {
					NewsFactory.deletenewsvideo(data.keyid, function(data) {
						$scope.modalalerts[0] = {type: data.type, msg: data.msg};
						NEWSVIDEOS();
					});
				}
			});
		}

		$scope.imageloader=false;
		$scope.imagecontent=true;
		$scope.upload = function(files) {
			var filename;
			var filecount = 0;
			if (files && files.length) {
			    $scope.imageloader=true;
			    $scope.imagecontent=false;
			    for (var i = 0; i < files.length; i++) {
			      var file = files[i];

			      	if (file.size >= 2000000) { //2mb max
			        	$scope.modalalerts[0] = {type: 'danger', msg: 'File ' + file.name + ' is too big'};
								$scope.imageloader=false;
								$scope.imagecontent=true;
			      	} else {
				        var promises;
				        var fileExtension = '.' + file.name.split('.').pop();
				        // rename the file with a sufficiently random value and add the file extension back
				        var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + i + fileExtension;

				        promises = Upload.upload({
				          	url:Config.amazonlink, //S3 upload url including bucket name
		                   	method: 'POST',
		                   	transformRequest: function (data, headersGetter) {
			                    //Headers change here
			                    var headers = headersGetter();
			                    delete headers['Authorization'];
			                    return data;
		                  	},
		                  	fields : {
		                      	key: 'uploads/news/' + file.name, // the key to store the file on S3, could be file name or customized
		                      	AWSAccessKeyId: Config.AWSAccessKeyId,
		                      	acl: 'private', // sets the access to the uploaded file in the bucket: private or public
		                      	policy: Config.policy, // base64-encoded json policy (see article below)
		                      	signature: Config.signature, // base64-encoded signature based on policy string (see article below)
		                      	"Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
		                    },
		                    file: file
		                })
						promises.then(function(data){
						  	filecount = filecount + 1;
						  	filename = data.config.file.name;
						  	var fileout = {
						    	// 'imgfilename' : renamedFile,
									'imgfilename' : filename,
						    	'category' : 'news'
						  	};
						  	NewsFactory.upload_news(fileout, function(data) {
						  		$scope.imageloader=false;
									$scope.imagecontent=true;
									$scope.modalalerts[0] = {type: data.type, msg: data.msg};
									NEWSIMAGES();
						  	})
						}); // PROMISES THEN end
					} // ELSE end
				} // FOR end
			} // IF end
		} // Upload end

		var NEWSIMAGES = function() {
			NewsFactory.newsimages(function(data){
				$scope.images = data;
			})
		}
		NEWSIMAGES();

		//extracting the youtube ID from embed
		var returnYoutubeThumb = function(item) {
			var x= '';
			var thumb = {};
			if(item){
				var newdata = item;
				var x;
				x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
				thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
				thumb['yid'] = x[1];
				return thumb;
			} else {
				return x;
			}
		}
		var NEWSVIDEOS = function() {
			NewsFactory.newsvideos(function(data){
				for(var x in data){
					var newd = returnYoutubeThumb(data[x].embed);
					data[x].videourl = newd.url;
					data[x].youtubeid = newd.yid;
				}
				$scope.videos = data;
			})
		}
		NEWSVIDEOS();

		$scope.chosen = function(keyid, keyname, type) {
			$scope.greencheck = true;

			$scope.featuredtype = type; // in ok
			$scope.featured = keyname; // in ok
			$scope.featuredid = keyid;

			angular.element(".medias").removeClass("chosen"); //remove the green border
			angular.element("#featured"+keyid).addClass("chosen"); // display the new green border

			angular.element(".chosenfeat").hide(); // remove the green check button
			angular.element("#"+keyid).show(); // display the new green check button
		}
	} //medialibraryCtrl ENDS

	var newcategoryCTRL = function($scope, $modalInstance, asset) {
		$scope.tpl_title = asset.tpl_title;
		$scope.placeholder = asset.placeholder;
		$scope.tru = asset.tru;
		$scope.fols = asset.fols;

		$scope.cancel = function() {
			$modalInstance.dismiss();
		}
		$scope.ok = function(asset) {
			$modalInstance.close({ctrl:"newcategory", newcategory:asset});
		}
	}

	var newtagCTRL = function($scope, $modalInstance, asset) {
		$scope.tpl_title = asset.tpl_title;
		$scope.placeholder = asset.placeholder;
		$scope.tru = asset.tru;
		$scope.fols = asset.fols;

		$scope.cancel = function() {
			$modalInstance.dismiss();
		}
		$scope.ok = function(asset) {
			$modalInstance.close({ctrl:"newtag", newtag:asset});
		}
	}

	var yesnoCTRL = function($scope, $modalInstance, asset) {
		$scope.mdl = asset;
		$scope.cancel = function() {
			$modalInstance.dismiss();
		}
		$scope.ok = function() {
			$modalInstance.close({mode:asset.mode, keyid: asset.keyid});
		}
	}

	var copypasteCTRL = function($scope, $modalInstance, asset) {
		$scope.mdl = asset;
		$scope.cancel = function() {
			$modalInstance.dismiss();
		}
	}

	var stayorlistCtrl = function($scope, $modalInstance, $timeout, $state, asset) {
		$scope.mdl = asset;
		$scope.ok = function() {
			$timeout(function() {
				$state.go('managenews');
			}, 0);
			$modalInstance.dismiss('cancel');
		}
		$scope.cancel = function() {
			var data = {ctrl:'stayorlist'};
			$modalInstance.close(data);
		}
	};

	//DATE PICKER
	$scope.today = function() {
		$scope.dt = new Date();
	};
	$scope.today();

	$scope.clear = function () {
		$scope.dt = null;
	};

	// Disable weekend selection
	$scope.disabled = function(date, mode) {
		return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
	};

	$scope.toggleMin = function() {
		$scope.minDate = $scope.minDate ? null : new Date();
	};
	$scope.toggleMin();

	$scope.open = function($event) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope.opened = true;
	};

	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1,
		class: 'datepicker'
	};

	$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
	$scope.format = $scope.formats[0];
	//DATE PICKER ends

 	// =====================================================================================

	// SAVING
	$scope.mainalerts = [];
	$scope.saveNews = function(news, status) {
		news['status'] = status;
		news['id'] = newsid;
		NewsFactory.savenewsupdate(news, function(data){
			console.log(data);
			$scope.mainalerts[0] = {type:data.type, msg:data.msg};
			$timeout(function(){
					$scope.mainalerts.splice(0, 1);
			},7000);

			var modal_data = {'templateUrl':'stayorlist',
					'size':'md',
					'controller':stayorlistCtrl,
					'type' : data.type,
					'heading':'News Notification',
					'body':data.msg,
					'cancel':'Review/Update',
					'ok':'News List'}
			MODAL(modal_data);
		});
	}

	$scope.preview = function(news) {
		$localStorage.atvnewspreview = news;
		$window.open('/blog/preview', 'onepageonly');
	}

})
