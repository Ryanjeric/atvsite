'use strict';

app.controller('Rentalhours', function($scope, $state, Upload ,$q, $http, Config, $modal, Rentalhours, $timeout, AtvService){
    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

     var notificationCTRL = function($scope, element, $modalInstance) {
        $scope.message = element.msg;
        $scope.type = element.type;
        $scope.okay= function() {
            $modalInstance.close();
        };
    };

    $scope.addrentalhours= function() {

        AtvService.modal('newrentalhr.html', 'md', function($modalInstance, $scope) {
            $scope.asset = {};
            $scope.sessionalerts = [];

            $scope.asset.start = moment().format('YYYY-MM-DD') + ' 08:00:00';
            $scope.hstep = 1; //[1, 2, 3]
            $scope.mstep = 5; //[1, 5, 10, 15, 25, 30]

            $scope.sessioncloseAlert = function() {
                $scope.sessionalerts.splice(0, 1);
            };

            $scope.catalerts = [];
            $scope.tpl_title = "Add Rental Hours";

            Rentalhours.sessions(function(data) {
                $scope.sessions = data.sessions;
            });

            // ng-change
            $scope.chksession = function(session) {
                Rentalhours.sessionduration(session, function(data) {
                    $scope.asset.duration = data.duration;
                    $scope.changeDuration(data.duration); //update the EET
                });
            };

            $scope.customizedsession = function(status) {
                $scope.asset.session = undefined;
                $scope.customsesssion = status;
            };

            var startDate, endDate;

            $scope.changeDuration = function(duration) {

                startDate = moment($scope.asset.start).format('YYYY-MM-DD');
                endDate = moment($scope.asset.start).add(duration, 'h').format('YYYY-MM-DD');

                $scope.asset.end = moment($scope.asset.start).add(duration, 'h').format('h:mm A');

            };

            $scope.changeStartTime = function(startTime) {

                startDate = moment(startTime).format('YYYY-MM-DD');
                endDate = moment(startTime).add($scope.asset.duration, 'h').format('YYYY-MM-DD');

                $scope.asset.end = moment(startTime).add($scope.asset.duration, 'h').format('h:mm A');
            };

            $scope.ok = function(asset) {
                if(asset.session === undefined || asset.session === "") {

                    return AtvService.swal('Warning', 'Please Select or Enter a Session Name');

                } else if (startDate == 'Invalid date' || endDate == 'Invalid date') {

                    return AtvService.swal('Warning', 'Time is invalid');

                } else if (startDate != endDate) {

                    return AtvService.swal('Warning', 'Time span exceeds another day, please set it on the same day.');

                } else {

                    asset.start = moment(asset.start).format('h:mm A'); // format it first

                    Rentalhours.addrh(asset, function(data) {
                        $modalInstance.close();
                        loadlist();
                        var modalInstance = $modal.open({
                            templateUrl: 'notification2.html',
                            controller: notificationCTRL,
                            resolve: {
                                element: function() {
                                    var element = {'msg' : data.msg, 'type' : data.type };
                                    return element;
                                }
                            }
                        });
                    });
                }
            };

            $scope.cancel = function() {
                $modalInstance.dismiss();
            };
        });
    };

    $scope.total = function(start,end){

        if(end==undefined || start==undefined){
            $scope.Total = "--";
        }
        else{
            Rentalhours.total(start,end,function(data){
                if(data.replace(/\"/g, "")=="00:00" || data.replace(/\"/g, "")=="00:30"){
                    $scope.Total = "INVALID";
                    $scope.disabled = true;
                } else {
                    $scope.Total = data.replace(/\"/g, "");
                    $scope.disabled = false;
                }
            });
        }
    };

    var num_c = 10;
    var off_c = 1;
    var keyword_c = null;

    var paginate = function(num, off,keyword) {
        Rentalhours.list(num, off, keyword, function(data) {
            console.log(data);
            $scope.list = data.list;
            $scope.maxSize = 5;
            $scope.TotalItems = data.total_items;
            $scope.CurrentPage = data.index;
        });
    };
    paginate(num_c, off_c, keyword_c);

    $scope.setPage = function (off) {
        var searchito = $scope.searchtext;
        paginate(num_c, off, searchito);
    };
    $scope.search = function (keyword) {
        var searchito = $scope.searchtext;
        if(keyword != '') {
            paginate(num_c, off_c, searchito);
        } else {
            paginate(num_c, off_c, keyword_c);
        }

    };
    $scope.resetsearch = function(){
      $scope.searchtext = undefined;
      paginate(num_c, off_c, keyword_c);
    };

    $scope.checksession = function(data){
        if(!data){
          return "Sesssion should not be empty";
        }
    };

     //DELETE rentalid
    $scope.delete = function(rentalid){
        var modalInstance = $modal.open({
            templateUrl: 'notification.html',
            controller: dltCTRL,
            resolve: {
                rentalid: function () {
                return rentalid;
                }
            }     
        });
    };

    var dltCTRL = function ($scope, $modalInstance, rentalid) {
        $scope.message="Are you sure do you want to delete this Rental Hours?";
        $scope.ok = function () {
            $modalInstance.dismiss('cancel');
            Rentalhours.deleted(rentalid, function(data) {
                loadlist();
                var modalInstance = $modal.open({
                    templateUrl: 'notification2.html',
                    controller: notificationCTRL,
                    resolve: {
                        element: function() {
                            var element = {'msg' : data.msg, 'type' : data.type };
                            return element
                        }
                    }
                });
            });
        };
            $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    var loadlist = function() {
      Rentalhours.list(num_c,$scope.CurrentPage,$scope.searchtext,function(data){
            $scope.list = data.list;
            $scope.maxSize = 5;
            $scope.TotalItems = data.total_items;
            $scope.CurrentPage = data.index;
        });
    };



    // =========================================================>>>>>>>> WEW
    $scope.update= function(rentalid){
        var modalInstance = $modal.open({
            templateUrl: 'updaterentalhr.html',
            controller: updateCTRL,
            resolve: {
                rentalid: function () {
                  return rentalid;
              }}
        });
    };

    var updateCTRL = function($scope, $modalInstance, rentalid) {
        $scope.asset = {};
        
        $scope.hstep = 1; //[1, 2, 3]
        $scope.mstep = 5; //[1, 5, 10, 15, 25, 30]

        $scope.sessionalerts = [];
        $scope.sessioncloseAlert = function() {
            $scope.sessionalerts.splice(0, 1);
        };

        $scope.catalerts = [];
        $scope.tpl_title = "Update Rental Hours";

        Rentalhours.sessions(function(data) {
            $scope.sessions = data.sessions;
        });

        Rentalhours.edit(rentalid, function(data) {
            $scope.asset = data.data;
            $scope.Total = data.total;
        });

        // ng-change
        $scope.chksession = function(session) {
            Rentalhours.sessionduration(session, function(data) {
                $scope.asset.duration = data.duration;
                $scope.changeDuration(data.duration); //update the EET
            });
        };

        $scope.customizedsession = function(status) {
            $scope.asset.session = undefined;
            $scope.customsesssion = status;
        };

        var startDate, endDate;

        $scope.changeDuration = function(duration) {

            startDate = moment($scope.asset.start).format('YYYY-MM-DD');
            endDate = moment($scope.asset.start).add(duration, 'h').format('YYYY-MM-DD');

            $scope.asset.end = moment($scope.asset.start).add(duration, 'h').format('h:mm A');

        };

        $scope.changeStartTime = function(startTime) {

            startDate = moment(startTime).format('YYYY-MM-DD');
            endDate = moment(startTime).add($scope.asset.duration, 'h').format('YYYY-MM-DD');

            $scope.asset.end = moment(startTime).add($scope.asset.duration, 'h').format('h:mm A');
        };

        $scope.ok = function(asset) {
            if(asset.session === undefined || asset.session === "") {

                return AtvService.swal('Warning', 'Please Select or Enter a Session Name');

            } else if (startDate == 'Invalid date' || endDate == 'Invalid date') {

                return AtvService.swal('Warning', 'Time is invalid');

            } else if (startDate != endDate) {

                return AtvService.swal('Warning', 'Time span exceeds another day, please set it on the same day.');

            } else {

                asset.start = moment(asset.start).format('h:mm A'); // format it first

                Rentalhours.update(asset, function(data) {
                    $modalInstance.close();
                    loadlist();
                    var modalInstance = $modal.open({
                        templateUrl: 'notification2.html',
                        controller: notificationCTRL,
                        resolve: {
                            element: function() {
                                var element = {'msg' : data.msg, 'type' : data.type };
                                return element
                            }
                        }
                    });
                });
            }
        };

        $scope.cancel = function() {
            $modalInstance.close();
        };
    };

    Rentalhours.showme(function(data){
        $scope.msg = data;
    });

    $scope.Savemsg = function(content){
        Rentalhours.saveme(content,function(data){
            $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'RENTAL MESSAGE UPDATED!' });
        })
    }


});
