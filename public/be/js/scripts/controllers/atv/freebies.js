'use strict';

/* Controllers */

app.controller('Freebies', function($scope, $state, Upload ,$q, $http, Config, $modal ,Freebies ){
	console.log("--Freebies--");

	$scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    Freebies.showme(function(data){
        $scope.msg = data;
    });

    Freebies.showme1(function(data){
        $scope.msg1 = data;
    });

    Freebies.showme2(function(data){
        $scope.msg2 = data;
    });
    
    $scope.Save = function(content){
        Freebies.saveme(content,function(data){
            $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'FREEBIES UPDATED!' });
        })
    }

    $scope.Save1 = function(content){
        Freebies.saveme1(content,function(data){
            $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'DELIVERY SERVICE MESSAGE UPDATED!' });
        })
    }

    $scope.Save2 = function(content){
        Freebies.saveme2(content,function(data){
            $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'SPECIAL OFFERS UPDATED!' });
        })
    }
});