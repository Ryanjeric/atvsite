'use strict';

/* Controllers */

app.controller('Manageprices', function($scope, $state, Upload ,$q, $http, Config, $modal ,Manageprices ){
	console.log("--Manageprices--");

	$scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    var notificationCTRL = function($scope, element, $modalInstance) {
        $scope.message = element.msg;
        $scope.type = element.type;
        $scope.okay= function() {
            $modalInstance.close();
        }
    }

    var num_c = 10;
    var off_c = 1;
    var keyword_c = null;

    var paginate = function(num, off,keyword) {
        Manageprices.list(num,off,keyword,function(data){
            $scope.list = data.data;
            $scope.maxSize = 5;
            $scope.TotalItems = data.total_items;
            $scope.CurrentPage = data.index;
        })
    }
    paginate(num_c, off_c, keyword_c);

    $scope.setPage = function (off) {
        var searchito = $scope.searchtext;
        paginate(num_c, off, searchito);
    };
    $scope.search = function (keyword) { 
        var searchito = $scope.searchtext;
        if(keyword != '') {
            paginate(num_c, off_c, searchito);
        } else {
            paginate(num_c, off_c, keyword_c);
        }
        
    }
    $scope.resetsearch = function(){
      $scope.searchtext = undefined;
      paginate(num_c, off_c, keyword_c);
    }

     //DELETE ATV
    $scope.delete = function(reqid){
        console.log(reqid);
        var modalInstance = $modal.open({
          templateUrl: 'notification.html',
          controller: dltCTRL,
          resolve: {
            reqid: function () {
              return reqid;
          }
      }
    });
    };

    var dltCTRL = function ($scope, $modalInstance, reqid) {
        $scope.message="Are you sure do you want to delete this ATV?";
        $scope.ok = function () {
            $modalInstance.dismiss('cancel');
            Manageprices.deleted(reqid, function(data) {
                loadlist();
                var modalInstance = $modal.open({
                    templateUrl: 'notification2.html',
                    controller: notificationCTRL,
                    resolve: {
                        element: function() {
                            var element = {'msg' : data.msg, 'type' : data.type };
                            return element
                        }
                    }
                });
            });
        };
            $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    var loadlist = function(){
      Manageprices.list(num_c,$scope.CurrentPage,$scope.searchtext,function(data){
            $scope.list = data.data;
            $scope.maxSize = 5;
            $scope.TotalItems = data.total_items;
            $scope.CurrentPage = data.index;
        })
    }

    //UPDATE USER
  $scope.update = function(asset){
    var modalInstance = $modal.open({
      templateUrl: 'notification.html',
      controller: updateCTRL,
      resolve: {
        update: function () {
          return asset;
        }
      }
    });
  };
  var updateCTRL = function ($scope, $modalInstance, update, $state) {
    $scope.message="Are you sure do you want to Update this ATV?";
    $scope.update = update;
    $scope.ok = function () {
      $state.go('updateprices', {asset:update});
      $modalInstance.dismiss('cancel');
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }


});