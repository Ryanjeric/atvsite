'use strict';

/* Controllers */

app.controller('Requirements', function($scope,$http, Config, $modal ,Requirements ){
	console.log("--Requirements--");

	$scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    var notificationCTRL = function($scope, element, $modalInstance) {
        $scope.message = element.msg;
        $scope.type = element.type;
        $scope.okay= function() {
            $modalInstance.close();
        }
    }
    // =========================================================>>>>>>>> REQ
    $scope.newrequirements= function(){
        var modalInstance = $modal.open({
            templateUrl: 'saveone.html',
            controller: newrequirementsCTRL,
        });
    }
    var newrequirementsCTRL = function($scope, $modalInstance) {
        $scope.catalerts = [];
        $scope.tpl_title = "Add Requirements";
        $scope.placeholder = "Requirements here";

        $scope.cancel = function() {
            $modalInstance.close();
        }
        $scope.ok = function(asset) {
            var req = {
                reqasset: asset
            } 
            Requirements.addreq(req, function(data) {
                $modalInstance.close();
                paginate(num_c, off_c, keyword_c);
                var modalInstance = $modal.open({
                    templateUrl: 'notification2.html',
                    controller: notificationCTRL,
                    resolve: {
                        element: function() {
                            var element = {'msg' : data.msg, 'type' : data.type };
                            return element
                        }
                    }
                });
            });
        }
    }


    var num_c = 10;
    var off_c = 1;
    var keyword_c = null;

    var paginate = function(num, off,keyword) {
        Requirements.list(num,off,keyword,function(data){
            $scope.list = data.list;
            $scope.maxSize = 5;
            $scope.TotalItems = data.total_items;
            $scope.CurrentPage = data.index;
        })
    }
    paginate(num_c, off_c, keyword_c);

    $scope.setPage = function (off) {
        var searchito = $scope.searchtext;
        paginate(num_c, off, searchito);
    };
    $scope.search = function (keyword) { 
        var searchito = $scope.searchtext;
        if(keyword != '') {
            paginate(num_c, off_c, searchito);
        } else {
            paginate(num_c, off_c, keyword_c);
        }
        
    }
    $scope.resetsearch = function(){
      $scope.searchtext = undefined;
      paginate(num_c, off_c, keyword_c);
    }

    $scope.update = function(id, oldname, newname,index) {
        var asset = {id:id, name:newname};
        Requirements.update(asset, function(data) {
            var modalInstance = $modal.open({
                templateUrl: 'notification2.html',
                controller: notificationCTRL,
                resolve: {
                    element: function() {
                        var element = {'msg' : data.msg, 'type' : data.type };
                        return element
                    }
                }
            });
            if(data.type=='danger') {
                $scope.list[index].requirements = oldname;
            }
        });
    }

    //DELETE USER
    $scope.delete = function(reqid){
        console.log(reqid);
        var modalInstance = $modal.open({
          templateUrl: 'notification.html',
          controller: dltCTRL,
          resolve: {
            reqid: function () {
              return reqid;
          }
      }
    });
    };

    var dltCTRL = function ($scope, $modalInstance, reqid) {
        $scope.message="Are you sure do you want to delete this Requirements?";
        $scope.ok = function () {
            $modalInstance.dismiss('cancel');
            Requirements.deleted(reqid, function(data) {
                loadlist();
                var modalInstance = $modal.open({
                    templateUrl: 'notification2.html',
                    controller: notificationCTRL,
                    resolve: {
                        element: function() {
                            var element = {'msg' : data.msg, 'type' : data.type };
                            return element
                        }
                    }
                });
            });
        };
            $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

    var loadlist = function(){
      Requirements.list(num_c,$scope.CurrentPage,$scope.searchtext,function(data){
            $scope.list = data.list;
            $scope.maxSize = 5;
            $scope.TotalItems = data.total_items;
            $scope.CurrentPage = data.index;
        })
    }


});