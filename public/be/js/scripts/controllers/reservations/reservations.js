'use strict';

/* Controllers */

app.controller('ReservationsCtrl', function($scope, $state, Upload ,$q, $http, Config, $modal ,$timeout,Reservations,uiCalendarConfig,$compile,toaster){
	console.log("--Reservations--");

	$scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };
    $scope.viewlist = true;
    $scope.table = false;
    $scope.calendar = true;
    var num_c = 10;
    var off_c = 1;
    var keyword_c = undefined;

    var paginate = function(num, off,keyword) {
        Reservations.list(num,off,keyword,function(data){
            $scope.list = data.list;
            $scope.maxSize = 10;
            $scope.TotalItems = data.total_items;
            $scope.CurrentPage = data.index;
        })
    }
    paginate(num_c, off_c, keyword_c);
    $scope.setPage = function (off) {
        var searchito = $scope.searchtext;
        paginate(num_c, off, searchito);
    };
    $scope.search = function (keyword) { 
        var searchito = $scope.searchtext;
        if(keyword != '') {
            paginate(num_c, off_c, searchito);
        } else {
            paginate(num_c, off_c, keyword_c);
        }   
    }
    $scope.resetsearch = function(){
      $scope.searchtext = undefined;
      paginate(num_c, off_c, keyword_c);
    }

    var getEventsPerMonth = function(callback){
        Reservations.getreservation(function(data){
    	console.log(data);
        var events = [];
    	if(data!=0){
    		for(var i = 0; i < data.length; i++){
                if(data[i]['status'] == 0){
                   events.push({
                    title: 'NEW/' + data[i]['title'],
                    start: moment(data[i]['reservationdate']),
                    atv: data[i]['title'],
                    datereserve: data[i]['reservationdate'],
                    email: data[i]['email'],
                    phonenumber: data[i]['phonenum'],
                    invoiceno: data[i]['invoiceno'],
                    datecreated: data[i]['datecreated'],textColor: '#000',backgroundColor: 'yellow',
                    atvid: data[i]['atvid'],
                    rstatus: data[i]['status']
                   }); 
                }
                else if(data[i]['status'] == 1){
                    events.push({
                    title: 'RESERVED/' + data[i]['title'],
                    start: moment(data[i]['reservationdate']),
                    atv: data[i]['title'],
                    datereserve: data[i]['reservationdate'],
                    email: data[i]['email'],
                    phonenumber: data[i]['phonenum'],
                    invoiceno: data[i]['invoiceno'],
                    datecreated: data[i]['datecreated'],textColor: '#fff',backgroundColor: 'violet',
                    atvid: data[i]['atvid'],
                    rstatus: data[i]['status']
                   }); 
                }
                else if(data[i]['status'] == 2){
                    events.push({
                    title: 'DONE/' + data[i]['title'],
                    start: moment(data[i]['reservationdate']),
                    atv: data[i]['title'],
                    datereserve: data[i]['reservationdate'],
                    email: data[i]['email'],
                    phonenumber: data[i]['phonenum'],
                    invoiceno: data[i]['invoiceno'],
                    datecreated: data[i]['datecreated'],textColor: '#fff',backgroundColor: 'green',
                    atvid: data[i]['atvid'],
                    rstatus: data[i]['status']
                   }); 
                }
                else if(data[i]['status'] == 3){
                    events.push({
                    title: 'VOID /' + data[i]['title'],
                    start: moment(data[i]['reservationdate']),
                    atv: data[i]['title'],
                    datereserve: data[i]['reservationdate'],
                    email: data[i]['email'],
                    phonenumber: data[i]['phonenum'],
                    invoiceno: data[i]['invoiceno'],
                    datecreated: data[i]['datecreated'],textColor: '#fff',backgroundColor: 'red',
                    atvid: data[i]['atvid'],
                    rstatus: data[i]['status']
                   }); 
                }
    		}
    	}
        callback(events);
    })};


    


    $scope.overlay = $('.fc-overlay');
    $scope.alertOnMouseOver = function( event, jsEvent, view ){
        $scope.event = event;
        $scope.overlay.removeClass('left right').find('.arrow').removeClass('left right top pull-up');
        var wrap = $(jsEvent.target).closest('.fc-event');
        var cal = wrap.closest('.calendar');
        var left = wrap.offset().left - cal.offset().left;
        var right = cal.width() - (wrap.offset().left - cal.offset().left + wrap.width());
        if( right > $scope.overlay.width() ) {
            $scope.overlay.addClass('left').find('.arrow').addClass('left pull-up')
        }else if ( left > $scope.overlay.width() ) {
            $scope.overlay.addClass('right').find('.arrow').addClass('right pull-up');
        }else{
            $scope.overlay.find('.arrow').addClass('top');
        }
        $scope.overlay.css('z-index', 10000);
         wrap.append( $scope.overlay );
    }


    $scope.viewreservation = function(invoiceno){
        var modalInstance = $modal.open({
            templateUrl: 'viewreservationCTRL.html',
            controller: viewreservationCTRL,
            resolve: {
                invoiceno: function () {
                    return invoiceno;
                }
            }
        });
    };

    var loadpage = function(){
        Reservations.list(10,$scope.CurrentPage,$scope.searchtext,function(data){
            $scope.list = data.list;
            $scope.maxSize = 10;
            $scope.TotalItems = data.total_items;
            $scope.CurrentPage = data.index;
        })
    }


    var viewreservationCTRL = function($scope, $modalInstance, invoiceno, $sce) {
       var loadlist = function(){
        Reservations.viewreservation(invoiceno,function(data){
            console.log(data);
            $scope.reservationinfo = data.data;
            $scope.reservationlist = data.data2;
        })
       }
       loadlist();
       $scope.change1 = function(id){
        Reservations.changestatus1(id,function(data){
            loadlist();
            loadpage();
        })
       }
       $scope.change2 = function(id){
        Reservations.changestatus2(id,function(data){
            loadlist();
            loadpage();
        })
       }
       $scope.change3 = function(id){
        Reservations.changestatus3(id,function(data){
            loadlist();
            loadpage();
        })
       }
       $scope.change4 = function(id){
        Reservations.changestatus4(id,function(data){
            loadlist();
            loadpage();
        })
       }

       $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
            uiCalendarConfig.calendars['calendar1'].fullCalendar( 'refetchEvents' );
        };
    };

    ///CALENDAR VALIDATIONS///
    function checkifreserved(obj,atvid,dropdate){
        for (var i = 0; i < Object.keys(obj).length; i++) 
        {
            if(obj[i]['rstatus'] == 1 && obj[i]['datereserve'] == dropdate && obj[i]['atvid'] == atvid){
                return true;
            }
        } 
        return false;
    }



    /////CALENDAR ->>>

     /* config object */
    $scope.uiConfig = {
      calendar:{
        height: 440,
        editable: true,
        header:{
          left: 'prev',
          center: 'title',
          right: 'next'
        },
        dayClick: $scope.dayClick,
        timeFormat: ' ',
        events: function(start, end, timezone, callback) {
                console.log("HITTING EVENTS!!!");
                getEventsPerMonth(function(data){
                    callback(data);
                });
            },
        eventMouseover: $scope.alertOnMouseOver,
        eventDrop: function(event, delta, revertFunc){
                var pulledEvents = uiCalendarConfig.calendars['calendar1'].fullCalendar( 'clientEvents');
                console.log(pulledEvents);
                if(event.start.unix() <= moment().unix()){
                    revertFunc();
                    toaster.pop("warning","These date have already passed.");
                }
                else if(checkifreserved(pulledEvents,event.atvid,event.start.format('YYYY-MM-DD')) == true){
                    revertFunc();
                    toaster.pop("warning","THIS ATV IS ALREADY RESERVED IN THIS DAY");
                }

       /* alert(event.title + " was dropped on " + event.start.format('YYYY-MM-DD'));

        if (!confirm("Are you sure about this change?")) {
            revertFunc();
        }*/
        /*var modalInstance = $modal.open({
            templateUrl: 'viewreservationCTRL.html',
            controller: viewreservationCTRL,
            resolve: {
                invoiceno: function () {
                    return invoiceno;
                }
            }
        });*/

        },
      }
    };

    $scope.events = [];
    /* event sources array*/

    $scope.eventSources = [$scope.events ];

    $scope.renderCalender = function(calendar) {
      if(uiCalendarConfig.calendars[calendar]){
        uiCalendarConfig.calendars[calendar].fullCalendar('render');
      }
    };

    $scope.today = function(calendar) {
        uiCalendarConfig.calendars['calendar1'].fullCalendar('today');
    };

    $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
        console.log(delta);
    };


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    var num = 10;
    var off = 1;
    var keyword = undefined;

    var paginate1 = function(num, off,keyword) {
        Reservations.list1(num,off,keyword,function(data){
            $scope.list1 = data.list;
            $scope.maxSize = 10;
            $scope.TotalItems = data.total_items;
            $scope.CurrentPage = data.index;
        })
    }
    paginate1(num, off, keyword);
    $scope.setPage = function (off) {
        var searchito = $scope.searchtext;
        paginate1(num, off, searchito);
    };
    $scope.search = function (keyword) { 
        var searchito = $scope.searchtext;
        if(keyword != '') {
            paginate1(num_c, off, searchito);
        } else {
            paginate1(num_c, off, keyword);
        }   
    }
    $scope.resetsearch = function(){
      $scope.searchtext = undefined;
      paginate1(num, off, keyword);
    }



    $scope.status0 = function(id){
        Reservations.status0(id,function(data){
            paginate1(num, $scope.CurrentPage, $scope.searchtext);
        })
    }

    $scope.status1 = function(id){
        Reservations.status1(id,function(data){
            paginate1(num, $scope.CurrentPage, $scope.searchtext);
        })
    }

    $scope.status2 = function(id){
        Reservations.status2(id,function(data){
            paginate1(num, $scope.CurrentPage, $scope.searchtext);
        })
    }

    $scope.status3 = function(id){
        Reservations.status3(id,function(data){
            paginate1(num, $scope.CurrentPage, $scope.searchtext);
        })
    }

    $scope.status4 = function(id){
        Reservations.status4(id,function(data){
            paginate1(num, $scope.CurrentPage, $scope.searchtext);
        })
    }


    $scope.view = function(customerid){
        var modalInstance = $modal.open({
            templateUrl: 'viewreservation1CTRL.html',
            controller: viewreservation1CTRL,
            resolve: {
                customerid: function () {
                    return customerid;
                }
            }
        });
    };

     var viewreservation1CTRL = function($scope, $modalInstance, customerid, $sce) {
       var loadlist = function(){
        Reservations.viewreservation1(customerid,function(data){
            console.log(data);
            $scope.reservationinfo = data.data;
            $scope.reservationlist = data.data2;
        })
       }
       loadlist();
       
       $scope.printme = function(){
        window.print();
        }

       $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
            
        };
    };


    
});