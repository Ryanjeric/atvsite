'use strict';

/* Controllers ni : Ryan jeric Sabado.*/

app.controller('Adduserctrl', function($scope, $state , Upload ,$q, $http, Config, $stateParams ,$modal, Users){

  $scope.alerts = [];

  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };
  var alertme = function(){
    $scope.alerts.splice(0, 1);
    $scope.alerts.push({type: 'success', msg: 'USER SUCCESSFULLY SAVE!'});      
  }

  $scope.chkusername =function(username){
    Users.checkusername(username, function(data){
      $scope.usrname= data.exists;
      if(data.exists == true){
        $scope.form.$invalid = true;
      }else{
        $scope.form.$invalid = false;
      }
    });
  };
  //VALIDATE EMAIL
  $scope.chkemail =function(email){
    Users.checkemail(email, function(data){
      $scope.usremail= data.exists;
      if(data.exists == true){
        $scope.form.$invalid = true;
      }else{
        $scope.form.$invalid = false;
      }
    });

  };
  //VALIDATE PASSWORD IF MATCH
  $scope.confirmpass =function(confirmpass, password){
    if(confirmpass!= undefined){
      Users.password(confirmpass, password, function(data){
      $scope.pwdconfirm = data;
      if(data == true){
        $scope.form.$invalid = true;
      }else{
        $scope.form.$invalid = false;
      }
    });
    }
    if(password.length<=5){
      $scope.pwdshort = true;
      $scope.form.$invalid = true;
    }
    else{
      $scope.pwdshort = false;
      $scope.form.$invalid = false;
    }
  };


  // UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////
$scope.showimageList = function(size,path){
    var amazon = $scope.amazon;
    var modalInstance = $modal.open({
        templateUrl: 'imagelist.html',
        controller: imagelistCTRL,
        size: size,
        resolve: {
            path: function() {
                return amazon
            }
        }

    });
}
$scope.stat="1";
var pathimage = "";

var pathimages = function(){
    $scope.amazonpath=pathimage;
}

var imagelistCTRL = function( $modalInstance,$scope, $state, Upload ,$q, $http, Config, $stateParams, path){
    $scope.amazonpath= path;
    $scope.imageloader=false;
    $scope.imagecontent=true;
    $scope.noimage = false;
    $scope.imggallery = false;

    var loadimages = function() {
        $http({
            url: Config.ApiURL + "/user/listimages",
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            if(data.error == "NOIMAGE" ){
              $scope.imggallery=false;
              $scope.noimage = true;
            }else{
              $scope.noimage = false;
              $scope.imggallery=true;
              $scope.imagelist = data;
            }
        }).error(function(data) {
        });
    }
    loadimages();

    $scope.path=function(path){
      var texttocut = Config.amazonlink + '/uploads/images/';
      var newpath = path.substring(texttocut.length); 
      pathimage = newpath;
      pathimages();
      $modalInstance.dismiss('cancel');
    }



    $scope.upload = function(files) {
       $scope.upload(files);  
    };

    $scope.delete = function(id){
          var modalInstance = $modal.open({
            templateUrl: 'notification.html',
            controller: deleteCTRL,
            resolve: {
              imgid: function() {
                return id
              }
            }
          });
       }
 
 var deleteCTRL = function($scope, $modalInstance, imgid) {

  $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

  $scope.message="Are you sure do you want to delete this Photo?";
    $scope.ok = function() {
     $http({
     url: Config.ApiURL+"/user/deleteimage/"+ imgid,
      method: "get",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }).success(function(data, status, headers, config) {
      $scope.alerts.push({ type: 'success', msg: 'Image successfully Deleted!' });
      loadimages();
      $modalInstance.close();
    }).error(function(data, status, headers, config) {
      loadimages();
      $modalInstance.close();
      $scope.alerts.push({ type: 'success', msg: 'Something went wrong Image not Deleted!' });
    });
  };

  $scope.cancel = function() {
    $modalInstance.dismiss('cancel');
  };
  }

  $scope.upload = function (files) 
  {
    var filename;
    var filecount = 0;
    if (files && files.length) 
    {
      $scope.imageloader=true;
      $scope.imagecontent=false;

      for (var i = 0; i < files.length; i++) 
      {
        var file = files[i];

        if (file.size >= 2000000)
        {
          $scope.alertss.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
          filecount = filecount + 1;
          
          if(filecount == files.length)
          {
            $scope.imageloader=false;
            $scope.imagecontent=true;
          }
        }
        else
        {
          var promises;

          var fileExtension = '.' + file.name.split('.').pop();

                    // rename the file with a sufficiently random value and add the file extension back
                    var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + i + fileExtension;
                    
                    promises = Upload.upload({
                      
                                 url:Config.amazonlink, //S3 upload url including bucket name
                                 method: 'POST',
                                 transformRequest: function (data, headersGetter) {
                                  //Headers change here
                                  var headers = headersGetter();
                                  delete headers['Authorization'];
                                  return data;
                                },
                                fields : {
                                    key: 'uploads/images/' + renamedFile, // the key to store the file on S3, could be file name or customized
                                    AWSAccessKeyId: Config.AWSAccessKeyId,
                                    acl: 'private', // sets the access to the uploaded file in the bucket: private or public 
                                    policy: Config.policy, // base64-encoded json policy (see article below)
                                    signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                                    "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                                  },
                                  file: file
                                })
          promises.then(function(data){

            filecount = filecount + 1;

            filename = data.config.file.name;
            var fileout = {
              'imgfilename' : renamedFile
            };
            $http({
              url: Config.ApiURL + "/user/saveimage",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(fileout)
            }).success(function (data, status, headers, config) {
              loadimages();
              if(filecount == files.length)
              {
                $scope.imageloader=false;
                $scope.imagecontent=true;
              }
              
            }).error(function (data, status, headers, config) {
              $scope.imageloader=false;
              $scope.imagecontent=true;
            });
            
          });
        }
      }
    }
  };
  $scope.cancel = function() {
      $modalInstance.dismiss('cancel');

  };
};

// UPLOAD PHOTO ///////////////////////////////////////////////////////////////////////////////////////////
$scope.adminischeck = false;

$scope.checkifchecked = function(){
  if($("#checkSurfaceEnvironment-1").prop('checked') == true){
    console.log('checked');
    $scope.adminischeck = true;
    $scope.user.role1 = false;
    $scope.user.role2 = false;
    $scope.user.role3 = false;
    $scope.user.role4 = false;
    $scope.user.role5 = false;
    $scope.user.role6 = false;
    $scope.user.role7 = false;
    $scope.user.role8 = false;
    $scope.user.role9 = false;
    $('.super').attr('checked','checked');
    $('.role').attr('checked', false);
  }else{
     console.log('!checked');
     $scope.adminischeck = false;
  }
}



  $scope.submitData = function(user){
    Users.adduser(user,function(data)
    {
      $scope.user = "";
      $scope.amazonpath = "";
      $scope.form.$setPristine(true);
      alertme();
    });

  }



	//DATE
	 $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];







})