'use strict';

/* Controllers ni : Ryan jeric Sabado.*/

app.controller('Userlistctrl', function($scope, $state , Upload ,$q, $http, Config, $stateParams ,$modal, Users){
  $scope.loggeduser = username;
  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = undefined;
  var role = undefined;
  var paginate = function (off, keyword, role) {
    $http({
      url: Config.ApiURL+"/user/list/" + num + '/' + off + '/' + keyword + '/' + role,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
          $scope.bigTotalItems = data.total_items;
          $scope.bigCurrentPage = data.index;
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }
  $scope.search = function (keyword,role) {
    var off = 1;
    paginate(off, keyword,role);
  }
  $scope.numpages = function (off, keyword,role) {
    var searchito = $scope.searchtext;
    var searchito1 = $scope.searchrole;
    paginate(off, searchito,searchito1);
  }
  $scope.setPage = function (off) {
    var searchito = $scope.searchtext;
    var searchito1 = $scope.searchrole;
        paginate(off, searchito,searchito1);
    };
  paginate(off, keyword,role);
  //END USER LISTING

  $scope.resetsearch = function(){
    $scope.searchtext = undefined;
    $scope.searchrole = undefined;
    paginate(off, keyword,role);
  }

  //DELETE USER
  $scope.delete = function(user){
    var modalInstance = $modal.open({
      templateUrl: 'notification.html',
      controller: dltCTRL,
      resolve: {
        dltuser: function () {
          return user;
        }
      }
    });
  };

  $scope.changestatus = function(id,status){
    $http({
        url:  Config.ApiURL+"/user/changestatus/"+ id + '/' + status,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        loadpage();
      })
  };

  var loadpage = function(){
        $http({
      url: Config.ApiURL+"/user/list/" + num + '/' + $scope.bigCurrentPage + '/' + $scope.searchtext + '/' + $scope.searchrole,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.maxSize = 5;
          $scope.bigTotalItems = data.total_items;
          $scope.bigCurrentPage = data.index;
    });
    }



  $scope.alerts = [];
  $scope.closeAlert = function(index) {$scope.alerts.splice(index, 1);};
  var alertme = function(){
    $scope.alerts.splice(0, 1);
    $scope.alerts.push({type: 'success', msg: 'User has been successfully Deleted!'});      
  }
  var dltCTRL = function ($scope, $modalInstance, dltuser) {
    $scope.dltuser = dltuser;
    $scope.message="Are you sure do you want to delete this User?";
    $scope.ok = function () {

      $http({
        url:  Config.ApiURL+"/user/delete/"+dltuser,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }).success(function (data, status, headers, config) {
        $modalInstance.dismiss('cancel');
        alertme();
        loadpage();
      })
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }
  //UPDATE USER
  $scope.update = function(user){
    var modalInstance = $modal.open({
      templateUrl: 'notification.html',
      controller: updateCTRL,
      resolve: {
        update: function () {
          return user;
        }
      }
    });
  };
  var updateCTRL = function ($scope, $modalInstance, update, $state) {
    $scope.message="Are you sure do you want to Update this User?";
    $scope.update = update;
    $scope.ok = function () {
      $state.go('updateuser', {userid:update});
      $modalInstance.dismiss('cancel');
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }
})