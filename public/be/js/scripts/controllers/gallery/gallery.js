'use strict';

/* Controllers */

app.controller('Gallery', function($scope, $state, Upload ,$q, $http, Config, $modal ,Gallery ){

  $scope.alerts = [];
  $scope.alertz = [];

  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };
  $scope.closeAlertz = function (index) {
    $scope.alertz.splice(index, 1);
  };

  var alertme = function(){
    $scope.alerts.splice(0, 1);
    $scope.alerts.push({type: 'success', msg: 'Image has been successfully Deleted!'});
  }

  var alertme2 = function(){
    $scope.alertz.splice(0, 1);
    $scope.alertz.push({type: 'success', msg: 'Video has been successfully Deleted!'});
  }

  $scope.imageloader=false;
  $scope.imagecontent=true;
  $scope.noimage = false;
  $scope.imggallery = false;

  var loadimages = function() {
    Gallery.loadimage(function(data){
    if(data.error == "NOIMAGE" ){
      $scope.imggallery=false;
      $scope.noimage = true;
    }else{
      $scope.noimage = false;
      $scope.imggallery=true;
      $scope.imagelist = data;
      $scope.countimg = data.length;
    }
  });
  }
  loadimages();

  $scope.delete = function(id){
   var modalInstance = $modal.open({
    templateUrl: 'notification.html',
    controller: deleteCTRL,
    resolve: {
      imgid: function() {
        return id
      }
    }

  });

 }


 var deleteCTRL = function($scope, $modalInstance, imgid) {
  $scope.message="Are you sure do you want to delete this Photo?";
  $scope.ok = function() {
   $http({
     url: Config.ApiURL+"/gallery/delete/"+ imgid,
     method: "get",
     headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  }).success(function(data, status, headers, config) {
    loadimages();
    alertme();
    $modalInstance.close();
  }).error(function(data, status, headers, config) {
    loadimages();
    $modalInstance.close();
  });


};

$scope.cancel = function() {
  $modalInstance.dismiss('cancel');
};
}





$scope.upload = function(files) {
 $scope.upload(files);
};
$scope.upload = function (files)
{
  var filename
  var filecount = 0;
  if (files && files.length)
  {
    $scope.imageloader=true;
    $scope.imagecontent=false;

    for (var i = 0; i < files.length; i++)
    {
      var file = files[i];

      if (file.size >= 2000000)
      {
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
        filecount = filecount + 1;

        if(filecount == files.length)
        {
          $scope.imageloader=false;
          $scope.imagecontent=true;
        }
      }
      else
      {
        var promises;

        var fileExtension = '.' + file.name.split('.').pop();

                  // rename the file with a sufficiently random value and add the file extension back
                  var renamedFile =  Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;


                  promises = Upload.upload({

                   url:Config.amazonlink,
                   method: 'POST',
                   transformRequest: function (data, headersGetter) {
                                //Headers change here
                                var headers = headersGetter();
                                delete headers['Authorization'];
                                return data;
                              },
                              fields : {
                                // key: 'uploads/images/' + renamedFile,
                                key: 'uploads/images/' + file.name,
                                AWSAccessKeyId: Config.AWSAccessKeyId,
                                acl: 'private',
                                policy: Config.policy,
                                signature: Config.signature,
                                "Content-Type": file.type != '' ? file.type : 'application/octet-stream'
                              },
                              file: file
                            })
                  promises.then(function(data){

                    filecount = filecount + 1;
                    filename = data.config.file.name;
                    var fileout = {
                      // 'imgfilename' : renamedFile
                      'imgfilename' : filename
                    };
                    $http({
                      url: Config.ApiURL + "/gallery/saveslider",
                      method: "post",
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                      data: $.param(fileout),
                    }).success(function (data, status, headers, config) {
                      loadimages();
                      if(filecount == files.length)
                      {
                        $scope.imageloader=false;
                        $scope.imagecontent=true;
                      }
                    })
                  });
                }



              }
            }
          };

          var returnYoutubeThumb = function(item){
            var x= '';
            var thumb = {};
            if(item){
              var newdata = item;
              var x;
              x = newdata.match(/src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
              thumb['url'] ='http://img.youtube.com/vi/' + x[1] + '/hqdefault.jpg';
              thumb['yid'] = x[1];
              return thumb;
            }else{
              return x;
            }
          }


          $scope.savevid = function(vid) {
            if(vid){
              var x = vid.match(/<iframe width="(.*?)" height="(.*?)" src="https:\/\/www.youtube.com\/embed\/(.*?)"+/);
              if(x==null){
                $scope.invalidvideo = true;
                $scope.video='';
              }else{
                var vidlink = { 'vid': vid }
                Gallery.saveVid(vidlink,function(data){
                  loadvideo();
                  $scope.video='';
                  $scope.invalidvideo = false;
                  console.log(data);
                });
              }
            }
          };

          var loadvideo = function() {
            Gallery.loadVideo(function(data){
              if(data.error == "NOVIDEOS" ){
                $scope.novid = true;
                 $scope.countvid = '0';
              }
              else{
                $scope.novid = false;
                for (var x in data){
                  var newd = returnYoutubeThumb(data[x].embed);
                  data[x].videourl = newd.url;
                  data[x].youtubeid = newd.yid;
                }
                $scope.countvid = data.length;
                $scope.videolist = data;
              }


            })
          }

          loadvideo();


          $scope.deletevideo = function(id){
           var modalInstance = $modal.open({
            templateUrl: 'notification.html',
            controller: deletevidCTRL,
            resolve: {
              videoid: function() {
                return id
              }
            }

          });
         }

         var deletevidCTRL = function($scope, $modalInstance, videoid) {
          $scope.message="Are you sure do you want to delete this Video?";
          $scope.ok = function() {
           var datavideo = {
            'videoid' : videoid
          };
          Gallery.deleteVideo(datavideo, function(data){
            loadvideo();
            alertme2();
            $modalInstance.close();
          });
        };

        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        };
      }



    })
