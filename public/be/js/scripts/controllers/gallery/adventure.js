'use strict';

/* Controllers */

app.controller('Adventure', function($scope, $state, Upload ,$q, $http, Config, $modal ,Gallery ){
	console.log("--Adventure--");

	$scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };


    Gallery.showme1(function(data){
        $scope.msg = data;
    });


    $scope.Save = function(content){
        Gallery.saveme1(content,function(data){
            $scope.alerts.splice(0, 1);
            $scope.alerts.push({ type: 'success', msg: 'CONTENT UPDATED!' });
        })
    }

});