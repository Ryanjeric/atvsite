app.factory('Metatrails', function($http, $q, Config ,Upload){
	return {
		data: {},
        list: function(num,off,keyword,callback) {
            $http({
                url: Config.ApiURL+"/metatrails/list/" + num + '/' + off + '/' + keyword,
                method: "GET",
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        update: function(asset, callback) {
            $http({
                url: Config.ApiURL+"/metatrails/update",
                method: "POST",
                headers: {"Content-Type" : "application/x-www-form-urlencoded"},
                data: $.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        edit: function(asset,callback){
             $http({
                url: Config.ApiURL+"/metatrails/edit/"+asset,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
}
})