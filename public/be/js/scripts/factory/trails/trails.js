app.factory('Trails', function($http, $q, Config ,Upload){
	return {
		data: {},
        showme:  function(callback){
            $http({
                url: Config.ApiURL + "/freebies/show",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
            });
        },
        saveme: function(req,callback){
           $http({
            url: Config.ApiURL+"/trails/savedata",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data:$.param(req)
        }).success(function (data, status, headers, config) {
            callback(data);
        });
    },
    uploadpic : function(files, callback){
        var file = files;
        var promises;


        promises = Upload.upload({
                url: Config.amazonlink, //S3 upload url including bucket name
                method: 'POST',
                transformRequest: function (data, headersGetter) {
                    //Headers change here
                    var headers = headersGetter();
                    delete headers['Authorization'];
                    return data;
                },
                fields: {
                    key: 'uploads/images/' + file.name, // the key to store the file on S3, could be file name or customized
                    AWSAccessKeyId: Config.AWSAccessKeyId,
                    acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                    policy: Config.policy, // base64-encoded json policy (see article below)
                    signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                    "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                },
                file: file
            }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                var log = progressPercentage;
            }).then(function(data){
                callback(data);
            });
        },
        uploadpic2 : function(files, callback){
           for (var i = 0; i < files.length; i++)
           {
            var file = files[i];
            var promises;


                promises = Upload.upload({
                url: Config.amazonlink, //S3 upload url including bucket name
                method: 'POST',
                transformRequest: function (data, headersGetter) {
                    //Headers change here
                    var headers = headersGetter();
                    delete headers['Authorization'];
                    return data;
                },
                fields: {
                    key: 'uploads/images/' + file.name, // the key to store the file on S3, could be file name or customized
                    AWSAccessKeyId: Config.AWSAccessKeyId,
                    acl: 'private', // sets the access to the uploaded file in the bucket: private or public
                    policy: Config.policy, // base64-encoded json policy (see article below)
                    signature: Config.signature, // base64-encoded signature based on policy string (see article below)
                    "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
                },
                file: file
            }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                var log = progressPercentage;
            }).then(function(data){
                callback(data);
            });

        }
    },
    listrentals: function(callback){
        $http({
            url: Config.ApiURL + "/atv/listrentals",
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            callback(data);

        })
    },
    list: function(num,off,keyword,callback) {
        $http({
            url: Config.ApiURL+"/trails/list/" + num + '/' + off + '/' + keyword,
            method: "GET",
        }).success(function (data, status, headers, config) {
            callback(data);
        });
    },
    deleted: function(asset,callback){
       $http({
        url: Config.ApiURL+"/trails/delete/"+asset,
        method: "GET",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
        callback(data);
    });
    },
    info: function(asset,callback){
        $http({
            url: Config.ApiURL+"/trails/info/"+asset,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data, status, headers, config) {
            callback(data);
        });
    },
    update: function(asset, callback) {
        $http({
            url: Config.ApiURL+"/trails/update",
            method: "POST",
            headers: {"Content-Type" : "application/x-www-form-urlencoded"},
            data: $.param(asset)
        }).success(function (data, status, headers, config) {
            callback(data);
        });
    },
		ensureuniqueslugs: function(trailid, slugs, callback) {
			$http({
				url: Config.ApiURL + "/trail/ensureuniqueslugs",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({trailid:trailid, slugs:slugs})
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		}
}
})
