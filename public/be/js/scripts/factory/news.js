app.factory("NewsFactory", function($http, Config) {
	return {
		addnewscategory: function(asset, callback) {
			$http({
				url: Config.ApiURL+"/news/add/category/"+asset,
				method: "GET",
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		paginate_categorylist: function(num,off,keyword,callback) {
			$http({
				url: Config.ApiURL+"/news/list/category/" + num + '/' + off + '/' + keyword,
				method: "GET",
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		updatecategory: function(asset, callback) {
			$http({
				url: Config.ApiURL+"/news/update/category",
				method: "POST",
				headers: {"Content-Type" : "application/x-www-form-urlencoded"},
				data: $.param(asset)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		addnewstag: function(asset, callback) {
			$http({
				url: Config.ApiURL+"/news/add/tag/"+asset,
				method: "GET",
			}).success(function (data, status, headers, config) {
				callback(data);
			}).error(function (data, status, headers, config) {
				callback(data);
			});

		},
		paginate_taglist: function(num,off,keyword,callback) {
			$http({
				url: Config.ApiURL+"/news/list/tags/" + num + '/' + off + '/' + keyword,
				method: "GET",
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		updatetag: function(asset, callback) {
			$http({
				url: Config.ApiURL+"/news/update/tag",
				method: "POST",
				headers: {"Content-Type" : "application/x-www-form-urlencoded"},
				data: $.param(asset)
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		removecategory: function(id, callback) {
			$http({
				url: Config.ApiURL+"/news/remove/category/" +id,
				method: "GET",
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		removetag: function(id, callback) {
			$http({
				url: Config.ApiURL+"/news/remove/tag/" +id,
				method: "GET",
			}).success(function (data, status, headers, config) {
				callback(data);
			});
		},
		upload_author: function(fileout, callback) {
			$http({
				url: Config.ApiURL + "/author/uploadimage",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		authorimages: function(callback) {
			$http({
				url: Config.ApiURL + "/author/images",
				method: "GET",
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		saveauthor: function(asset, callback) {
			$http({
				url: Config.ApiURL + "/author/create",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(asset)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		authorspagination: function(asset, callback) {
			$http({
				url: Config.ApiURL + "/authors/pagination",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(asset)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		deleteauthor: function(authorid, callback) {
			$http({
				url: Config.ApiURL + "/author/delete",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({authorid:authorid})
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		loadauthor: function(authorid, callback) {
			$http({
				url: Config.ApiURL + "/author/load",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({authorid:authorid})
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		ensureuniqueauthorname: function(asset, callback) {
			$http({
				url: Config.ApiURL + "/author/ensureuniquename",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(asset)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		updateauthor: function(asset, callback) {
			$http({
				url: Config.ApiURL + "/author/update",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(asset)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		createnews_resources: function(callback) {
			$http({
				url: Config.ApiURL + "/news/create/resources",
				method: "GET",
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		loadcategories: function(callback) {
			$http({
				url: Config.ApiURL + "/news/loadcategories",
				method: "GET",
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		loadtags: function(callback) {
			$http({
				url: Config.ApiURL + "/news/loadtags",
				method: "GET",
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		upload_news: function(fileout, callback) {
			$http({
				url: Config.ApiURL + "/news/uploadimage",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		newsimages: function(callback) {
			$http({
				url: Config.ApiURL + "/news/images",
				method: "GET",
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		deletenewsimage: function(imgid, callback){
			$http({
				url: Config.ApiURL + "/news/deleteimage",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({imgid:imgid})
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		saveembed: function(assets, callback) {
			$http({
				url: Config.ApiURL + "/news/saveembed",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(assets)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		newsvideos: function(callback) {
			$http({
				url: Config.ApiURL + "/news/videos",
				method: "GET",
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		deletenewsvideo: function(vidid, callback) {
			$http({
				url: Config.ApiURL + "/news/deletevideo",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({vidid:vidid})
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		createnews: function(news, callback) {
			$http({
				url: Config.ApiURL + "/news/create",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(news)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		newspagination: function(asset, callback) {
			$http({
				url: Config.ApiURL + "/news/pagination",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(asset)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		deletenews: function(newsid, callback) {
			$http({
				url: Config.ApiURL + "/news/delete",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({newsid:newsid})
			}).success(function (data, status, headers, config) {
				console.log(data)
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		newsinfo: function(newsid, callback) {
			$http({
				url: Config.ApiURL + "/news/info",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({newsid:newsid})
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		ensureuniqueslugs: function(newsid, newsslugs, callback) {
			$http({
				url: Config.ApiURL + "/news/ensureuniqueslugs",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({newsid:newsid, newsslugs:newsslugs})
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		savenewsupdate: function(news, callback) {
			$http({
				url: Config.ApiURL + "/news/saveupdate",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(news)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		deleteauthorimage: function(imgid, callback){
			$http({
				url: Config.ApiURL + "/author/deleteimage",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({imgid:imgid})
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		attemptdeleteauthor: function(page, authorid, callback) {
			$http({
				url: Config.ApiURL + "/author/attemptdelete",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({authorid:authorid, page:page})
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		changenewsstatus: function(assets, callback) {
			$http({
				url: Config.ApiURL + "/news/changestatus",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(assets)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		newsupdateauthor: function(news, callback) {
			$http({
				url: Config.ApiURL + "/news/updateauthor",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(news)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		attemptdeletecategory: function(page, categoryid, callback) {
			$http({
				url: Config.ApiURL + "/category/attemptdelete",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({categoryid:categoryid, page:page})
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		newsupdatecategory: function(news, callback) {
			$http({
				url: Config.ApiURL + "/news/updatecategory",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(news)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		attemptdeletetag: function(page, tagid, callback) {
			$http({
				url: Config.ApiURL + "/tag/attemptdelete",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({tagid:tagid, page:page})
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		},
		newsupdatetag: function(news, callback) {
			$http({
				url: Config.ApiURL + "/news/updatetag",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(news)
			}).success(function (data, status, headers, config) {
				callback(data)
			}).error(function (data, status, headers, config) {
				callback(data)
			});
		}


	} // RETURN END
})
