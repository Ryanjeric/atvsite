app.factory('Createprices', function($http, $q, Config){
	return {
		data: {},
        showme:  function(callback){
            $http({
                url: Config.ApiURL + "/freebies/show",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
            });
         },
         saveme: function(content,callback){
             $http({
                url: Config.ApiURL+"/atv/savedata",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data:$.param(content)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        listrentals: function(callback){
            $http({
                url: Config.ApiURL + "/atv/listrentals",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
	}
})