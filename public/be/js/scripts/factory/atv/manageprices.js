app.factory('Manageprices', function($http, $q, Config){
	return {
		data: {},
        list: function(num,off,keyword,callback) {
            $http({
                url: Config.ApiURL+"/atv/list/" + num + '/' + off + '/' + keyword,
                method: "GET",
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        deleted: function(asset,callback){
             $http({
                url: Config.ApiURL+"/atv/delete/"+asset,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
	}
})