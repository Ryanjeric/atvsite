app.factory('Freebies', function($http, $q, Config){
	return {
		data: {},
        showme:  function(callback){
            $http({
                url: Config.ApiURL + "/freebies/show",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
            });
         },
         saveme: function(content,callback){
             $http({
                url: Config.ApiURL+"/freebies/savedata",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data:$.param(content)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        showme1:  function(callback){
            $http({
                url: Config.ApiURL + "/deliver/show",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
            });
         },
         saveme1: function(content,callback){
             $http({
                url: Config.ApiURL+"/deliver/savedata",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data:$.param(content)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
         showme2:  function(callback){
            $http({
                url: Config.ApiURL + "/special/show",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
            });
         },
         saveme2: function(content,callback){
             $http({
                url: Config.ApiURL+"/special/savedata",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data:$.param(content)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
	}
})