app.factory('Rentalhours', function($http, $q, Config){
	return {
		data: {},
         addrh: function(content,callback){
             $http({
                url: Config.ApiURL+"/rentalhours/savedata",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data:$.param(content)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        total: function(start,end,callback){
             $http({
                url: Config.ApiURL+"/rental/total/"+start+"/"+end,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        list: function(num,off,keyword,callback) {
            $http({
                url: Config.ApiURL + '/rental/list/' + num + '/' + off + '/' + keyword,
                method: "GET",
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        update: function(asset, callback) {
            $http({
                url: Config.ApiURL+"/rental/update",
                method: "POST",
                headers: {"Content-Type" : "application/x-www-form-urlencoded"},
                data: $.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        edit: function(asset,callback){
             $http({
                url: Config.ApiURL+"/rental/edit/"+asset,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        deleted: function(asset,callback){
             $http({
                url: Config.ApiURL+"/rental/delete/"+asset,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        showme:  function(callback){
            $http({
                url: Config.ApiURL + "/rentalmsg/show",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
            });
         },
         saveme: function(content,callback){
             $http({
                url: Config.ApiURL+"/rentalmsg/savedata",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data:$.param(content)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
				sessions: function(callback){
						$http({
							 url: Config.ApiURL+"/rentalhours/sessions",
							 method: "GET",
							 headers: {'Content-Type': 'application/x-www-form-urlencoded'}
					 }).success(function (data, status, headers, config) {
							 callback(data);
					 });
			 },
			 sessionduration: function(session, callback) {
				 $http({
					 url: Config.ApiURL+"/rentalhours/sessionduration",
					 method: "POST",
					 headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					 data:$.param({session:session})
				 }).success(function (data, status, headers, config) {
					 callback(data);
				 });
			 }
	}
})
