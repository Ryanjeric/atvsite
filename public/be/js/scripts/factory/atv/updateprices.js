app.factory('Updateprices', function($http, $q, Config){
	return {
		data: {},
       info: function(asset,callback){
            $http({
                url: Config.ApiURL+"/atv/info/"+asset,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data, status, headers, config) {
                callback(data);
            }); 
        },
        update: function(asset, callback) {
            $http({
                url: Config.ApiURL+"/atv/update",
                method: "POST",
                headers: {"Content-Type" : "application/x-www-form-urlencoded"},
                data: $.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
	}
})