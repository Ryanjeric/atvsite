app.factory('Requirements', function($http, $q, Config){
	return {
		data: {},
         addreq: function(asset,callback){

             $http({
                url: Config.ApiURL+"/requirements/save",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        list: function(num,off,keyword,callback) {
            $http({
                url: Config.ApiURL+"/requirements/list/" + num + '/' + off + '/' + keyword,
                method: "GET",
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        update: function(asset, callback) {
            $http({
                url: Config.ApiURL+"/requirements/update",
                method: "POST",
                headers: {"Content-Type" : "application/x-www-form-urlencoded"},
                data: $.param(asset)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        deleted: function(asset,callback){
             $http({
                url: Config.ApiURL+"/requirements/delete/"+asset,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
	}
})