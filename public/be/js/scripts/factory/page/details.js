app.factory('PageFactory', function($http, $q, Config ,Upload){
	return {
        getDetails: function(callback) {
            $http({
                url: Config.ApiURL+"/pagedetails/getcontact",
                method: "GET",
                headers: {"Content-Type" : "application/x-www-form-urlencoded"}
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
		saveContactInfo: function(data, callback) {
            $http({
                url: Config.ApiURL+"/pagedetails/update",
                method: "POST",
                headers: {"Content-Type" : "application/x-www-form-urlencoded"},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        gethome: function(callback) {
            $http({
                url: Config.ApiURL+"/pagedetails/gethome",
                method: "GET",
                headers: {"Content-Type" : "application/x-www-form-urlencoded"}
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        updatehomeimg: function(num, data, callback) {
            $http({
                url: Config.ApiURL+"/pagedetails/updatehomeimg/" + num,
                method: "POST",
                headers: {"Content-Type" : "application/x-www-form-urlencoded"},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        loadbanner: function(callback) {
            $http({
                url: Config.ApiURL+"/pagedetails/getbanner",
                method: "GET",
                headers: {"Content-Type" : "application/x-www-form-urlencoded"}
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        updatebanner: function(data, callback){
            $http({
                url: Config.ApiURL+"/pagedetails/updatebanner",
                method: "POST",
                headers: {"Content-Type" : "application/x-www-form-urlencoded"},
                data: $.param(data)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        }
    }
})