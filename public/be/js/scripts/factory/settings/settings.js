app.factory('Settings', function($http, $q, Config){
	return {
		saveimage: function(fileout, callback){
			$http({
				url: Config.ApiURL + "/settings/uploadlogo",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(fileout)
			}).success(function (data, status, headers, config) {
				callback(data);

			}).error(function (data, status, headers, config) {
				callback(data);
			});

		},
		loadimage: function(callback){
			$http({
				url: Config.ApiURL + "/settings/logolist",
				method: "GET",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);

			})
		},
	}
})