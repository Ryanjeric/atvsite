app.factory('Gallery', function($http, $q, Config){
	return {
		loadimage: function(callback){
			$http({
				url: Config.ApiURL + "/gallery/list",
				method: "get",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).success(function(data) {
				callback(data);
			})
		},
		saveVid: function(link, callback){
            $http({
                url: Config.ApiURL + "/gallery/vidsave",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(link)
            }).success(function(data, status, headers, config) {
                callback(data);
            }).error(function(data, status, headers, config) {
                callback(data);
            });
        },
        loadVideo: function(callback){
            $http({
                url: Config.ApiURL + "/gallery/vidlist",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        },
          deleteVideo: function(datavideo, callback){
            $http({
                url: Config.ApiURL + "/gallery/deletevideo",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(datavideo)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },
        showme1:  function(callback){
            $http({
                url: Config.ApiURL + "/adventure/show",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function (data, status, headers, config) {
                data = data;
                callback(data);
            }).error(function (data, status, headers, config) {
                data = data;
            });
         },
         saveme1: function(content,callback){
             $http({
                url: Config.ApiURL+"/adventure/savedata",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data:$.param(content)
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
	}
})