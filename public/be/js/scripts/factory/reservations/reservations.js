app.factory('Reservations', function($http, $q, Config){
	return {
		data: {},
        list: function(num,off,keyword,callback) {
            $http({
                url: Config.ApiURL+"/reservation/belist/" + num + '/' + off + '/' + keyword,
                method: "GET",
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        getreservation:  function(callback){
            $http({
                url: Config.ApiURL + "/reservation/getreservationbe",
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        }, 
        viewreservation: function(invoice,callback){
            $http({
                url: Config.ApiURL + "/reservation/viewreservation/" + invoice,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
        changestatus1: function(id,callback){
            $http({
                url: Config.ApiURL + "/reservation/changestatus1/" + id,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
        changestatus2: function(id,callback){
            $http({
                url: Config.ApiURL + "/reservation/changestatus2/" + id,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
        changestatus3: function(id,callback){
            $http({
                url: Config.ApiURL + "/reservation/changestatus3/" + id,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
        changestatus4: function(id,callback){
            $http({
                url: Config.ApiURL + "/reservation/changestatus4/" + id,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
        ////////////////////////////////////////////////////////////////////////////////////

        list1: function(num,off,keyword,callback) {
            $http({
                url: Config.ApiURL+"/reservation/belist1/" + num + '/' + off + '/' + keyword,
                method: "GET",
            }).success(function (data, status, headers, config) {
                callback(data);
            });
        },
        status0: function(id,callback){
            $http({
                url: Config.ApiURL + "/reservation/status0/" + id,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
        status1: function(id,callback){
            $http({
                url: Config.ApiURL + "/reservation/status1/" + id,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
        status2: function(id,callback){
            $http({
                url: Config.ApiURL + "/reservation/status2/" + id,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
        status3: function(id,callback){
            $http({
                url: Config.ApiURL + "/reservation/status3/" + id,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
        status4: function(id,callback){
            $http({
                url: Config.ApiURL + "/reservation/status4/" + id,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },
        viewreservation1: function(id,callback){
            $http({
                url: Config.ApiURL + "/reservation/viewreservation1/" + id,
                method: "GET",
                headers:{
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                callback(data);

            })
        },

	}
})